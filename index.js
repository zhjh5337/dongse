import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'
import { store } from './src/reducers/index'
import App from './src/containers/App'
import * as Api from './src/network/Api'
import * as Types from './src/actions/types'
import {Actions} from 'react-native-router-flux'
import ProgressDialog from './src/components/ProgressDialog'
import Notify from './src/components/ToastUtil'
import Toast from 'react-native-root-toast'
import Storage from './src/components/Storage'
import * as Tools from './src/components/Tools'

global.Api = Api
global.Store = store
global.Types = Types
global.Actions = Actions
global.ProgressDialog = ProgressDialog
global.Toast = Toast
global.Notify = Notify
global.Storage = Storage
global.Tools = Tools

export default class Index extends Component {

    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}
AppRegistry.registerComponent('DongSe', () => Index);
