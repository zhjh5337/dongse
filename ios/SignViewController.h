//
//  SignViewController.h
//  DongSe
//
//  Created by 李龙 on 2018/5/5.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface SignViewController : UIViewController
@property(nonatomic, strong) RCTResponseSenderBlock myCallback;//回调数据
@property (nonatomic, weak) RCTBridge *bridge;
@end
