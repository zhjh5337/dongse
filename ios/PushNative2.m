//
//  PushNative2.m
//  DongSe
//
//  Created by 李龙 on 2018/5/5.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "PushNative2.h"

// 导入跳转的页面
#import "AppDelegate.h"
#import "SignViewController.h"

@implementation PushNative2

RCT_EXPORT_MODULE(PushNative2)
// RN跳转原生界面

// RNOpenOneVC指的就是跳转的方法，下面会用到
RCT_EXPORT_METHOD(RNOpenOneVC:(RCTResponseSenderBlock)callback){
  
  
  //主要这里必须使用主线程发送,不然有可能失效
  dispatch_async(dispatch_get_main_queue(), ^{
    SignViewController *one = [[SignViewController alloc]init];
    one.myCallback = callback;
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.nav pushViewController:one animated:YES];
  });
  
  
  
}



@end
