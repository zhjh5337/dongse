//
//  PushNative2.h
//  DongSe
//
//  Created by 李龙 on 2018/5/5.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
// 导入RCTBridgeModule类，这个是react-native提供
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>

@interface PushNative2 : NSObject<RCTBridgeModule>

@end
