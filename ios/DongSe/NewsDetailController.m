//
//  NewsDetailController.m
//  schoolApp
//
//  Created by 李龙 on 2017/12/21.
//  Copyright © 2017年 Facebook. All rights reserved.
//

// 新闻中心页面
#import "NewsDetailController.h"

@interface NewsDetailController ()<UIWebViewDelegate>

@end

@implementation NewsDetailController
{
  UIView *NavView;
  UIWebView *main;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.view.backgroundColor = [UIColor whiteColor];
  [self addTopNav];
  [self addWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 添加nav
 */
- (void)addTopNav{
  NavView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
  [NavView setBackgroundColor:[UIColor colorWithRed:70/255.0 green:69/255.0 blue:147/255.0 alpha:1]];
  [self.view addSubview:NavView];
  
  UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(15, 20, 12, 44)];
  [back setAdjustsImageWhenDisabled:NO];
  [back setAdjustsImageWhenHighlighted:NO];
  [back setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
  [back addTarget:self action:@selector(leftButtonAction:) forControlEvents:UIControlEventTouchUpInside];
  [back.imageView setContentMode:UIViewContentModeScaleAspectFit];
  [NavView addSubview:back];
  
  UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 20, 100, 44)];
  [title setText:@"合同预览"];
  [title setTextAlignment:NSTextAlignmentCenter];
  [title setFont:[UIFont boldSystemFontOfSize:16.0f]];
  [title setTextColor:[UIColor whiteColor]];
  [NavView addSubview:title];
  
}

- (void)addWebView{
  
  main = [[UIWebView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(NavView.frame), NavView.frame.size.width, self.view.frame.size.height-64)];
  main.delegate = self;
  [main setOpaque:NO];
  main.dataDetectorTypes = UIDataDetectorTypeNone;
  [main setBackgroundColor:[UIColor whiteColor]];
  main.scrollView.showsVerticalScrollIndicator = NO;
//  NSString *tem = [NSString stringWithFormat:@"<style>img{max-width: %fpx;height: auto;!important}</style>"
//                   "<style>video{max-width: %fpx;height: auto;!important}</style>",[UIScreen mainScreen].bounds.size.width-30,[UIScreen mainScreen].bounds.size.width-30];
//  NSString * content = [NSString stringWithFormat:@"%@%@",tem,_htmlUrl];
  NSString *encodedString = [_htmlUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  NSURL *weburl = [NSURL URLWithString:encodedString];
  [main loadRequest:[NSURLRequest requestWithURL:weburl]];
//  [main loadHTMLString:content baseURL:nil];
  [main setScalesPageToFit:YES];
  [self.view addSubview:main];
  
}

- (void)leftButtonAction:(UIButton *)sender
{
  NSLog(@"click back!");
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
//  NSString *injectionJSString = @"var script = document.createElement('meta');"
//  "script.name = 'viewport';"
//  "script.content=\"width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\";"
//  "document.getElementsByTagName('head')[0].appendChild(script);";
//  [webView stringByEvaluatingJavaScriptFromString:injectionJSString];
//
//  [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitUserSelect='none';"];
//  
//  [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
//  [webView stringByEvaluatingJavaScriptFromString:
//   @"var script = document.createElement('script');"
//   "script.type = 'text/javascript';"
//   "script.text = \"function ResizeImages() { "
//   "var myimg,oldwidth, newheight;"
//   "var maxwidth=100;" //缩放系数
//   "for(i=0;i <document.images.length;i++){"
//   "myimg = document.images;"
//   "myimg.setAttribute('style','max-width:300px;height:auto')"
//   "}"
//   "}\";"
//   "document.getElementsByTagName('head')[0].appendChild(script);"];
//
//
//  [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
  
  if(navigationType==UIWebViewNavigationTypeLinkClicked)//判断是否是点击链接
    
  {
    
    return NO;
    
  }
  else
  {
    return YES;
  }
  
}

// 控制器实现此方法
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
  if (action == @selector(copy:) ||
      action == @selector(paste:)||
      action == @selector(cut:))
  {
    return NO;
  }
  return [super canPerformAction:action withSender:sender];
}

@end
