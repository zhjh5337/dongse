//
//  PushNative3.h
//  schoolApp
//
//  Created by 李龙 on 2017/12/21.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

// 导入RCTBridgeModule类，这个是react-native提供
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>

@interface PushNative : NSObject<RCTBridgeModule>

@end
