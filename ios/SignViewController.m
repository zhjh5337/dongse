//
//  SignViewController.m
//  DongSe
//
//  Created by 李龙 on 2018/5/5.
//  Copyright © 2018年 Facebook. All rights reserved.
//

//手写签名
#import "SignViewController.h"
#import "BJTSignView.h"
#import "AppDelegate.h"
#import <React/RCTEventDispatcher.h>

#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
@interface SignViewController ()
@property(nonatomic,strong) BJTSignView *signView;
@property(nonatomic,strong) UIImageView *imageView;
@end

@implementation SignViewController
{
  UIView *NavView;
}
@synthesize bridge = _bridge;
- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.view.backgroundColor = [UIColor whiteColor];
  [self addTopNav];
  [self addMainView];
  
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
  return NO;
  
}
- (BOOL)shouldAutorotate{
  
  return NO;
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:YES];
  //在视图出现的时候，将allowRotate改为1，
  AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  delegate.allowRotate = 1;
  if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
  {
    NSNumber *num = [[NSNumber alloc] initWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:(id)num];
    [UIViewController attemptRotationToDeviceOrientation];
    //这行代码是关键
  }
  SEL selector=NSSelectorFromString(@"setOrientation:");
  NSInvocation *invocation =[NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
  [invocation setSelector:selector];
  [invocation setTarget:[UIDevice currentDevice]];
  int val =UIInterfaceOrientationLandscapeRight;
  [invocation setArgument:&val atIndex:2];
  [invocation invoke];
  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewWillDisappear:(BOOL)animated{
  [super viewWillDisappear:YES];
  AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  delegate.allowRotate = 0;
  if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
    SEL selector = NSSelectorFromString(@"setOrientation:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:[UIDevice currentDevice]];
    int val = UIInterfaceOrientationPortrait;
    [invocation setArgument:&val atIndex:2];
    [invocation invoke];
  }
}
/**
 添加nav
 */
- (void)addTopNav{
  NavView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenHeight, 44)];
  [NavView setBackgroundColor:[UIColor colorWithRed:70/255.0 green:69/255.0 blue:147/255.0 alpha:1]];
  [self.view addSubview:NavView];
  
  UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, 12, 44)];
  [back setAdjustsImageWhenDisabled:NO];
  [back setAdjustsImageWhenHighlighted:NO];
  [back setImage:[UIImage imageNamed:@"goback"] forState:UIControlStateNormal];
  [back addTarget:self action:@selector(leftButtonAction:) forControlEvents:UIControlEventTouchUpInside];
  [back.imageView setContentMode:UIViewContentModeScaleAspectFit];
  [NavView addSubview:back];
  
  UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(kScreenHeight/2-50, 0, 100, 44)];
  [title setText:@"签名"];
  [title setTextAlignment:NSTextAlignmentCenter];
  [title setFont:[UIFont boldSystemFontOfSize:16.0f]];
  [title setTextColor:[UIColor whiteColor]];
  [NavView addSubview:title];
  
}

- (void)leftButtonAction:(UIButton *)sender
{
  NSLog(@"click back!");
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)addMainView{
  //画布
  UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(NavView.frame), kScreenHeight, kScreenWidth-60-CGRectGetMaxY(NavView.frame))];
  
  [self.view addSubview:backView];
  self.signView = [[BJTSignView alloc] initWithFrame:backView.bounds];
  [backView addSubview:self.signView];
  
  UIButton *clearBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(backView.frame)+5, 80, 40)];
  [clearBtn setTitle:@"完成" forState:UIControlStateNormal];
  [clearBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [clearBtn setBackgroundColor:[UIColor lightGrayColor]];
  clearBtn.layer.cornerRadius=3;
  [self.view addSubview:clearBtn];
  [clearBtn addTarget:self action:@selector(imageBtnClick) forControlEvents:UIControlEventTouchUpInside];
  
  UIButton *imageBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(clearBtn.frame)+15, clearBtn.frame.origin.y, clearBtn.frame.size.width, clearBtn.frame.size.height)];
  [imageBtn setTitle:@"清除签名" forState:UIControlStateNormal];
  [imageBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [imageBtn setBackgroundColor:[UIColor lightGrayColor]];
  imageBtn.layer.cornerRadius=3;
  [self.view addSubview:imageBtn];
  [imageBtn addTarget:self action:@selector(clearBtnClick) forControlEvents:UIControlEventTouchUpInside];
  
  self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - kScreenWidth/2)/2,380 , kScreenWidth/2, 200/2)];
  self.imageView.backgroundColor = [UIColor blackColor];
  [self.view addSubview:self.imageView];
}

//照片获取本地路径转换
-(NSString *)getImagePath:(UIImage *)Image {
  
  NSString * filePath = nil;
  NSData * data = nil;
  
  if (UIImagePNGRepresentation(Image) == nil) {
    data = UIImageJPEGRepresentation(Image, 0.5);
  } else {
    data = UIImagePNGRepresentation(Image);
  }
  
  //图片保存的路径
  //这里将图片放在沙盒的documents文件夹中
  NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
  
  //文件管理器
  NSFileManager *fileManager = [NSFileManager defaultManager];
  
  //把刚刚图片转换的data对象拷贝至沙盒中
  [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
  NSString * ImagePath = [[NSString alloc]initWithFormat:@"/theSignImage.png"];
  [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:ImagePath] contents:data attributes:nil];
  
  //得到选择后沙盒中图片的完整路径
  filePath = [[NSString alloc]initWithFormat:@"%@%@",DocumentsPath,ImagePath];
  
  return filePath;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationMaskLandscape;
}
#pragma mark - 清楚图片
- (void)clearBtnClick{
  
  [self.signView clearSignature];
}

#pragma mark - 生成图片
- (void)imageBtnClick{
  UIImage *image  =  [self.signView getSignatureImage];
  self.imageView.image = image;
  [self getImagePath:image];
//  NSData *data = UIImageJPEGRepresentation(image, 1.0f);
//
//  NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//  NSArray *events=@[encodedImageStr];
  self.myCallback(@[[self getImagePath:image]]);

//  [self.bridge.eventDispatcher sendAppEventWithName:@"sentImage" body:@{@"image": [self getImagePath:image]}];
  
  [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
