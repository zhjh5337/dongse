package com.dongse.car;

import android.support.multidex.MultiDexApplication;


import com.dongse.car.BuildConfig;
import com.dongse.car.module.MyReactPackage;

import com.facebook.react.ReactApplication;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.rnfs.RNFSPackage;
import com.microsoft.codepush.react.CodePush;
import com.psykar.cookiemanager.CookieManagerPackage;
import com.beefe.picker.PickerViewPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.yunpeng.alipay.AlipayPackage;
import com.theweflex.react.WeChatPackage;
import org.wonday.pdf.RCTPdfView;
import cn.qiuxiang.react.amap3d.AMap3DPackage;

import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.RNFetchBlob.RNFetchBlobPackage;

import org.reactnative.camera.RNCameraPackage;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends MultiDexApplication implements ReactApplication {

    private final String CODEPUSH_KEY_PRODUCTIO = "3dmMxKWlaXca23MOoHOiIKdaN8XtH1pwEvX6G";
    private final String CODEPUSH_KEY_STAGING = "rTAztLGGv-hVb4-WqmoK8pwVa1_QHyXPEvmpz";
    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {



        @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }

        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new RNI18nPackage(),
                    new RNFSPackage(),
                    new CodePush(CODEPUSH_KEY_STAGING, getApplicationContext(), BuildConfig.DEBUG),
                    new CookieManagerPackage(),
                    new PickerViewPackage(),
                    new VectorIconsPackage(),
                    new AlipayPackage(),
                    new WeChatPackage(),
                    new AMap3DPackage(),
                    new PickerPackage(),
                    new RNFetchBlobPackage(),
                    new RNCameraPackage(),
                    new MyReactPackage(),
                    new RCTPdfView()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
