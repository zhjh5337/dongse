package com.dongse.car.view;

import com.dongse.car.widget.MyWebView;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

/**
 * @author zhjh
 */
public class MyWebViewManager extends SimpleViewManager<MyWebView> {
    @Override
    public String getName() {
        return "MyWebView";
    }

    @Override
    protected MyWebView createViewInstance(ThemedReactContext reactContext) {
        MyWebView myWebView = new MyWebView(reactContext);
        return myWebView;
    }


    @ReactProp(name="url")
    public void loadUrl(MyWebView view,String url){
        view.loadUrl(url);
    }
}
