package com.dongse.car.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongse.car.widget.MyWebView;

/**
 * @author zhjh
 */
public class WebActivity extends AppCompatActivity{
    private MyWebView webView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webView = new MyWebView(this);
        webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(webView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String data = getIntent().getStringExtra("data");
        Log.e("test", "===== webActivity ===== " + data);
        JSONObject jsonObject = JSON.parseObject(data);
        String url =  jsonObject.getString("url");
        String name =  jsonObject.getString("name");
        getSupportActionBar().setTitle(name);
        webView.loadUrl(url);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.onBack();
    }

    public void onBack(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0)
            {
                getSupportFragmentManager().popBackStack();
            }
            else
            {
                this.onBack();
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
