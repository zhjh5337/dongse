package com.dongse.car.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * Created by weilai on 2016/12/14.
 */

public class MyWebView extends WebView {

    private WebSettings webSettings;
    //声明AMapLocationClient类对象

    public MyWebView(Context context) {
        super(context);
        initSettings();
    }

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSettings();
    }

    private void initSettings() {
        webSettings = this.getSettings();
        //支持javascript
        webSettings.setJavaScriptEnabled(true);


        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(true);
        //扩大比例的缩放
        webSettings.setUseWideViewPort(true);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        // 开启Javascript脚本
        webSettings.setJavaScriptEnabled(true);
        // 启用localStorage 和 essionStorage
        webSettings.setDomStorageEnabled(true);
        // 设置默认为utf-8
        webSettings.setDefaultTextEncodingName("UTF-8");

        // 开启应用程序缓存
        webSettings.setAppCacheEnabled(true);
        webSettings.setSupportZoom(true);
        String appCacheDir = getContext().getDir("cache", Context.MODE_PRIVATE).getPath();
        webSettings.setAppCachePath(appCacheDir);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setAppCacheMaxSize(1024 * 1024 * 10);// 设置缓冲大小，我设的是10M
        webSettings.setAllowFileAccess(true);


        String databaseDir = getContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setDatabasePath(databaseDir);// 设置数据库路径

        // 启用地理定位
        webSettings.setGeolocationEnabled(true);
        // 设置定位的数据库路径
        webSettings.setGeolocationDatabasePath(databaseDir);
        // 启用Webdatabase数据库
        webSettings.setDatabaseEnabled(true);
        // 开启插件（对flash的支持）
        webSettings.setPluginState(WebSettings.PluginState.OFF);
        // webSettings.setPluginsEnabled(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        setWebChromeClient(chromeClient);
        setWebViewClient(webViewClient);
    }

    private WebViewClient webViewClient = new WebViewClient() {
        // 处理页面导航
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            loadUrl(url);
            // 记得消耗掉这个事件。给不知道的朋友再解释一下，
            // Android中返回True的意思就是到此为止吧,事件就会不会冒泡传递了，我们称之为消耗掉
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
//            view.loadUrl("javascript:" + getJS("removeTitle"));
//            if(url.equals("http://www.bus365.com/public/www/register.html")){
//                show();
//                view.loadUrl("javascript:gotobuyticket()");
//            }else if(url.startsWith("http://www.bus365.com/public/www/index.html")){
//                view.loadUrl("javascript:" + getJS("removeIndex"));
//            }else if(url.startsWith("http://www.bus365.com/public/www/addcar.html")){
//                view.loadUrl("javascript:" + getJS("saveUser"));
//            }
            super.onPageFinished(view, url);
//            dismiss();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

//            show();
        }
    };


    private String getJS(String funtionName) {
        return "var newscript = document.createElement(\"script\");\n" +
                "\t\tnewscript.src=\"http://www.chinaxiancitywall.com/appService.js\";\n" +
                "\t\tnewscript.onload=function(){" + funtionName + "();};\n" +
                "\t\tdocument.body.appendChild(newscript);";
    }

    /**
     * 浏览网页历史记录
     * goBack()和goForward()
     */
    private WebChromeClient chromeClient = new WebChromeClient() {
        private View myView = null;
        private CustomViewCallback myCallback = null;

        // 配置权限 （在WebChromeClinet中实现）
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin,
                                                       GeolocationPermissions.Callback callback) {
            callback.invoke(origin, true, true);
            super.onGeolocationPermissionsShowPrompt(origin, callback);
        }

        // 扩充数据库的容量（在WebChromeClinet中实现）
        @Override
        public void onExceededDatabaseQuota(String url,
                                            String databaseIdentifier, long currentQuota,
                                            long estimatedSize, long totalUsedQuota,
                                            WebStorage.QuotaUpdater quotaUpdater) {

            quotaUpdater.updateQuota(estimatedSize * 2);
        }

        // 扩充缓存的容量
        @Override
        public void onReachedMaxAppCacheSize(long spaceNeeded,
                                             long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {

            quotaUpdater.updateQuota(spaceNeeded * 2);
        }

        // Android 使WebView支持HTML5 Video（全屏）播放的方法
        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            if (myCallback != null) {
                myCallback.onCustomViewHidden();
                myCallback = null;
                return;
            }

            ViewGroup parent = (ViewGroup) getParent();
            parent.removeView(MyWebView.this);
            parent.addView(view);
            myView = view;
            myCallback = callback;
            chromeClient = this;
        }

        @Override
        public void onHideCustomView() {
            if (myView != null) {
                if (myCallback != null) {
                    myCallback.onCustomViewHidden();
                    myCallback = null;
                }
                ViewGroup parent = (ViewGroup) myView.getParent();
                parent.removeView(myView);
                parent.addView(MyWebView.this);
                myView = null;
            }
        }
    };


}
