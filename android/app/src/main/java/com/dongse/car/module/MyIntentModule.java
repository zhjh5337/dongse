package com.dongse.car.module;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.dongse.car.MainActivity;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by xll on u2018/3/20.
 */

public class MyIntentModule extends ReactContextBaseJavaModule {

    public static String PARAMAS_KEY = "data";

    public MyIntentModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "IntentModule";
    }

    /***
     * RN跳转原生 仅跳转
     * @param name 页面名称 包.类名
     * @param params 传递的参数
     */
    @ReactMethod
    public void startActivityFromJS(String name, String params) {
        try {
            Activity currentActivity = getCurrentActivity();
            if (null != currentActivity) {
                Class toActivity = Class.forName(name);
                Intent intent = new Intent(currentActivity, toActivity);
                intent.putExtra(PARAMAS_KEY, params);
                currentActivity.startActivity(intent);
            }
        } catch (Exception e) {
            throw new JSApplicationIllegalArgumentException(
                    "不能打开Activity : " + e.getMessage());
        }
    }

    /**
     * RN调用Android原生  跳转页面
     * @param name activity 页面
     * @param params 参数
     * @param callback 回退返回
     */
    @ReactMethod
    public void startActivityFroResult(String name, String params, Callback callback) {
        try {
            if (TextUtils.isEmpty(params)) {
                return;
            }
            Activity currentActivity = getCurrentActivity();
            if (null != currentActivity) {
                Class toActivity = Class.forName(name);
                Intent intent = new Intent(currentActivity, toActivity);
                intent.putExtra(PARAMAS_KEY, params);
                currentActivity.startActivityForResult(intent, 100);
                //进行回调数据
                callback.invoke(MainActivity.mQueue.take());
            }
        } catch (Exception e) {
            throw new JSApplicationIllegalArgumentException(
                    "不能打开Activity : " + e.getMessage());
        }
    }

}
