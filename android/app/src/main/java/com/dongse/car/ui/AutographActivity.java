package com.dongse.car.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongse.car.R;
import com.zhjh.handwrite.view.HandWriteView;

import java.io.File;
import java.io.IOException;
import java.util.UUID;


/**
 * 手写签名板
 *
 * @author zhjh
 */
public class AutographActivity extends AppCompatActivity implements View.OnClickListener {

    private HandWriteView handWriteView;
    private Button save;
    private Button clear;
    private String path;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autograph);
        this.path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + UUID.randomUUID() + ".png";
        this.handWriteView = findViewById(R.id.handWrite);
        this.save =  findViewById(R.id.save);
        this.clear =  findViewById(R.id.clear);
        this.save.setOnClickListener(this);
        this.clear.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String data = getIntent().getStringExtra("data");
        JSONObject jsonObject = JSON.parseObject(data);
        String name =  jsonObject.getString("name");
        String saveName =  jsonObject.getString("saveName");
        String clearName =  jsonObject.getString("clearName");
        getSupportActionBar().setTitle(name);
        this.save.setText(saveName);
        this.clear.setText(clearName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0x555);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save) {
            this.saveImage();
        } else if (v.getId() == R.id.clear) {
            handWriteView.clear();
        }
    }


    private void saveImage() {
        if (handWriteView.isSign()) {
            try {
                handWriteView.save(path, true, 10, false);
                Intent intent = new Intent();
                intent.putExtra("path", path);
                setResult(RESULT_OK, intent);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(AutographActivity.this, "还没有签名！", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.onBack();
    }

    public void onBack(){
        Intent intent = new Intent();
        intent.putExtra("path", "");
        setResult(RESULT_OK, intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0)
            {
                getSupportFragmentManager().popBackStack();
            }
            else
            {
                this.onBack();
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
