package com.dongse.car;

import android.content.Intent;

import com.facebook.react.ReactActivity;

import java.util.concurrent.ArrayBlockingQueue;

public class MainActivity extends ReactActivity {

    //构建一个阻塞的单一数据的队列
    public static ArrayBlockingQueue<String> mQueue = new ArrayBlockingQueue<String>(1);
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "DongSe";
    }


    /**
     * 打开 带返回的Activity
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_OK){
            return;
        }
        if(requestCode == 100){
            String result = data.getStringExtra("path");
            mQueue.add(result);
        }
    }
}
