import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    View,
    Image,
    Keyboard,
} from 'react-native'

import {AppSetting, AppStyles} from '../components/AppStyles'
import Button from '../components/Button'
import {InputStyles} from '../components/InputView'
import ActionBar from '../components/ActionBar'
import Toast from "react-native-root-toast"
import Location from '../components/Location'
import ForgetPassword from "./me/ForgetPassword";


export default class Login extends Component {


    constructor(props) {
        super(props)
        this.state = {
            // mobile: undefined,
            // password: undefined,
            // mobile: __DEV__ ? '18591914191' : undefined,
            mobile: __DEV__ ? '15903486867' : undefined,
            password:  __DEV__ ? '111111' : undefined,
            // mobile: __DEV__ ? '18092289558' : undefined,
            // password:  __DEV__ ? '111111' : undefined,
        }
    }


    paramsIsNull = () => {
        if (!this.state.mobile || !Tools.isPhone(this.state.mobile)) {
            Toast.show('请正确输入手机号');
            return true;
        }
        if (!this.state.password) {
            Toast.show('请输入密码');
            return true;
        }
        return false;
    }

    onLoginPress = () => {
        // Location.getLongitudeAndLatitude();
        // Actions.reset('Main')
        // Actions.reset('SelectCity')
        // Actions.reset('AreaPicker')

        // AreaPicker.show();
        if (this.paramsIsNull()) {
            return;
        }
        ProgressDialog.show()
        Api.login(this.state.mobile, this.state.password)
            .then(this.imLogin)
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })

    };


    /**
     * IM登录
     * @param response
     */
    imLogin = (response) => {
        console.log(JSON.stringify(response))
        ProgressDialog.hide()
        Actions.reset('Splash')
    }


    inputView = (source, hint, value, isPassword, onChangeText) => {
        return (
            <View style={[InputStyles.background, {
                marginHorizontal: 20,
                marginTop: 20,
                borderRadius: 5,
                backgroundColor: '#ecdff0',
                justifyContent: 'flex-start',
            }]}
            >
                <Image source={source}/>
                <TextInput
                    placeholderTextColor={'#a798c1'}
                    value={value}
                    underlineColorAndroid='transparent'
                    style={[AppStyles.loginInput, {marginLeft: 10}]}
                    placeholder={hint}
                    maxLength={isPassword ? 12 : 11}
                    secureTextEntry={isPassword}
                    selectionColor={AppSetting.GREEN}
                    onChangeText={onChangeText}/>
            </View>
        );
    }


    render() {
        return (
            <TouchableWithoutFeedback onPress={() => {
                Keyboard.dismiss()
            }}>
                <ImageBackground style={styles.container}
                                 source={require('../../assets/images/login_bg.png')}
                                 resizeMode={'cover'}>
                    <ActionBar title={' '}
                               funcTextColor={AppSetting.DEFAULT_TEXT}
                               backgroundColor={'transparent'}
                               // functionName={'注册'}
                               // functionEvent={() => Actions.Register()}
                               isShowBack={false}
                    />


                    <Image style={{alignSelf: 'center', marginTop: 22, marginBottom: 20}}
                           source={require('../../assets/images/login_logo.png')}
                           resizeMode={'contain'}/>


                    {
                        this.inputView(
                            require('../../assets/images/ic_phone.png'),
                            '请输入手机号',
                            this.state.mobile,
                            false, (text) => {
                                this.setState({mobile: text})
                            })
                    }

                    {
                        this.inputView(
                            require('../../assets/images/ic_psw.png'),
                            '请输入密码',
                            this.state.password,
                            true, (text) => {
                                this.setState({password: text})
                            })
                    }

                    <Button style={{marginHorizontal: 20, marginTop: 20, borderRadius: 5}}
                            text={'登录'}
                            onPress={this.onLoginPress}/>


                    <View style={{
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        height: 30,
                        marginLeft:20,
                        marginRight:20,
                        marginTop: 20,
                        flexDirection:'row'
                    }}>
                        <TouchableOpacity
                            style={{paddingTop:5, paddingBottom:5, paddingRight:5, paddingLeft:3}}
                            activeOpacity={0.7}
                                          underlayColor='transparent'
                                          onPress={() => Actions.Register()}>
                            <Text
                                style={{color: AppSetting.DEFAULT_TEXT, textAlign:'left',fontSize: 15, backgroundColor: 'transparent'}}>
                                立即注册
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{paddingTop:5, paddingBottom:5,  paddingLeft:5}}
                            activeOpacity={0.7}
                                          underlayColor='transparent'
                                          onPress={() => Actions.ForgetPassword()}>
                            <Text
                                style={{color: AppSetting.DEFAULT_TEXT, fontSize: 15, backgroundColor: 'transparent'}}>
                                忘记密码？
                            </Text>
                        </TouchableOpacity>
                    </View>


                </ImageBackground>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
})
