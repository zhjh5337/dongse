import React, {Component} from 'react'
import {MapView, MultiPoint, Marker} from 'react-native-amap3d'
import AREA_DATA from '../../../assets/value/area.json';
import {AppSetting, ScreenWidth} from "../../components/AppStyles"
import SearchBar from "../../components/SearchBar"

import Location from "../../components/Location"

import {
    TextInput,
    Image,
    View
} from 'react-native'
/**
 * 附近
 */
export default class NearbyMap extends Component {


    constructor(props) {
        super(props);
        this.state = {
            currentlatitude: 0,
            currentlongitude: 0,
            currentCode: 0,
            points: [],
            currentSearch: '',
        }

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    getOffices(code) {

        Api.getOfficesList(code, 'citycode')
            .then(response => {
                var temArry = [];
                response.dataList.forEach((item, i) => {
                    temArry.push({
                        latitude: item.lat,
                        longitude: item.lng,
                        title: item.title,
                        address: item.address
                    })
                });
                this.setState({
                    points: temArry,
                })
            })
            .catch(e => {
                Toast.show(e.message);

            })
    }

    getCityCode(code) {
        var tem = 1;
        for (var i = 0; i < AREA_DATA.length - 1; i++) {
            for (var j = 0; j < AREA_DATA[i].childArea.length - 1; j++) {
                if (AREA_DATA[i].childArea[j].childArea.length > 0) {
                    for (var loop = 0; loop < AREA_DATA[i].childArea[j].childArea.length - 1; loop++) {
                        if (AREA_DATA[i].childArea[j].childArea[loop].areaID === parseInt(code)) {
                            tem = 2;
                            this.setState({
                                currentCode: AREA_DATA[i].childArea[j].childArea[loop].parentID,
                            }, function () {
                                this.getOffices(AREA_DATA[i].childArea[j].childArea[loop].parentID);

                            });
                            break;
                        }
                    }
                }
                else {
                    if (AREA_DATA[i].childArea[j].areaID === parseInt(code)) {
                        tem = 2;
                        this.setState({
                            currentCode: AREA_DATA[i].childArea[j].parentID,
                        }, function () {
                            this.getOffices(AREA_DATA[i].childArea[j].parentID);
                        });
                        break;
                    }
                }

            }
            if (tem !== 1) {
                break;
            }
        }

    }

    searchCallback(value) {

        if (value.pois.length !== 0) {
            var arr = value.pois[0].location.split(",");
            this.setState({
                currentlatitude: parseInt(arr[1]),
                currentlongitude: parseInt(arr[0]),
            }, function () {
                this.getCoverPoints();
            })
        }
        else {
            Toast.message('搜索地点有误');
        }
    }

    getCoverPoints() {
        let str = 'http://apis.map.qq.com/ws/geocoder/v1/?location=' + this.state.currentlatitude + ',' + this.state.currentlongitude + '&key=URWBZ-EEEKO-2UKWD-STDP6-SD6KT-Y7BES';
        fetch(str)
            .then(response => {
                var data = JSON.parse(response._bodyInit);

                this.getCityCode(data.result.ad_info.adcode);

            })
            .catch(error => {
            })
    }

    render() {

        return (
            <View style={{flex: 1}}>
                <View style={{
                    paddingHorizontal: 10,
                    paddingVertical: 6, backgroundColor: 'white', height: AppSetting.SECTION_MARGIN
                }}>

                    <SearchBar searchPlaceHolder="输入地址寻找周边办事处" onSubmitEditing={() => {
                        let data = Location.searchPoint(this.state.currentSearch, (value) => this.searchCallback(value));

                    }}
                               onChangeText={(value) => {
                                   this.setState({
                                       currentSearch: value,
                                   })
                               }}
                    />
                </View>
                <MapView
                    style={{flex: 1}}
                    locationEnabled
                    showsLocationButton={true}
                    onLocation={({nativeEvent}) => {
                        console.log(`${nativeEvent.latitude}, ${nativeEvent.longitude}`);

                        this.setState({
                            currentlatitude: nativeEvent.latitude,
                            currentlongitude: nativeEvent.longitude,
                        }, function () {
                            let str = 'http://apis.map.qq.com/ws/geocoder/v1/?location=' + this.state.currentlatitude + ',' + this.state.currentlongitude + '&key=URWBZ-EEEKO-2UKWD-STDP6-SD6KT-Y7BES'
                            this.getCoverPoints();
                        })
                    }
                    }
                    coordinate={{
                        latitude: this.state.currentlatitude,
                        longitude: this.state.currentlongitude,
                    }}
                    zoomLevel={12}

                >
                    {this.state.points.map(function (item, i) {
                        return (
                            <Marker key={i} title={item.title} color='red' description={item.address}
                                    coordinate={{latitude: item.latitude, longitude: item.longitude,}}/>
                        )
                    })}
                </MapView>
            </View>

        );
    }
}
