import React, {Component} from 'react'

import {
    Animated, DeviceEventEmitter,
    FlatList,
    Text,
    View
} from 'react-native'

const ListView = Animated.createAnimatedComponent(FlatList)
import {AppSetting, ScreenWidth} from "../../components/AppStyles"
import Button from "../../components/Button"
import IconItemView from "../../components/IconItemView"
const width = ScreenWidth / 4

import AREA_DATA from '../../../assets/value/area.json';

/**
 * 附近
 */
export default class NearbyList extends Component {


    constructor(props) {
        super(props);
        this.state = {
            area: props.area,
            childArea: [],
            areaPosition: 0,
            areaRefreshing: false,
            data: [],
        }

    }


    filterAreaData(areaID) {
        AREA_DATA.forEach((item, i) => {
            if (item.areaID == areaID) {
                this.setState({
                    childArea: item.childArea
                },function () {


                });
                return;
            }
            item.childArea.forEach((childItem, childI) => {
                if (childItem.areaID == areaID) {
                    this.setState({
                        childArea: childItem.childArea
                    },function () {

                    });
                    return;
                }
            })
        })
    }

    componentDidMount() {
        this.filterAreaData(this.state.area.area_id)
        DeviceEventEmitter.addListener('selectOffice', (data) => {
            this.filterAreaData(data.area_id);
            this.setState({
                area: data,
                areaPosition: 0,
            }, () => {
                this.getItemList(this.state.childArea[0].areaID,'Districtcode');
                // this.getItemList(data.item.areaID,'Districtcode');
            })
        });

    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            area: nextProps.area,
        })
    }

    componentWillUnmount() {

    }

    areaPress = (data) => {

        this.setState({
            areaPosition: data.item.key,
            areaRefreshing: !this.state.areaRefreshing,
        }, function () {
            this.getItemList(data.item.areaID,'Districtcode');
        })
    }

    getItemList(areaID,type){
        Api.getOfficesList(areaID, type)
            .then(response => {

                var temArry = [];
                response.dataList.forEach((item, i) => {
                    temArry.push({
                        latitude: item.lat,
                        longitude: item.lng,
                        title: item.title,
                        address: item.address
                    })
                });
                this.setState({
                    data: temArry,
                })
            })
            .catch(e => {
                Toast.show(e.message);

            })
    }

    areaItemView = (data) => {

        return (
            <View>
                <Button
                    style={{
                        height: AppSetting.ITEM_HEIGHT,
                        backgroundColor: this.state.areaPosition == data.item.key ? 'white' : AppSetting.DEFAULT_BG,
                    }}
                    color={AppSetting.DEFAULT}
                    fontSize={15}
                    text={data.item.areaName}
                    onPress={() => this.areaPress(data)}/>
                <View style={{height: AppSetting.LineWidth, backgroundColor: AppSetting.SplitLine}}/>
            </View>
        );
    }


    dataItemView = (data) => {

        return (

            <View style={{
                backgroundColor: 'white', borderBottomWidth: AppSetting.LineWidth,
                borderBottomColor: AppSetting.SplitLine
            }}>
                <Text style={{margin: 10, fontSize: 14, color: AppSetting.DEFAULT}}>
                    {data.item.title}
                </Text>
                <Text style={{
                    marginHorizontal: 10,
                    marginTop: 5,
                    marginBottom: 10,
                    fontSize: 11,
                    color: AppSetting.OTHER
                }}>
                    {data.item.address}
                </Text>
            </View>
        );
    };

    render() {
        return (
            <View style={{flex: 1,}}>
                <IconItemView
                    name={'购买城市'}
                    content={this.state.area.area_name}
                    onPress={() => Actions.SelectCity({isSelectOffice: true})}
                />
                <View style={{flex: 1, flexDirection: 'row'}}>

                    <View style={{
                        flex: 1,
                        backgroundColor: 'white',
                        borderRightWidth: AppSetting.LineWidth,
                        borderRightColor: AppSetting.SplitLine
                    }}>
                        <FlatList
                            data={this.state.childArea}
                            refreshing={this.state.areaRefreshing}
                            renderItem={this.areaItemView}
                            keyExtractor={(item, index) => item.key = index}
                        />
                    </View>
                    <View style={{flex: 3, backgroundColor: 'white'}}>
                        <ListView
                            data={this.state.data}
                            renderItem={this.dataItemView}
                            keyExtractor={(item, index) => item.key = index}
                        />
                    </View>
                </View>
            </View>

        );
    }
}
