import React, {Component} from 'react'
import {
    DeviceEventEmitter,
    View,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import IconItemView from "../../components/IconItemView"
import NearbyMap from "./NearbyMap"
import NearbyList from "./NearbyList"

/**
 * 附近
 */
export default class index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            actionType: true,
            area: props.area,
        }

    }

    actionPress = () => {
        this.setState({
            actionType: !this.state.actionType
        })
    }


    componentDidMount() {
        DeviceEventEmitter.addListener('selectOffice', (data) => {
            this.setState({
                area: data
            })
        });
        DeviceEventEmitter.addListener('selectArea', (data) => {
            console.log(data);
            this.setState({
                area: data,
            })
        });
    }

    componentWillUnmount() {

    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        isShowBack={false}
                        title={'附近'}
                        functionName={this.state.actionType ? '列表' : '地图'}
                        functionEvent={this.actionPress}
                    />
                </View>

                {
                    this.state.actionType ? <NearbyMap/> : <NearbyList area={this.state.area}/>
                }
            </View>
        );
    }
}
