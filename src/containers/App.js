import React, { Component } from 'react'

import {
    Platform,
    StatusBar,
    View,
    AppState,
} from 'react-native'
import CodePush from 'react-native-code-push'
import { connect } from 'react-redux'
import scenes from '../router'
import { Actions, Router} from 'react-native-router-flux'
const Routers = connect()(Router);




const CODE_PUSH_PRODUCTION = Platform.select({
    ios: 'SHIczHr6DBcBlwJHWbo5aCJoLtyXB11krwmaz',
    android: '3dmMxKWlaXca23MOoHOiIKdaN8XtH1pwEvX6G',
})

const CODE_PUSH_STAGING = Platform.select({
    ios: '0Ry7Wz53jY9VPmRYvdxySORT1HxJrymREw76f',
    android: 'rTAztLGGv-hVb4-WqmoK8pwVa1_QHyXPEvmpz',
})

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {

    }

    componentWillMount() {
        CodePush.disallowRestart();
        AppState.addEventListener('change', this.handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }


    componentWillReceiveProps(nextProps) {
    }


    /**
     * App  热更新
     */
    appHotUpdate = () => {
        CodePush.sync({
                updateDialog: {
                    //是否显示更新描述
                    appendReleaseDescription : true ,
                    //更新描述的前缀。 默认为"Description"
                    descriptionPrefix : '' ,
                    mandatoryUpdateMessage: '',
                    //强制更新按钮文字，默认为continue
                    mandatoryContinueButtonLabel : '立即更新' ,
                    //非强制更新时，按钮文字,默认为"ignore"
                    optionalIgnoreButtonLabel : '稍后',
                    //非强制更新时，确认按钮文字. 默认为"Install"
                    optionalInstallButtonLabel : '后台更新',
                    //非强制更新时，检查到更新的消息文本
                    optionalUpdateMessage : ' ' ,
                    //Alert窗口的标题
                    title : '更新提示'
                },
                mandatoryInstallMode: CodePush.InstallMode.IMMEDIATE,
                deploymentKey: CODE_PUSH_STAGING,
            },
            (status) => {
                switch (status) {
                    case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
                        ProgressDialog.show('应用更新中，请稍后...')
                        break;
                    case CodePush.SyncStatus.INSTALLING_UPDATE:
                        ProgressDialog.hide()
                        CodePush.allowRestart();
                        break;
                }
            },
            (progress) => {
                console.log('=======更新包下载进度=======：' + progress.receivedBytes + " of " + progress.totalBytes + " received.");
            })
    }


    /*
      app前后台切换时的回调
      --退出时记录退出时间
      --进入时preVerifyGesInfo判断
  */
    handleAppStateChange = (appState) => {
        if (appState == 'active') {
            this.appHotUpdate();
        } else if (appState == 'background') {

        } else {//active

        }
    }



    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    animated={true}
                    translucent={true}
                    barStyle={'light-content'}
                    backgroundColor={'rgba(0, 0, 0, 0.2)'}
                />
                <Routers scenes={scenes()}
                         backAndroidHandler={() => {
                             var result = Actions.currentScene != '_Home'
                             if (result) {
                                 Actions.pop()
                             }
                             return result;
                         }} />

            </View>
        )
    }

}

let codePushOptions = {
    //设置检查更新的频率
    //ON_APP_RESUME APP恢复到前台的时候
    //ON_APP_START APP开启的时候
    //MANUAL 手动检查
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,

};
App = CodePush(codePushOptions)(App);
export default App;
// export default connect(({ app }) => ({ user: app.currentUser}))(App);
