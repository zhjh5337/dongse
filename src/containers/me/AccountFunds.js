import React, {Component} from 'react'
import {
    View,
    Text,
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import Button from '../../components/Button'
import IconItemView from '../../components/IconItemView'

/**
 * 账户资金
 */
export default class AccountFunds extends Component {


    static propTypes = {};

    static defaultProps = {};


    constructor(props) {
        super(props)
        this.state = {
            totalMoney: 0,
            divide: 0,//已分成
            nodivide: 0,//未分成
        }
    }

    componentDidMount() {

        Api.getMyAccount(this.props.userId)
            .then(response => {
                console.log(response);
                let temDivide = 0;
                let temNodivide = 0;
                if (response.list.length > 0) {
                    response.list.forEach((item) => {
                        if (item.status === 0) {
                            temNodivide = item.money;
                        }
                        else if (item.status === 1) {
                            temDivide = item.money;
                        }
                    })
                }
                this.setState({
                    totalMoney: response.totalMoney,
                    divide: temDivide,
                    nodivide: temNodivide
                })
            })
            .catch(e => {
                Toast.show(e.message);
            })
    }

    onWithdrawalPress = () => {
        Actions.WithdrawalAmount()
        // Actions.BrokerageWithdraw()
    }


    render() {
        return (
            <View style={{
                flex: 1,
            }}>
                <View style={{
                    height: 160,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: AppSetting.MAIN_COLOR
                }}>
                    <Text
                        style={{fontSize: 26, color: 'white'}}>￥ {AppSetting.parseBalance(this.state.totalMoney)}</Text>
                    <Text style={{fontSize: 15, color: 'white', marginTop: AppSetting.NORMAL_MARGIN}}>报单总佣金</Text>
                </View>
                <IconItemView
                    align={'top'}
                    name={'已分成佣金'}
                    content={'￥ ' + AppSetting.parseBalance(this.state.divide)}
                    isShowArrow={false}/>
                <IconItemView
                    align={'center'}
                    name={'未分成佣金'}
                    content={'￥ ' + AppSetting.parseBalance(this.state.nodivide)}
                    isShowArrow={false}/>
                {/*<IconItemView*/}
                {/*align={'bottom'}*/}
                {/*name={'已提现'}*/}
                {/*content={'￥300000'}*/}
                {/*isShowArrow={false}/>*/}

                <IconItemView
                    style={{marginTop: AppSetting.NORMAL_MARGIN}}
                    name={'佣金明细'}
                    onPress={() => Actions.CommissionsDetail()}/>

                <View style={{flex: 1}}/>
                <Button style={{
                    marginHorizontal: AppSetting.CENTER_MARGIN,
                    marginBottom: AppSetting.CENTER_MARGIN, borderRadius: 5
                }}
                        text={'申请提现'}
                        onPress={this.onWithdrawalPress}/>
            </View>
        )
    }
}


