import React, {Component} from 'react'
import {
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Platform,
    Image,
    View,
    Text,
    Linking,
    ScrollView, Dimensions
} from 'react-native'
import ActionBar from '../../components/ActionBar'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import IconItemView from '../../components/IconItemView'
import Toast from "react-native-root-toast";

/**
 * 我的
 */
export default class index extends Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            user: this.props.user,
            tel: ''
        };
    }

    componentDidMount() {

        Api.getCustomerTel()
            .then(data => {
                if (data.tpConfig && data.tpConfig.value) {
                    this.setState({
                        tel: data.tpConfig.value
                    })
                }
            })
    }

    componentWillUnmount() {

    }

    /**
     * 判断是否实名认证
     * @returns {boolean}
     */
    isAuth = () => {
        if (!this.props.real || this.props.user.isAuth !== 2) {
            Toast.show('请先进行实名认证！')
            return false;
        }
        return true;
    }

    /**
     * 用户头像
     * @returns {*}
     */
    avatarView = () => {
        var topPosition = Platform.OS === 'ios' ? 45 : 57;
        return (
            <TouchableOpacity activeOpacity={0.98} style={{
                height: 150 - topPosition,
                flexDirection: 'row',
                alignItems: 'center',
                paddingLeft: 30,
                paddingRight: 30,
                marginTop: 15,
                backgroundColor: 'transparent'
            }} onPress={() => Actions.Setting()}>
                <Image
                    source={!this.props.user.headPic ? require('../../../assets/images/ic_default_head.png') :
                        {uri: this.props.user.headPic}}
                    style={styles.avatar}/>

                <View style={{
                    flex: 1, flexDirection: 'column',
                    justifyContent: 'center',
                    marginLeft: 15
                }}>
                    <Text style={{fontSize: 18, color: '#FFFFFF'}}>{this.state.user.nickname}</Text>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        marginTop: 8
                    }}>
                        {/*/ic_me_ordinary_member  1 普通用户   ic_me_vip_member vip用户*/}

                        {/*<Image*/}
                            {/*source={require('../../../assets/images/ic_me_vip_member.png')}*/}
                            {/*source={this.state.user.level === null || this.state.user.level == 1*/}
                                {/*? require('../../../assets/images/ic_me_ordinary_member.png')*/}
                                {/*: require('../../../assets/images/ic_me_vip_member.png')}*/}
                            {/*style={{height: 14, width: 14,}}/>*/}
                        <Text style={{
                            fontSize: 14,
                            color: '#DDDDDD',
                            marginLeft: 5
                        }}>{Tools.getLevelName(this.state.user.level)}</Text>
                        {/*<View style={{*/}
                        {/*borderColor: '#DDDDDD',*/}
                        {/*borderWidth: 1,*/}
                        {/*borderRadius: 8,*/}
                        {/*height: 16,*/}
                        {/*marginLeft: 5*/}
                        {/*}}>*/}
                        {/*<Text style={{*/}
                        {/*fontSize: 12, color: '#DDDDDD',*/}
                        {/*marginHorizontal: 8, marginVertical: 2,*/}
                        {/*textAlign:'center'*/}
                        {/*}}>{this.props.user.usertype === 1*/}
                        {/*? '个人用户'*/}
                        {/*: '企业用户'}</Text>*/}
                        {/*</View>*/}
                    </View>
                </View>
                {/*<Image source={require('../../../assets/images/arrow_right.png')} style={{tintColor: '#FFFFFF'}}/>*/}
            </TouchableOpacity>
        );
    }

    /***
     * 条目小图标
     * @param icon
     * @returns {*}
     */
    itemImageView = (icon) => {
        return (
            <Image
                source={icon === null ? require('../../../assets/images/ic_me_bank_card.png') : icon}
                resizeMode={'center'}
                style={{height: 30, width: 30}}/>
        );
    }

    /**
     *
     * @param icoPath
     * @param name
     * @param description
     */
    funcItemView = (icoPath, name, onPress) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
                onPress={onPress}>
                <Image
                    source={icoPath === null ? require('../../../assets/images/ic_me_bank_card.png') : icoPath}
                    resizeMode={'center'}
                    style={{height: 30, width: 30}}/>
                <Text style={styles.itemTextStyle}>{name}</Text>
            </TouchableOpacity>
        );
    }
    /***
     * 我的钱包
     * @returns {*}
     */
    myWalletView = () => {
        return (
            <View style={styles.itemBigView}>
                <IconItemView
                    align={'top'}
                    name={'我的钱包'}
                    isShowArrow={false}/>
                <View style={styles.itemTwoView}>
                    <TouchableOpacity style={styles.itemThreeView}
                                      onPress={() => Actions.AccountManager()}>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center', justifyContent: 'center', height: 26
                        }}>
                            <Text style={{fontSize: 14, color: '#CCCC00'}}>
                                {(this.state.user.userMoney === null ? 0 : this.state.user.userMoney).toFixed(2)}</Text>
                            <Text style={{fontSize: 12, paddingLeft: 2, color: '#999999'}}>元</Text>
                        </View>
                        <Text style={styles.itemTextStyle}>可用金额</Text>
                    </TouchableOpacity>

                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_me_bank_card.png'),
                        '银行卡',
                        () => {
                            {
                                if (!this.isAuth()) {
                                    return;
                                }
                                if (this.props.real.bankName && this.props.real.bankName !== '') {
                                    Actions.BankCard()
                                } else {
                                    Actions.BindingBankCard()
                                }
                            }
                        }
                    )}

                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_me_recharge.png'),
                        '充值',
                        () => Actions.Recharge())}

                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_me_withdraw.png'),
                        '提现',
                        () => {
                            if (!this.isAuth()) {
                                return;
                            }
                            if(!this.props.real.bankCard || this.props.real.bankCard === null ||  this.props.real.bankCard === '')
                            {
                                Toast.show('请绑定银行卡后进行提现操作');
                                return;
                            }
                            Actions.WithdrawalAmount();
                        })}

                </View>

            </View>
        );
    }
    /***
     * 消费订单
     * @returns {*}
     */
    consumerOrderView = () => {
        return (
            <View style={styles.itemBigView}>
                <IconItemView
                    align={'top'}
                    name={'消费订单'}
                    content={'全部订单'}
                    contentTextStyle={AppStyles.bottomText}
                    onPress={() => {

                        Actions.ConsumerOrders({position: 0})
                    }}/>
                <View style={styles.itemTwoView}>
                    {this.funcItemView(require('../../../assets/images/ic_corder_be_audit.png'),
                        '待审核', () => {

                            Actions.ConsumerOrders({position: 1})
                        })}
                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_corder_audited.png'),

                        '已审核', () => Actions.ConsumerOrders({position: 2}))}
                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_corder_out.png'),
                        '已出单', () => {

                            Actions.ConsumerOrders({position: 3})
                        })}
                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_order_com.png'),
                        '已完成', () => {

                            Actions.ConsumerOrders({position: 4})
                        })}
                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_corder_fail.png'),
                        '未通过', () => {

                            Actions.ConsumerOrders({position: 5})
                        })}

                </View>

            </View>
        );
    }


    /***
     * 商城订单
     * @returns {*}
     */
    goodsOrderView = () => {

        return (
            <View style={styles.itemBigView}>
                <IconItemView
                    align={'top'}
                    name={'商城订单'}
                    content={'全部订单'}
                    contentTextStyle={AppStyles.bottomText}
                    onPress={() => {
                        Actions.CommodityOrders({position: 0})
                    }}/>
                <View style={styles.itemTwoView}>


                    {this.funcItemView(require('../../../assets/images/ic_goods_bepay.png'),
                        '待付款', () => {

                            Actions.CommodityOrders({position: 1})
                        })}
                    <View style={styles.itemLineStyle}/>
                    {this.funcItemView(require('../../../assets/images/ic_goods_wait.png'),
                        '待发货', () => {

                            Actions.CommodityOrders({position: 2})
                        })}
                    <View style={styles.itemLineStyle}/>

                    {this.funcItemView(require('../../../assets/images/ic_goods_be_receive.png'),
                        '待收货', () => {

                            Actions.CommodityOrders({position: 3})
                        })}
                    <View style={styles.itemLineStyle}/>
                    {this.funcItemView(require('../../../assets/images/ic_goods_be_evaluate.png'),
                        '待评价', () => {

                            Actions.CommodityOrders({position: 5})
                        })}
                    <View style={styles.itemLineStyle}/>
                    {this.funcItemView(require('../../../assets/images/ic_goods_finish.png'),
                        '退款/售后', () => {

                            Actions.CommodityOrders({position: 6})
                        })}

                </View>

            </View>
        );
    }

    render() {
        return (
            <View style={AppStyles.main}>
                <ImageBackground style={{height: 200}}
                                 source={require('../../../assets/images/ic_me_top_bg.png')}
                                 resizeMode={'cover'}>
                    <ActionBar title={'我的'}
                               backgroundColor={'transparent'}
                               functionIco={require('../../../assets/images/ic_me_edit.png')}
                               functionEvent={() => Actions.Setting()}
                               isShowBack={false}/>
                    {this.avatarView()}
                </ImageBackground>

                <ScrollView style={{flex: 1}}>

                    {this.myWalletView()}
                    <View style={{marginTop: 15}}/>
                    {this.consumerOrderView()}
                    <View style={{marginTop: 15}}/>
                    {this.goodsOrderView()}
                    <IconItemView
                        style={{marginTop: AppSetting.NORMAL_MARGIN}}
                        align={'top'} name={'我的认证'}
                        icon={require('../../../assets/images/ic_me_my_auth.png')}
                        contentTextStyle={AppStyles.bottomText}
                        content={Tools.getAuthName(this.props.user.isAuth)}
                        onPress={() => {
                            this.props.user.isAuth === 2 ? Actions.VerifiedDetail() :
                            this.props.user.isAuth === 3 ?  Actions.NewVerified({isFail: true, reason: this.props.real.reviewContent}) : Actions.NewVerified()
                        }}

                    />
                    {
                        this.props.user.level === 1 ?
                            <IconItemView align={'center'} name={'会员升级'}
                                          icon={require('../../../assets/images/ic_me_recommend.png')}
                                          onPress={() => Actions.MembershipUpgrade()}
                            />
                            : null
                    }


                    {
                        this.props.user.level !== 1 ? null :
                            <IconItemView align={'center'} name={'我的推荐'}
                                          icon={require('../../../assets/images/ic_me_recommend.png')}
                                          onPress={() => {
                                              Actions.MyReferrals()
                                          }}
                            />
                    }

                    {
                        this.props.user.level === 1 ? null :
                            <IconItemView align={'center'} name={'我的客户'}
                                          icon={require('../../../assets/images/ic_me_customer.png')}
                                          onPress={() => {
                                              Actions.MyCustomer({userId: this.props.user.userId});
                                          }}
                            />
                    }

                    <IconItemView align={'center'} name={'地址管理'}
                                  icon={require('../../../assets/images/ic_ads.png')}
                                  onPress={() => {

                                      Actions.ReceivingAddress()
                                  }}
                    />
                    {
                        this.props.user.level === 1 ? null :
                            <IconItemView align={'center'} name={'合同管理'}
                                          icon={require('../../../assets/images/ic_contract.png')}
                                          onPress={() => {
                                              Actions.ContractManager()
                                          }}
                            />
                    }

                    <IconItemView align={'bottom'} name={'推广码'}
                                  icon={require('../../../assets/images/ic_me_promo_code.png')}
                                  onPress={() => {
                                      if (!this.isAuth()) {
                                          return;
                                      }
                                      Actions.ExtensionCode();
                                  }
                                  }
                    />

                    {/*<IconItemView*/}
                        {/*style={{marginTop: AppSetting.NORMAL_MARGIN}}*/}
                        {/*name={'帮助'}*/}
                        {/*icon={require('../../../assets/images/ic_me_help.png')}*/}
                    {/*/>*/}

                    <IconItemView
                        style={{marginTop: AppSetting.NORMAL_MARGIN, marginBottom: AppSetting.NORMAL_MARGIN}}
                        name={'客服电话'}
                        content={this.state.tel}
                        icon={require('../../../assets/images/icon_me_service_phone.png')}
                        onPress={() => Linking.openURL('tel:' + this.state.tel)}
                    />

                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    avatar: {
        resizeMode: 'stretch',
        height: 60,
        width: 60,
        justifyContent: 'center',
        borderRadius: 30,
        borderColor: 'white',
        borderWidth: 3
    },
    itemBigView: {
        flexDirection: 'column',
        backgroundColor: 'white',
        paddingBottom: 15,
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth
    },
    itemTopTextView: {
        fontSize: 15,
        color: '#666666',
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'transparent'
    },
    itemTwoView: {
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemThreeView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemTextStyle: {
        fontSize: 14,
        color: '#999999',
        marginTop: 8
    },
    itemLineStyle: {
        width: 1,
        height: 50,
        backgroundColor: '#eeeeee'
    }
})
