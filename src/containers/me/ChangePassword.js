import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import Button from '../../components/Button'
import ActionBar from '../../components/ActionBar'
import Toast from "react-native-root-toast"
import InputView from "../../components/InputView";

/**
 * 更改密码
 */
export default class ChangePassword extends Component {


    constructor(props) {
        super(props)
        this.state = {
            mobile: '',
            pin: '',
            password: '',
            newPassword: '',
            newPassword2: '',
        }
    }


    paramsIsNull = () => {
        if (!this.state.mobile) {
            Toast.show('请输入手机号');
            return true;
        }
        if (!this.state.password) {
            Toast.show('请输入密码');
            return true;
        }
        return false;
    }

    onRegisterPress = () => {

    }

    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'更改密码'}/>
                </View>


                <InputView
                    align={'top'}
                    value={this.state.password}
                    name={'原密码'}
                    hintText={'请输入原始密码'}
                    isPassword={true}
                    onChangeText={(text) => this.setState({password: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.newPassword}
                    name={'新密码'}
                    hintText={'请输入至少6位密码'}
                    isPassword={true}
                    onChangeText={(text) => this.setState({newPassword: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.newPassword2}
                    name={'确认'}
                    hintText={'请再次输入密码'}
                    isPassword={true}
                    onChangeText={(text) => this.setState({password2: text})}/>

                <Button style={{marginHorizontal: 20, marginTop: 40, borderRadius: 5}}
                        text={'确认修改'}
                        onPress={this.onRegisterPress}/>

            </View>
        )
    }
}
