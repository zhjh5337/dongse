import React, {Component} from 'react'
import {
    View
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {SegmentedView} from 'teaset'
import BaseConsumerOrders from "./BaseConsumerOrders"


/**
 * 消费订单
 */
export default class ConsumerOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            position: props.position || 0
        }
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'消费订单'}/>
                </View>
                <SegmentedView
                    style={{flex: 1}}
                    barStyle={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.MAIN_COLOR}
                    activeIndex={this.state.position}
                    justifyItem={'scrollable'}
                    onChange={position =>
                        this.setState({
                            position: position
                        })
                    }
                    >

                    <SegmentedView.Sheet title={'全部'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>

                        <BaseConsumerOrders type={-1} userId = {this.props.user.userId}/>

                    </SegmentedView.Sheet>

                    <SegmentedView.Sheet title={'待审核'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BaseConsumerOrders type={0} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>

                    <SegmentedView.Sheet title={'已审核'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BaseConsumerOrders type={1} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>


                    <SegmentedView.Sheet title={'已出单'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BaseConsumerOrders type={2} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>


                    <SegmentedView.Sheet title={'已完成'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BaseConsumerOrders type={3} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>

                    <SegmentedView.Sheet title={'未通过'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BaseConsumerOrders type={4} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>
                </SegmentedView>

            </View>
        );
    }
}
