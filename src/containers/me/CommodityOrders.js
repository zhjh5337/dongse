/**
 * Created by lilong on 2018/4/26.
 */
import React, {Component} from 'react'
import {
    View
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {SegmentedView} from 'teaset'
import BaseCommodityOrders from "./BaseCommodityOrders"


/**
 * 商城订单
 */
export default class CommodityOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            position: this.props.position||0,
        }
    }

    componentDidMount() {

    }
    render() {

        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'商城订单'}/>
                </View>
                <SegmentedView
                    style={{flex: 1}}
                    barStyle={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.MAIN_COLOR}
                    activeIndex={this.state.position}
                    justifyItem={'scrollable'}
                    onChange={position =>
                        this.setState({
                            position: position
                        },function () {
                            console.log(position);
                        })
                    }
                >
                    <SegmentedView.Sheet title={'全部订单'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>

                        <BaseCommodityOrders type={0} userId = {this.props.user.userId}/>

                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'待付款'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        <BaseCommodityOrders type={1} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'待发货'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        <BaseCommodityOrders type={2} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'待收货'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        {/*<BaseCommodityOrders type={3} userId = {this.props.user.userId}/>*/}
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'已完成'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        <BaseCommodityOrders type={4} userId = {this.props.user.userId}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'待评价'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        {/*<BaseCommodityOrders type={3} userId = {this.props.user.userId}/>*/}
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'退款售后'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>
                        {/*<BaseCommodityOrders type={3} userId = {this.props.user.userId}/>*/}
                    </SegmentedView.Sheet>

                </SegmentedView>

            </View>
        );
    }
}
