/**
 * Created by lilong on 2018/4/26.
 */
import React, {Component} from 'react'
import {
    View
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {SegmentedView} from 'teaset'
import BaseBalanceList from "./BaseBalanceList"


/**
 * 余额明细
 */
export default class BalanceDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            position: this.props.position||0,
        }
    }

    componentDidMount() {

    }
    render() {

        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'余额明细'}/>
                </View>
                <SegmentedView
                    style={{flex: 1}}
                    barStyle={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.MAIN_COLOR}
                    activeIndex={this.state.position}
                    onChange={position =>
                        this.setState({
                            position: position
                        })
                    }
                >
                    <SegmentedView.Sheet title={'全部'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>

                        <BaseBalanceList type={0} userId={this.props.user.userId}/>

                    </SegmentedView.Sheet>

                    <SegmentedView.Sheet title={'赚取'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>

                        <BaseBalanceList type={1} userId={this.props.user.userId}/>

                    </SegmentedView.Sheet>

                    <SegmentedView.Sheet title={'消费'}
                                         titleStyle={AppStyles.promptText}
                                         activeTitleStyle={AppStyles.tabActiveSmallItem}>

                        <BaseBalanceList type={2} userId={this.props.user.userId}/>

                    </SegmentedView.Sheet>


                </SegmentedView>

            </View>
        );
    }
}
