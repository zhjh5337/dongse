/**
 * Created by lilong on 2018/4/30.
 */

import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'

import {AppSetting, AppStyles} from "../../components/AppStyles"

/**
 * 客户ITEM
 */
export default class CustomsItem extends Component {

    constructor(props) {
        super(props)
        console.log(this.props);
        this.state = {
            mobile: this.props.data.mobile,
            nickname: this.props.data.nickname,
            regTime: this.props.data.regTime,
        }
    }

    componentDidMount() {
        console.log(this.props);
    }


    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        this.setState({
            mobile: nextProps.data.mobile,
            nickname: nextProps.data.nickname,
            regTime: nextProps.data.regTime,
        })
    }


    render() {
        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: AppSetting.NORMAL_MARGIN,
                    paddingRight: AppSetting.NORMAL_MARGIN,
                }}>
                    <Image
                        style={{
                            height: 40,
                            width: 40
                        }}
                        resizeMode={'contain'}
                        source={require('../../../assets/images/ic_car.png')}
                    />
                    <Text
                        style={[AppStyles.nameText, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{this.state.nickname || this.state.mobile}</Text>
                    {/*<View style={{flex:1}}/>*/}
                    {/*<Text style={AppStyles.contentText}>金额： 50000</Text>*/}
                </View>
                <View style={styles.itemLine}/>
                {/*<Text style={AppStyles.nameText}>姓名：<Text style={AppStyles.contentText}>{this.state.mobile}</Text></Text>*/}
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>联系方式：<Text
                    style={AppStyles.contentText}>{this.state.mobile}</Text></Text>
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>注册区域：<Text
                    style={AppStyles.contentText}>广东省 佛山市 南海区</Text></Text>
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>签约日期：<Text
                    style={AppStyles.contentText}>{Tools.timestampToTime(this.state.regTime)}</Text></Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingTop: AppSetting.NORMAL_MARGIN,
        paddingBottom: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemHead: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: AppSetting.NORMAL_MARGIN,
    }
})

