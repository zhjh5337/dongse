import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'

/**
 * 银行卡
 */
export default class BankCard extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        var position = this.props.real.bankName.indexOf('银行')
        var bankNmae = this.props.real.bankName.substring(0, position+2)
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'我的银行卡'}/>
                </View>

                <View style={styles.cardBg}>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{fontSize: 14, color: 'white'}}>{bankNmae}</Text>
                        <Text style={{fontSize: 14, color: 'white'}}>已经绑定</Text>
                    </View>

                    <Text style={{marginTop: AppSetting.CENTER_MARGIN, fontSize: 18, color: 'white'}}>{this.props.real.bankCard}</Text>

                    <Text style={{marginTop: AppSetting.CENTER_MARGIN, fontSize: 16, color: 'white'}}>{this.props.real.bankName}</Text>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    cardBg: {
        margin: AppSetting.CENTER_MARGIN,
        padding: AppSetting.CENTER_MARGIN,
        borderRadius: 5,
        backgroundColor: AppSetting.MAIN_COLOR,
    }
})

