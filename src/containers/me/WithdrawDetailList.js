import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import ListView from "../../components/DragonList"
import {AppSetting, AppStyles} from "../../components/AppStyles"

/**
 * 提现明细列表
 */
export default class BrokerageDetailList extends Component {

    static propTypes = {
        data: PropTypes.array,
    };

    static defaultProps = {
        data: [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
        ],
    };


    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: props.data
        }
    }

    componentDidMount() {
        // this.loadData(true)
    }


    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {

    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: data.page < data.total_pages
            })
            // this.dataDispatch(Math.min(data.page, data.total_pages), this.orderFilter(data.objects))
            this.dataDispatch(Math.min(data.page, data.total_pages), (data.objects))
            return
        }
        var oldData = this.props.data ? this.props.data : []
        var orderData = oldData.concat(data.objects.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.number == item.number
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: data.page < data.total_pages,
            loadingMore: false,
        })
        this.dataDispatch(Math.min(data.page, data.total_pages), this.orderFilter(orderData))
    }


    // orderFilter = (data) => {
    //     let newData = data.filter(item => item.status == 1 || item.status == 2)
    //     newData.sort((a, b) => {
    //         if (a.status > b.status) {
    //             return 1
    //         } else if (a.status < b.status) {
    //             return -1
    //         } else {
    //             if (a.created_at < b.created_at) {
    //                 return 1;
    //             } else if (a.created_at > b.created_at) {
    //                 return -1;
    //             }
    //             return 0;
    //         }
    //     })
    //     return newData
    // }

    /**
     * 数据发送回调
     * @param pageNum
     * @param data
     */
    dataDispatch = (pageNum, data) => {

    }

    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }

    /**
     * 订单匹配成功
     */
    // orderMatchingSuccess = (orderNum) => {
    //     this.dataDispatch(this.props.pageNum, this.props.data.filter(item => item.number != orderNum))
    // }

    render() {
        return (
            <ListView
                style={{flex: 1}}
                data={this.props.data}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                onEndReached={this.loadMore}
                renderItem={(item) => <DetailItemView data={item.item}/>}
                hasMoreData={this.state.hasMoreData}
                keyExtractor={(item, index) => item.key = index}
            />
        )
    }
}


/**
 * 明细ITEM
 */
class DetailItemView extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
    }


    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{height: 60, flexDirection: 'row', alignItems:'center'}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={AppStyles.nameText}>佣金收益</Text>
                        <Text style={[AppStyles.contentText, {marginTop: AppSetting.NORMAL_MARGIN}]}>20190812 0329</Text>
                    </View>
                    <Text style={{fontSize: 14, color: AppSetting.OTHER}}>+300.00</Text>
                </View>

            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        padding: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemHead: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: AppSetting.NORMAL_MARGIN,
    }
})

