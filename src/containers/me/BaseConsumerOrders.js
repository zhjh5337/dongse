import React, {Component} from 'react'

import ListView from "../../components/DragonList"
import ConsumerOrderItem from "./ConsumerOrderItem"
import {AppSetting} from "../../components/AppStyles"
import {DeviceEventEmitter} from "react-native";

/**
 * 订单列表baseview
 */
export default class BaseConsumerOrders extends Component {


    constructor(props) {
        super(props)
        this.state = {
            data: [],
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            type: this.props.type,
            userId: this.props.userId
        }
    }

    componentDidMount() {
        this.loadData(true)

    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            userId: nextProps.userId,
        }, () => this.loadData(true))
    }


    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {
        let type = undefined
        if(this.state.type === -1 ){
            type = undefined
        }else if (this.state.type === 0 ){
            type = '0,1'
        }else if (this.state.type === 1 ){
            type = '2'
        }else if (this.state.type === 2 ){
            type = '3'
        }else if (this.state.type === 3 ){
            type = '4'
        }else if (this.state.type === 4 ){
            type = '-2,-1'
        }
        Api.getConsumerOdersList(isRefresh, this.state.userId, type, isRefresh ? 0 : this.state.data[this.state.data.length-1].rownum)
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            })
            return
        }
        var oldData = this.state.data ? this.state.data : []
        var orderData = oldData.concat(data.dataList.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.rownum == item.rownum
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    }



    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }

    render() {
        return (
            <ListView
                style={{flex: 1}}
                data={this.state.data}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                onEndReached={this.loadMore}
                renderItem={(item) => <ConsumerOrderItem data={item.item}/>}
                hasMoreData={this.state.hasMoreData}
                keyExtractor={(item, index) => item.key = index}
            />
        )
    }
}
