import React from 'react';
import {
    Text,
    View,
    Image,
    Dimensions,
    StyleSheet
} from 'react-native';
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import QRCode from 'react-native-qrcode'

const WIDTH = Dimensions.get('window').width - AppSetting.NORMAL_MARGIN * 4

/**
 * 推广码
 */
export default class ExtensionCode extends React.Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {
    }


    componentWillMount() {

    }

    componentWillUnmount() {


    }

    createQr = () => {
        var qr = {}
        qr.tag = 'DongSe'
        qr.userId = this.props.user.userId
        qr.mobile = this.props.user.mobile
        return JSON.stringify(qr)
    }


    render() {

        return (
            <View style={{flex: 1, backgroundColor: '#5752a2'}}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'推广码'}
                    />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <View style={styles.centerBG}>


                        <View style={{flexDirection: 'row', margin: 20}}>

                            <Image
                                style={{
                                    width: 60,
                                    height: 60,
                                }}
                                source={this.props.user.headPic === null ? require('../../../assets/images/ic_head.png') :
                                    {uri: this.props.user.headPic}}
                            />
                            <View style={{
                                flex: 1,
                                marginLeft: AppSetting.NORMAL_MARGIN,
                            }}>
                                <Text style={AppStyles.nameText}>{this.props.real.realName}</Text>
                                <Text style={[AppStyles.contentText, {marginTop: 15}]}>{Tools.code2AreaName([this.props.user.province, this.props.user.city, this.props.user.district])}</Text>
                            </View>

                        </View>
                        <View style={{
                            height: AppSetting.LineWidth,
                            width: '100%',
                            backgroundColor: AppSetting.SplitLine,
                            marginBottom:20,
                        }}/>

                        <QRCode
                            value={this.createQr()}
                            size={240}
                            bgColor={AppSetting.BLACK}
                            fgColor='white'
                        />


                        <Text
                            style={[AppStyles.contentText, {
                                marginTop: 20,
                                marginBottom: 20
                            }]}>APP扫一扫上面的二维码推荐</Text>
                    </View>
                </View>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    centerBG: {
        width: WIDTH,
        marginLeft: AppSetting.NORMAL_MARGIN,
        marginRight: AppSetting.NORMAL_MARGIN,
        borderRadius: 5,
        backgroundColor: "white",
        shadowRadius: 5,
        alignItems: 'center',
    },
})
