import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    ScrollView,
    View,
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import Button from '../../components/Button'
import InputView from "../../components/InputView"
import Toast from "react-native-root-toast";


const note = '1、提现手续费将从余额中额外扣除；\n' +
    '2、手续费的计算精确到小数点后两位；\n' +
    '3、绑定的银行卡将是您唯一的提现账户；\n' +
    '4、系统将每月还款日前三天将月供汇入个人账号中，由个人在账号中提现至个人银行账户。'
/**
 * 提现
 */
export default class WithdrawalAmount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            password: undefined,
            account: undefined,
        }
    }

    paramsIsNull = () => {
        if (!this.state.account || !(/^(\d+)(\.\d+)?$/.test(this.state.account))) {
            Toast.show('请正确输入金额')
            return true;
        }
        if (parseFloat(this.state.account) > parseFloat(this.props.user.userMoney)) {
            Toast.show('请输入小于等于余额的数字')
            return true;
        }
        if (!this.state.password) {
            Toast.show('请输入密码');
            return true;
        }
        return false;
    }

    onWithdrawalPress = () => {
        if (this.paramsIsNull()) {
            return;
        }
        ProgressDialog.show();
        Api.cashApply(this.props.user.userId, this.props.real.bankName, this.props.real.bankCard, this.props.real.realName, this.state.account, this.state.password)
            .then(data => {
                Toast.show('提现申请成功！');
                ProgressDialog.hide();
            })
            .catch(e => {
                Toast.show(e.message);
                ProgressDialog.hide();
            })
    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'提现'}
                        functionName={'明细'}
                        functionEvent={() => Actions.CashRecord()}/>

                </View>
                <ScrollView>
                    <View style={{
                        height: 160,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: AppSetting.MAIN_COLOR
                    }}>
                        <Text style={{
                            fontSize: 26,
                            color: 'white'
                        }}>￥ {AppSetting.parseBalance(this.props.user.userMoney)}</Text>
                        <Text style={{fontSize: 15, color: 'white', marginTop: AppSetting.NORMAL_MARGIN}}>账户余额</Text>
                    </View>
                    <InputView
                        align={'top'}
                        editable={false}
                        name={'认证账户'}
                        value={this.props.real.realName}
                    />
                    <InputView
                        align={'center'}
                        editable={false}
                        name={'认证银行'}
                        value={this.props.real.bankName}
                    />
                    <InputView
                        align={'center'}
                        editable={false}
                        name={'认证账号'}
                        value={this.props.real.bankCard}
                    />
                    <InputView
                        align={'bottom'}
                        name={'支付密码'}
                        hintText={'请输入支付密码'}
                        isPassword={true}
                        onChangeText={(text) => this.setState({password: text})}
                    />


                    <View style={styles.amountData}>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>提现金额</Text>
                        </View>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>￥</Text>
                            <TextInput
                                style={[AppStyles.itemContentInput, {marginLeft: AppSetting.NORMAL_MARGIN, flex: 1}]}
                                placeholderTextColor={AppSetting.HINT}
                                placeholder={'请输入提现金额'}
                                underlineColorAndroid='transparent'
                                value={this.state.account}
                                onChangeText={(text) => this.setState({account: text})}
                            />

                        </View>
                    </View>
                    <Text style={[AppStyles.itemName, {
                        fontSize: 15,
                        marginTop: AppSetting.NORMAL_MARGIN,
                        paddingLeft: AppSetting.NORMAL_MARGIN,
                        paddingRight: AppSetting.NORMAL_MARGIN,
                    }]}>备注：</Text>
                    <Text style={[AppStyles.itemContent, {
                        marginTop: AppSetting.NORMAL_MARGIN,
                        marginLeft: AppSetting.CENTER_MARGIN,
                        paddingRight: AppSetting.NORMAL_MARGIN,
                        fontSize: 13,
                        lineHeight: 22
                    }]}>{note}</Text>

                </ScrollView>
                <Button style={{
                    marginHorizontal: AppSetting.CENTER_MARGIN,
                    marginTop: AppSetting.NORMAL_MARGIN,
                    marginBottom: AppSetting.NORMAL_MARGIN,
                    borderRadius: 5
                }}
                        text={'提现'}
                        onPress={this.onWithdrawalPress}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    userData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
    },
    amountData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        borderTopColor: AppSetting.SplitLine,
        borderTopWidth: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN
    },
    itemBg: {
        flexDirection: 'row',
        height: AppSetting.ITEM_HEIGHT,
        alignItems: 'center',
    },
})
