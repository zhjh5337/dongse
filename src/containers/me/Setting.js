import React, {Component} from 'react'
import {
    View
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import IconItemView from '../../components/IconItemView'
import Button from '../../components/Button'
import {ActionSheet} from 'teaset'

/**
 * 设置
 */
export default class Setting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user,
            sex: this.props.user.sex
        }

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    settingGender = () => {
        let items = [
            {title: '男', onPress: () => this.updateSex(1)},
            {title: '女', onPress: () => this.updateSex(2)},
        ];
        ActionSheet.show(items, {title: '取消'});
    }

    updateSex = (sex) => {
        this.setState({
            sex: sex
        })
        Api.updateUser(this.props.user.userId, sex)
            .then(data => {
                Toast.show('修改成功！')
            })
            .catch(e => {
                // Toast.show(e.message)
            })
    }


    loginOut = () => {
        Actions.reset('Login')
    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'设置'}
                    />
                </View>
                {/*contentIco={this.props.user.avatar === null ? require('../../../assets/images/ic_default_head.png') :*/}
                {/*{uri: this.props.user.avatar}}*/}
                <IconItemView
                    style={{marginTop: 15}}
                    align={'top'}
                    name={'头像'}
                    contentIco={this.props.user.headPic === null ? require('../../../assets/images/ic_head.png') :
                        {uri: this.props.user.headPic}}
                    onPress={() => Actions.SetAvatar()}/>

                {/*0保密  1男 2 女*/}
                <IconItemView
                    align={'center'}
                    name={'性别'}
                    content={this.props.user.sex === null || this.props.user.sex == 0
                        ? '保密' : (this.props.user.sex == 1 ? '男' : '女')}
                    onPress={() => this.settingGender()}/>

                <IconItemView
                    align={'center'}
                    name={'推荐人'}
                    content={this.props.user.inviterMobile === null ? '无' : this.props.user.inviterMobile}
                    isShowArrow={false}/>

                <IconItemView
                    align={'bottom'}
                    name={'所在地区'}
                    content={Tools.code2AreaName([this.props.user.province, this.props.user.city, this.props.user.district])}
                    isShowArrow={false}/>

                <IconItemView
                    style={{marginTop: 15}}
                    align={'top'}
                    name={'修改密码'}
                    onPress={() => Actions.ForgetPassword()}/>
                <IconItemView
                    align={'bottom'}
                    name={'支付密码'}
                    onPress={() => Actions.PayPassword()}/>

                <IconItemView
                    style={{marginTop: 15}}
                    name={'关于東銫'}
                    onPress={() => Actions.About()}/>

                <Button
                    style={{marginHorizontal: 20, marginTop: 30, borderRadius: 5}}
                    text={'退出登录'}
                    fontSize={16}
                    onPress={this.loginOut}
                />

            </View>
        );
    }
}
