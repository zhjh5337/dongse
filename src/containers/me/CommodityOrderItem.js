/**
 * Created by lilong on 2018/4/26.
 */
import React, {Component} from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'

import {AppSetting, AppStyles} from "../../components/AppStyles"
import Button from '../../components/Button'
/**
 * 商城订单订单ITEM
 */
export default class CommodityOrderItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            goodsName:this.props.data.goodsName,
            orderNum: this.props.data.orderSn,
            orderStatus: this.justifyOrder(this.props),
            goodsNum: this.props.data.goodsNum,
            price: this.props.data.finalPrice,
            type: this.props.type,
            originalImg:this.props.data.originalImg,
        }
    }

    justifyOrder(nextProps) {
        if (nextProps.type === 1) {
            if (nextProps.data.payStatus === 0) {
                return 1;
            }
            if (nextProps.data.shippingStatus === 0) {
                return 2;
            }
            if (nextProps.data.shippingStatus === 1) {
                return 3;
            }
            if (nextProps.data.orderStatus === 4) {
                return 4;
            }
        }
        else if (nextProps.type === 2) {
            return 1;
        }
        else if (nextProps.type === 3) {
            return 2;
        }
        else if (nextProps.type === 4) {
            return 3;
        }
        else if (nextProps.type === 5) {
            return 4;
        }
        else {
            return 5;
        }
    }

    componentDidMount() {
    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            goodsName:nextProps.data.goodsName,
            orderNum: nextProps.data.orderSn,
            orderStatus: this.justifyOrder(nextProps),
            goodsNum: nextProps.data.goodsNum,
            price: nextProps.data.finalPrice,
            type: nextProps.type,
            originalImg:nextProps.data.originalImg,
        })
    }

    itemView = (source, name, content, style) => {
        return (
            <View style={[{flexDirection: 'row', justifyContent: 'center'}, style]}>
                <Image
                    source={source}
                    resizeMode={'contain'}
                />
                <Text style={[AppStyles.nameText, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{name}</Text>
                <Text style={[AppStyles.contentText, {marginLeft: AppSetting.NORMAL_MARGIN, flex: 1}]}
                      numberOfLines={1}>{content}</Text>
            </View>
        )
    }

    selectView() {
        switch (this.state.orderStatus) {
            case 1:
                return (<View style={styles.bottomRightStyle}>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'取消购买'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.REDLIGHT,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.REDLIGHT}
                            fontSize={13}
                            text={'去支付'}
                            onPress={this.onRegisterPress}/>
                </View>)
            case 2:
                return (<View style={styles.bottomRightStyle}>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'查看物流'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.REDLIGHT,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.REDLIGHT}
                            fontSize={13}
                            text={'再次购买'}
                            onPress={this.onRegisterPress}/>
                </View>)
            case 3:
                return (<View style={styles.bottomRightStyle}>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'再次购买'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'查看物流'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.REDLIGHT,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.REDLIGHT}
                            fontSize={13}
                            text={'确认收货'}
                            onPress={this.onRegisterPress}/>
                </View>)
            case 4:
                return (<View style={styles.bottomRightStyle}>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'删除订单'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.OTHER,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.OTHER}
                            fontSize={13}
                            text={'评价晒单'}
                            onPress={this.onRegisterPress}/>
                    <Button style={{
                        padding: 5,
                        marginHorizontal: 5,
                        backgroundColor: 'transparent',
                        borderRadius: 4,
                        borderColor: AppSetting.REDLIGHT,
                        borderWidth: AppSetting.LineWidth,
                        height: 30
                    }}
                            color={AppSetting.REDLIGHT}
                            fontSize={13}
                            text={'再次购买'}
                            onPress={this.onRegisterPress}/>
                </View>)
        }
    }

    justifyOrderStatus(orderStatus) {

        switch (orderStatus) {
            case 1:
                return '等待付款';
            case 2:
                return '等待发货';
            case 3:
                return '等待收货';
            case 4:
                return '已完成';
            default:
                return '未知状态';
        }

    }

    render() {


        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingRight: AppSetting.NORMAL_MARGIN,
                    marginBottom: AppSetting.SMALL_MARGIN,
                }}>

                    <Text style={[AppStyles.contentTextSmall,]}>订单编号：{this.state.orderNum}</Text>
                    <Text
                        style={[AppStyles.redTextmore, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{this.justifyOrderStatus(this.state.orderStatus)}</Text>
                </View>
                <View style={styles.itemLine}/>
                <View style={styles.midStyle}>
                    <Image resizeMethod='resize' resizeMode="stretch"
                           source={{uri: this.state.originalImg}}
                           style={styles.itemImage}/>
                    <View style={styles.midRightStyle}>
                        <Text style={[AppStyles.contentText2,]}>{this.state.goodsName}</Text>
                        <Text
                            style={[AppStyles.lightTextBig, {marginTop: AppSetting.SSMALL_MARGIN}]}>数量：x{this.state.goodsNum}</Text>
                    </View>
                </View>
                <View style={styles.itemLine}/>
                <View style={styles.bottomStyle}>
                    <Text style={[AppStyles.redTextbig,]}>¥{this.state.price}</Text>
                    {this.selectView()}
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingTop: AppSetting.NORMAL_MARGIN,

        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,

    },
    midStyle: {
        flexDirection: 'row',
        paddingVertical: AppSetting.SSMALL_MARGIN,
        alignItems: 'center',
    },
    bottomStyle: {
        flexDirection: 'row',
        paddingVertical: AppSetting.SSMALL_MARGIN,
        alignItems: 'center',
    },
    midRightStyle: {
        marginHorizontal: AppSetting.SMALL_MARGIN,
        flex: 1,

    },
    bottomRightStyle: {
        marginHorizontal: AppSetting.SMALL_MARGIN,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginVertical: AppSetting.SSMALL_MARGIN,
    },
    itemImage: {
        margin: AppSetting.SMALL_MARGIN,
        height: AppSetting.SECTION_MARGIN,
        width: AppSetting.SECTION_MARGIN,
    }

})

