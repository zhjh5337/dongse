import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    Dimensions
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import InputView from "../../components/InputView"
import Button from '../../components/Button'
import ImageView from '../../components/ImageView'
import Toast from "react-native-root-toast"
import PropTypes from 'prop-types'

const noteItemWidth = (Dimensions.get('window').width - AppSetting.NORMAL_MARGIN * 4) / 4
/**
 * 实名认证
 */
export default class NewVerified extends Component {

    static propTypes = {
        isFail: PropTypes.bool,
        reason: PropTypes.string,
    };

    static defaultProps = {
        isFail: false,
        reason: undefined,
    };



    constructor(props) {
        super(props);
        this.state = {
            isPersonal: this.props.user.usertype === 1,
            name: props.isFail ? props.real.realName : '',
            enterpriseName:  props.isFail ? props.real.comName : '',
            cardNum:  props.isFail ? props.real.selfNum : this.props.user.sfcard,
            creditNum:  props.isFail ? props.real.zhizhaoCard : '',
            idcardFront: undefined,
            idcardOpposite: undefined,
            idcardPerson: undefined,
            chartered: undefined,
        }

    }

    paramsIsNull = () => {
        if (!this.state.name) {
            Toast.show(this.state.isPersonal ? '请输入真实姓名' : '请输入法人姓名');
            return true;
        }

        if (!this.state.isPersonal && !this.state.enterpriseName) {
            Toast.show('请输入企业名称');
            return true;
        }

        if (this.state.isPersonal && (!this.state.cardNum || !Tools.isIdCard(this.state.cardNum))) {
            Toast.show('请正确输入身份证号');
            return true;
        }

        if (!this.state.isPersonal && (!this.state.creditNum || !Tools.isEnterpriseCreditCode(this.state.creditNum))) {
            Toast.show('请正确输入企业信用代码');
            return true;
        }

        if (!this.state.idcardFront) {
            Toast.show('请上传身份证正面');
            return true;
        }
        if (!this.state.idcardOpposite) {
            Toast.show('请上传身份证反面');
            return true;
        }
        if (!this.state.idcardPerson) {
            Toast.show('请上传手持身份证照片');
            return true;
        }
        if (!this.state.isPersonal && !this.state.chartered) {
            Toast.show('请上传营业执照正本照');

            return true;
        }
        if (this.state.idcardFront === undefined) {
            Toast.show('请上传身份证正面照片');
            return true;
        }
        if (this.state.idcardOpposite === undefined) {
            Toast.show('请上传身份证背面照片');
            return true;
        }
        if (this.state.idcardPerson === undefined) {
            Toast.show('请上传手持身份证照片');
            return true;
        }
        if (!this.state.isPersonal && !this.state.chartered) {
            Toast.show('请上传执照照片');
            return true;
        }
        return false;
    }


    actionPress = () => {
        this.setState({
            // actionType: !this.state.actionType
        })
    }


    onCommitPress = () => {
        if (this.paramsIsNull()) {
            return
        }
        ProgressDialog.show()
        if (this.state.isPersonal) {
            Api.personCertification(this.props.isFail, this.props.user.userId, this.state.name, this.state.cardNum, this.state.idcardFront, this.state.idcardOpposite, this.state.idcardPerson, this.props.isFail ? this.props.real.realId : '')
                .then(data => {
                    ProgressDialog.hide()
                    Toast.show("实名认证信息提交成功！")
                    Actions.pop();
                })
                .catch(e => {
                    Toast.show(e.message)
                    ProgressDialog.hide()
                })
        } else {
            Api.companyCertification(this.props.isFail, this.props.user.userId, this.state.name, this.state.enterpriseName, this.state.creditCode, this.state.idcardFront, this.state.idcardOpposite, this.state.idcardPerson, this.state.chartered, this.props.isFail ? this.props.real.realId : '')
                .then(data => {
                    ProgressDialog.hide()
                    Toast.show("实名认证信息提交成功！");
                    Actions.pop();
                })
                .catch(e => {
                    Toast.show(e.message)
                    ProgressDialog.hide()
                })
        }

    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'实名认证'}/>
                </View>
                <ScrollView>
                    <InputView
                        align={'top'}
                        value={this.state.name}
                        name={this.state.isPersonal ? '真实姓名' : '法人姓名'}
                        hintText={this.state.isPersonal ? '请输入真实姓名' : '请输入法人姓名'}
                        onChangeText={(text) => this.setState({name: text})}/>

                    {
                        this.state.isPersonal ?
                            <InputView
                                align={'bottom'}
                                value={this.state.cardNum}
                                name={'身份证号'}
                                hintText={'请输入身份证号'}
                                onChangeText={(text) => this.setState({cardNum: text})}/>
                            :
                            <InputView
                                align={'center'}
                                value={this.state.enterpriseName}
                                name={'企业名称'}
                                hintText={'请输入企业名称'}
                                onChangeText={(text) => this.setState({enterpriseName: text})}/>
                    }

                    {
                        this.state.isPersonal ? null :

                            <InputView
                                align={'bottom'}
                                value={this.state.creditNum}
                                name={'信用代码'}
                                hintText={'请输入企业信用代码'}
                                onChangeText={(text) => this.setState({creditNum: text})}/>
                    }


                    {
                        this.props.isFail ?
                            <View style={styles.failBg}>
                                <View style={styles.itemBg}>
                                    <Text style={AppStyles.nameText}>失败原因</Text>
                                </View>
                                    <Text
                                        style={[AppStyles.itemContentInput, {
                                            marginBottom: AppSetting.NORMAL_MARGIN,
                                            color:AppSetting.RED,
                                            flex: 1
                                        }]}
                                    >{this.props.reason}</Text>

                            </View>
                            : null
                    }


                    <View style={{alignItems: 'center', marginTop: AppSetting.CENTER_MARGIN}}>
                        <Text
                            style={styles.itemName}>{this.state.isPersonal ? '请上传您的身份证信息' : '请上传法人身份证正反面、手持身份证'}</Text>
                        {/*source={this.props.user.avatar === null ? require('../../../assets/images/ic_default_head.png') :*/}
                        {/*{uri: this.props.user.avatar}}*/}


                        <ImageView
                            style={styles.itemImage}
                            source={this.state.idcardFront ? {uri: this.state.idcardFront} : require('../../../assets/images/ic_idcard_positive.png')}
                            onSelect={image => {
                                this.setState({
                                    idcardFront: `data:${image.mime};base64,` + image.data
                                })
                            }
                            }

                        />


                        <ImageView
                            style={styles.itemImage}
                            source={this.state.idcardOpposite ? {uri: this.state.idcardOpposite} : require('../../../assets/images/ic_idcard_obverse.png')}
                            onSelect={image =>
                                this.setState({
                                    idcardOpposite: `data:${image.mime};base64,` + image.data
                                })}
                        />

                        <ImageView
                            style={styles.itemImage}
                            source={this.state.idcardPerson ? {uri: this.state.idcardPerson} : require('../../../assets/images/ic_handheld_idcard.png')}
                            onSelect={image =>
                                this.setState({
                                    idcardPerson: `data:${image.mime};base64,` + image.data
                                })}
                        />

                        {
                            this.state.isPersonal ? null :
                                <ImageView
                                    style={styles.itemImage}
                                    source={this.state.chartered ? {uri: this.state.chartered} : require('../../../assets/images/ic_license.png')}
                                    onSelect={image =>
                                        this.setState({
                                            chartered: `data:${image.mime};base64,` + image.data
                                        })}
                                />
                        }

                    </View>

                    <View style={styles.lineBg}>
                        <View style={styles.line}/>
                        <Text style={styles.itemName}>拍摄照片要求</Text>
                        <View style={styles.line}/>
                    </View>

                    <View style={styles.noteBg}>
                        <Image source={require('../../../assets/images/ic_idcard_default.png')}
                               resizeMode={'contain'}/>
                        <Image source={require('../../../assets/images/ic_idcard_missing.png')}
                               resizeMode={'contain'}/>
                        <Image source={require('../../../assets/images/ic_idcard_blurry.png')}
                               resizeMode={'contain'}/>
                        <Image source={require('../../../assets/images/ic_idcard_flash.png')}
                               resizeMode={'contain'}/>
                    </View>

                    <Button style={{
                        marginHorizontal: AppSetting.CENTER_MARGIN,
                        marginBottom: AppSetting.CENTER_MARGIN,
                        borderRadius: 5
                    }}
                            text={'提交'}
                            onPress={this.onCommitPress}/>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    itemImage: {
        marginTop: AppSetting.NORMAL_MARGIN,
    },
    itemName: {
        color: AppSetting.DEFAULT,
        fontSize: 15
    },
    lineBg: {
        flexDirection: 'row',
        marginLeft: AppSetting.NORMAL_MARGIN,
        marginRight: AppSetting.NORMAL_MARGIN,
        marginTop: AppSetting.CENTER_MARGIN,
        alignItems: 'center',
    },
    line: {
        flex: 1,
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        margin: AppSetting.NORMAL_MARGIN
    },
    noteBg: {
        flexDirection: 'row',
        padding: AppSetting.CENTER_MARGIN,
        justifyContent: 'space-between'
    },
    noteItem: {
        width: noteItemWidth
    },

    failBg: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        borderTopColor: AppSetting.SplitLine,
        borderTopWidth: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN
    },
    itemBg: {
        flexDirection: 'row',
        height: AppSetting.ITEM_HEIGHT,
        alignItems: 'center',
    },
})

