import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import Button from '../../components/Button'


/**
 * 佣金提现
 */
export default class BrokerageWithdraw extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    onWithdrawalPress = () => {

    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'佣金提现'}
                        functionName={'明细'}
                        functionEvent={() => Actions.BrokerageDetail()}/>

                </View>
                <View style={{flex: 1}}>
                    <View style={{
                        height: 160,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: AppSetting.MAIN_COLOR
                    }}>
                        <Text style={{fontSize: 26, color: 'white'}}>￥ 5000.00</Text>
                        <Text style={{fontSize: 15, color: 'white', marginTop: AppSetting.NORMAL_MARGIN}}>账户余额</Text>
                    </View>
                    <View style={styles.userData}>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>真实姓名</Text>
                            <Text style={[AppStyles.itemContent, {marginLeft: AppSetting.NORMAL_MARGIN}]}>王二小</Text>

                        </View>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>中国银行</Text>
                            <Text style={[AppStyles.itemContent, {marginLeft: AppSetting.NORMAL_MARGIN}]}>XXXX XXXX XXXX
                                XXXX XXX</Text>

                        </View>
                    </View>

                    <View style={styles.amountData}>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>提现金额</Text>
                        </View>
                        <View style={styles.itemBg}>
                            <Text style={AppStyles.itemName}>￥</Text>
                            <TextInput
                                style={[AppStyles.itemContentInput, {marginLeft: AppSetting.NORMAL_MARGIN, flex: 1}]}
                                placeholderTextColor={AppSetting.HINT}
                                placeholder={'请输入提现金额'}
                                underlineColorAndroid="transparent"/>

                        </View>
                    </View>
                    <Text style={[AppStyles.bottomText, {
                        marginLeft: AppSetting.NORMAL_MARGIN,
                        marginTop: 6
                    }]}>最低提现金额为1元，必须为1的整数倍</Text>
                    <View style={{flex: 1}}/>
                    <Button style={{
                        marginHorizontal: AppSetting.CENTER_MARGIN,
                        marginBottom: AppSetting.CENTER_MARGIN,
                        borderRadius: 5
                    }}
                            text={'提现'}
                            onPress={this.onWithdrawalPress}/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    userData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
    },
    amountData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        borderTopColor: AppSetting.SplitLine,
        borderTopWidth: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN
    },
    itemBg: {
        flexDirection: 'row',
        height: AppSetting.ITEM_HEIGHT,
        alignItems: 'center',
    },
})
