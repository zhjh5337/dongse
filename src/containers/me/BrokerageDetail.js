import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {SegmentedView} from 'teaset'
import BrokerageDetailList from "./BrokerageDetailList"
import WithdrawDetailList from "./WithdrawDetailList"

/**
 * 佣金明细
 */
export default class BrokerageDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    onWithdrawalPress = () => {

    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'我的佣金'}/>
                </View>
                <SegmentedView
                    style={{flex: 1}}
                    barStyle={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.MAIN_COLOR}
                >
                    <SegmentedView.Sheet title={'佣金明细'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <BrokerageDetailList/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'提现记录'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <WithdrawDetailList/>
                    </SegmentedView.Sheet>
                </SegmentedView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    userData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
    },
    amountData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        borderTopColor: AppSetting.SplitLine,
        borderTopWidth: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN
    },
    itemBg: {
        flexDirection: 'row',
        height: AppSetting.ITEM_HEIGHT,
        alignItems: 'center',
    },
})
