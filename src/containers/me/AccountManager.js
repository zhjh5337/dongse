import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import IconItemView from "../../components/IconItemView"
import BalanceDetail from "./BalanceDetail";
import RechargeRecord from "./RechargeRecord";

/**
 * 资金管理
 */
export default class AccountManager extends Component {

    constructor(props) {
        super(props);
        this.state = {
            password: undefined
        }
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'资金管理'}/>

                </View>

                <View style={{
                    height: 160,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: AppSetting.MAIN_COLOR
                }}>
                    <Text style={{
                        fontSize: 26,
                        color: 'white'
                    }}>￥ {AppSetting.parseBalance(this.props.user.userMoney)}</Text>
                    <Text style={{fontSize: 15, color: 'white', marginTop: AppSetting.NORMAL_MARGIN}}>账户余额</Text>
                </View>
                <IconItemView
                    style={{marginTop: AppSetting.NORMAL_MARGIN}}
                    align={'top'}
                    name={'余额明细'}
                    onPress={() => Actions.BalanceDetail()}
                />
                <IconItemView
                    align={'center'}
                    name={'充值记录'}
                    onPress={() => Actions.RechargeRecord({userId:this.props.user.userId})}
                />
                <IconItemView
                    align={'bottom'}
                    name={'提现记录'}
                    onPress={() => Actions.CashRecord({userId:this.props.user.userId})}
                />

            </View>
        );
    }
}
const styles = StyleSheet.create({
    userData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
    },
    amountData: {
        backgroundColor: 'white',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        borderTopColor: AppSetting.SplitLine,
        borderTopWidth: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN
    },
    itemBg: {
        flexDirection: 'row',
        height: AppSetting.ITEM_HEIGHT,
        alignItems: 'center',
    },
})
