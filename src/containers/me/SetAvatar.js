import React, {Component} from 'react'

import {
    View,
    Image,
    Dimensions,
    StyleSheet,
} from 'react-native'

import ActionBar from '../../components/ActionBar'
import Button from '../../components/Button'
import {AppSetting, AppStyles} from '../../components/AppStyles';
import selectImage from 'react-native-image-crop-picker';
import * as Types from "../../actions/types";

/**
 * 设置个人头像
 */
export default class SetAvatar extends Component {

    constructor(props) {
        super(props);
        this.imageProcessing = this.imageProcessing.bind(this);
        this.selectImages = this.selectImages.bind(this);
        this.state = {
            head: undefined
        }
    }

    selectImages(isCamera: boolean) {
        return isCamera ?
            selectImage.openCamera({
                cropping: true,
                width: 500,
                height: 500,
                includeBase64: true,
                compressImageQuality: 0.7,
            }) :
            selectImage.openPicker({
                cropping: true,
                width: 500,
                height: 500,
                multiple: false,
                waitAnimationEnd: false,
                includeBase64: true,
                maxFiles: 9,
                compressImageQuality: 0.7,
            })
    }

    imageProcessing(isCamera: boolean) {
        this.selectImages(isCamera)
            .then(image => {
                this.setState({
                    head:image
                },()=> {
                    console.log('----------- ' + this.state.head.mime)
                    ProgressDialog.show()
                    Api.uploadAvatar(this.props.user.userId, `data:${this.state.head.mime};base64,` + this.state.head.data)
                        .then((data) => {
                            Toast.show('头像修改成功')
                            var user= this.props.user
                            user.headPic = data.path
                            Store.dispatch({type: Types.UPDATE_HEAD, user: user})
                            ProgressDialog.hide()
                        })
                        .catch(e => {
                            Toast.show(e.message)
                            ProgressDialog.hide()
                        })
                })
            }).catch(e => {});
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'设置头像'}/>
                </View>

                <Image
                    source={!this.props.user.headPic ? require('../../../assets/images/ic_head.png') :
                        this.state.head ? {uri: `data:${this.state.head.mime};base64,` + this.state.head.data} :  {uri: this.props.user.headPic}}
                    style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').width,
                        backgroundColor:'#F2F2F2',
                    }}
                />

                <Button text={'相册'}
                        color={AppSetting.BLACK}
                        style={styles.button}
                        onPress={() => {
                            this.imageProcessing(false)
                        }}/>

                <Button text={'拍照'}
                        color={AppSetting.BLACK}
                        style={[styles.button, {marginTop: 15}]}
                        onPress={() => {
                            this.imageProcessing(true)
                        }}/>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    button: {
        marginTop: 20,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#DADADA',
        borderWidth: 0.5,
    }
})
