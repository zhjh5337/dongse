import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import Button from '../../components/Button'
import ActionBar from '../../components/ActionBar'
import Toast from "react-native-root-toast"
import InputView from "../../components/InputView"
import VeriCodeInput from "../../components/VeriCodeInput"

/**
 * 忘记密码
 */
export default class ForgetPassword extends Component {


    constructor(props) {
        super(props)
        this.state = {
            mobile: '',
            pin: '',
            password: '',
            password2: '',
        }
    }


    paramsIsNull = () => {
        if (!this.state.mobile || !Tools.isPhone(this.state.mobile)) {
            Toast.show('请正确输入手机号');
            return true;
        }
        if (!this.state.pin || this.state.pin.length < 6) {
            Toast.show('请正确输入验证码');
            return true;
        }
        if (!this.state.password || !this.state.password2) {
            Toast.show('请正确输入密码');
            return true;
        }
        if (this.state.password < 6) {
            Toast.show('请正确输入大于6位的密码');
            return true;
        }
        if (this.state.password !== this.state.password2) {
            Toast.show('两次输入密码不一致');
            return true;
        }
        return false;
    }

    onRegisterPress = () => {
        if (this.paramsIsNull()) {
            return
        }
        ProgressDialog.show()
        Api.resetPassword(this.state.mobile, this.state.pin, this.state.password)
            .then(data => {
                Toast.show('密码设置成功')
                ProgressDialog.hide()
                Actions.pop()
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })
    }

    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'忘记密码'}/>
                </View>


                <InputView
                    style={{marginTop: 15}}
                    align={'top'}
                    value={this.state.mobile}
                    name={'手机号'}
                    hintText={'请输入手机号'}
                    onChangeText={(text) => this.setState({mobile: text})}/>

                <VeriCodeInput
                    type={2}
                    align={'center'}
                    mobile={this.state.mobile}
                    value={this.state.pin}
                    name={'验证码'}
                    hintText={'请输入验证码'}
                    onChangeText={(text) => this.setState({pin: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.password}
                    name={'密码'}
                    hintText={'请输入至少6位密码'}
                    isPassword={true}
                    onChangeText={(text) => this.setState({password: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.password2}
                    name={'确认'}
                    hintText={'请再次输入密码'}
                    isPassword={true}
                    onChangeText={(text) => this.setState({password2: text})}/>

                <Button style={{marginHorizontal: 20, marginTop: 40, borderRadius: 5}}
                        text={'提交'}
                        onPress={this.onRegisterPress}/>

            </View>
        )
    }
}
