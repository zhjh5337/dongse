import React, {Component} from 'react'
import {ImageBackground} from 'react-native'
import {
    View,
    Text,
    ScrollView,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from "../../components/ActionBar"
import Recharge from "./Recharge";

/**
 * 会员升级
 */
export default class MembershipUpgrade extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <ImageBackground source={require('../../../assets/images/upgrade_bg.png')}
                             style={{flex: 1, backgroundColor: AppSetting.MAIN_COLOR}}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'会员升级'}
                               backgroundColor={'transparent'}/>
                </View>

                <Image style={{alignSelf: 'center', width: AppSetting.ScreenWidth - 80, marginTop: 22}}
                       source={require('../../../assets/images/ic_memb_title.png')}
                       resizeMode={'contain'}/>


                <View style={{
                    height: 66,
                    width: 200,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    borderRadius: 33,
                    alignItems: 'center',
                    backgroundColor: 'rgba(0,0,0,0.3)',
                }}>
                    <Text style={{
                        alignSelf: 'center',
                        color: 'white',
                        backgroundColor: 'transparent',
                        fontSize: 30,
                    }}>￥9999.00</Text>
                </View>
                <ImageBackground
                    source={require('../../../assets/images/describe_bg.png')}
                    style={{
                        width: AppSetting.ScreenWidth - 80,
                        alignSelf: 'center',
                        paddingBottom: 20,
                        marginTop: 20,
                    }}>

                    <Image style={{marginLeft: 10, width: 100}}
                           source={require('../../../assets/images/ic_memb_title1.png')}
                           resizeMode={'center'}/>

                    <Text style={styles.item}>1、升级为业务员需支付9999元；</Text>
                    <Text style={styles.item}>2、升级业务员后即可享受会员推荐奖励。</Text>
                    <Text style={styles.item}>3、业务员有效期为一年。</Text>

                    <Image style={{marginLeft: 10, width: 100}}
                           source={require('../../../assets/images/ic_memb_title2.png')}
                           resizeMode={'center'}/>
                    <Text style={styles.item}>升级业务员后即可享受5%的会员推荐奖励。</Text>
                </ImageBackground>


                <TouchableOpacity activeOpacity={0.7}
                                  underlayColor='transparent'
                                  style={{marginTop: 35,}}
                                  onPress={() => Actions.Recharge()}>
                    <Image
                        style={{
                            alignSelf: 'center',
                            height: 45,
                            width: AppSetting.ScreenWidth - 80
                        }}
                        source={require('../../../assets/images/upgrade_but.png')}
                    />
                </TouchableOpacity>

            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    item: {
        color: AppSetting.TITLE,
        fontSize: 14,
        marginTop: 8,
        marginLeft: 10,
        backgroundColor: 'transparent'
    }
})
