import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import Button from '../../components/Button'
import ActionBar from '../../components/ActionBar'
import Toast from "react-native-root-toast"
import InputView from "../../components/InputView";

/**
 * 绑定银行卡
 */
export default class BindingBankCard extends Component {


    constructor(props) {
        super(props)
        this.state = {
            cardName: '',
            bankAddress: '',
            bankNum: '',
        }
    }


    paramsIsNull = () => {
        if (!this.state.bankAddress) {
            Toast.show('请输入开户行及地址');
            return true;
        }
        if (!this.state.bankNum) {
            Toast.show('请输入银行卡号');
            return true;
        }
        return false;
    }

    onRegisterPress = () => {
        if(this.paramsIsNull()){
            return;
        }
        ProgressDialog.show();
        Api.bindingCard(this.props.real.realId, this.state.bankAddress,this.state.bankNum)
            .then(() => {
                Toast.show('绑定成功！')
                Actions.pop()
                ProgressDialog.hide();
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide();
            })
    }

    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'绑定银行卡'}/>
                </View>

                {/*<InputView*/}
                    {/*align={'top'}*/}
                    {/*value={this.state.cardName}*/}
                    {/*name={'持卡人'}*/}
                    {/*hintText={'请输入持卡人姓名'}*/}
                    {/*onChangeText={(text) => this.setState({cardName: text})}/>*/}

                <InputView
                    align={'center'}
                    value={this.state.bankAddress}
                    name={'开户行'}
                    hintText={'请输入开户行及地址'}
                    onChangeText={(text) => this.setState({bankAddress: text})}/>

                {/*<InputView*/}
                    {/*align={'center'}*/}
                    {/*name={'卡片类型'}*/}
                    {/*hintText={'请选择银行卡类型'}*/}
                    {/*funcIco={require('../../../assets/images/ic_arrow_down.png')}*/}
                    {/*editable={false}/>*/}

                <InputView
                    align={'bottom'}
                    value={this.state.bankNum}
                    name={'银行卡号'}
                    hintText={'请输入银行卡号'}
                    onChangeText={(text) => this.setState({bankNum: text})}/>

                <Button style={{marginLeft: 20, marginRight: 20, marginTop: 40, borderRadius: 5}}
                        text={'提交'}
                        onPress={this.onRegisterPress}/>

            </View>
        )
    }
}
