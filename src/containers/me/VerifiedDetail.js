import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    Dimensions
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import Button from '../../components/Button'
import IconItemView from "../../components/IconItemView"

/**
 * 实名认证详情
 */
export default class VerifiedDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isPersonal: props.user.usertype == 1,
            name: '',
            cardNum: '',
            creditNum: '',
        }

    }

    componentDidMount() {

    }






    /**
     * 个人View
     * @returns {*}
     */
    personalView = () => {
        return (
            <View style={{marginTop: AppSetting.CENTER_MARGIN}}>
                <IconItemView
                    align={'top'}
                    isShowArrow={false}
                    name={'手机号'}
                    content={this.props.user.mobile}/>
                <IconItemView
                    align={'center'}
                    isShowArrow={false}
                    name={'所在地区'}
                    content={Tools.code2AreaName([this.props.user.province, this.props.user.city, this.props.user.district])}/>
                <IconItemView
                    align={'center'}
                    isShowArrow={false}
                    name={'身份证照'}
                    content={'已上传'}/>
                <IconItemView
                    align={'bottom'}
                    isShowArrow={false}
                    name={'银行卡'}
                    content={this.props.real.bankCard && this.props.real.bankCard !== null &&  this.props.real.bankCard !== '' ? '已绑定' : '未绑定'}/>
            </View>
        );
    }


    /**
     * 企业View
     * @returns {*}
     */
    enterpriseView = () => {
        return (
            <View style={{marginTop: AppSetting.CENTER_MARGIN}}>
                <IconItemView
                    align={'top'}
                    isShowArrow={false}
                    name={'法人姓名'}
                    content={this.props.user.name}/>
                <IconItemView
                    align={'center'}
                    isShowArrow={false}
                    name={'手机号'}
                    content={this.props.user.mobile}/>
                <IconItemView
                    align={'center'}
                    isShowArrow={false}
                    name={'所在地区'}
                    content={Tools.code2AreaName([this.props.user.province, this.props.user.city, this.props.user.district])}/>
                <IconItemView
                    align={'center'}
                    isShowArrow={false}
                    name={'营业执照'}
                    content={'已上传'}/>
                <IconItemView
                    align={'bottom'}
                    isShowArrow={false}
                    name={'银行卡号'}
                    content={this.props.real.bankCard && this.props.real.bankCard !== null &&  this.props.real.bankCard !== '' ? '已绑定' : '未绑定'}/>
            </View>
        );
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'实名认证'}/>
                </View>
                <ScrollView>
                    <View style={{height: 240}}>
                        <View style={{height: 160, backgroundColor: AppSetting.MAIN_COLOR}}/>
                        <View style={styles.userDataBg}>
                            <Button
                                style={styles.authTag}
                                color={AppSetting.GREEN}
                                fontSize={13}
                                text={'✓ 已认证'}/>
                            <Text style={[AppStyles.itemName, {marginTop: 15}]}>{this.props.real.realName}</Text>
                            <Text style={[AppStyles.itemContent, {marginTop: AppSetting.CENTER_MARGIN}]}>{this.props.real.selfNum}</Text>
                        </View>
                        <Image
                            style={styles.heardBg}
                            source={!this.props.user.headPic ? require('../../../assets/images/ic_head.png') :
                                {uri: this.props.user.headPic}}
                        />


                    </View>

                    {
                        this.state.isPersonal ? this.personalView() : this.enterpriseView()
                    }

                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    heardBg: {
        position: 'absolute',
        top: 60,
        height: 80,
        width: 80,
        left: Dimensions.get('window').width / 2 - 40,
        right: Dimensions.get('window').width / 2 - 40,
        justifyContent: 'center',
        borderRadius: 40,
        borderColor: 'white',
        borderWidth: 3
    },
    userDataBg: {
        height: 160,
        backgroundColor: 'white',
        borderRadius: 10,
        position: 'absolute',
        top: 80,
        right: AppSetting.CENTER_MARGIN,
        left: AppSetting.CENTER_MARGIN,
        borderColor: AppSetting.SplitLine,
        borderWidth: AppSetting.LineWidth,
        justifyContent: 'center',
        alignItems: 'center',
    },
    authTag: {
        height: 25,
        width: 70,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: 'white',
        borderRadius: 4,
        borderWidth: AppSetting.LineWidth,
        borderColor: AppSetting.GREEN,
        position: 'absolute',
        top: AppSetting.NORMAL_MARGIN,
        right: AppSetting.NORMAL_MARGIN,
    }


})

