import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import ListView from "../../components/DragonList"
import {AppSetting, AppStyles} from "../../components/AppStyles"

/**
 * 客户列表
 */
export default class CustomerList extends Component {

    static propTypes = {
        data: PropTypes.array,
    };

    static defaultProps = {
        data: [],
    };


    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: []
        }
    }

    componentDidMount() {
        this.loadData(true)
    }


    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {
        Api.getMyCustomersList(isRefresh, this.props.userId, isRefresh ? '' : this.state.data[this.state.data.length - 1])
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }



    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            })
            return
        }
        var oldData = this.state.data ? this.state.data : []
        var orderData = oldData.concat(data.dataList.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.rownum == item.rownum
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    }


    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }


    render() {
        return (
            <ListView
                style={{flex: 1}}
                data={this.state.data}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                onEndReached={this.loadMore}
                renderItem={(item) => <CustomerItemView data={item.item}/>}
                hasMoreData={this.state.hasMoreData}
                keyExtractor={(item, index) => item.key = index}
            />
        )
    }
}


/**
 * 客户ITEM
 */
class CustomerItemView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data:this.props.data,
            mobile: this.props.data.mobile,
            nickname: this.props.data.nickname,
            regTime: this.props.data.regTime,
        }
    }

    componentDidMount() {
    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            data: nextProps.data,
            mobile: nextProps.data.mobile,
            nickname: nextProps.data.nickname,
            regTime: nextProps.data.regTime,
        })
    }


    render() {
        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: AppSetting.NORMAL_MARGIN,
                    paddingRight: AppSetting.NORMAL_MARGIN,
                }}>
                    <Image
                        style={{
                            height: 40,
                            width: 40,
                            borderRadius:20
                        }}
                        resizeMode={'contain'}
                        source={this.props.data.headPic && this.props.data.headPic !== null ? {uri: this.props.data.headPic} : require('../../../assets/images/ic_default_head.png')}
                    />
                    <Text
                        style={[AppStyles.nameText, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{this.state.nickname || this.state.mobile}</Text>
                </View>
                <View style={styles.itemLine}/>
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>联系方式：<Text
                    style={AppStyles.contentText}>{this.state.mobile}</Text></Text>
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>注册区域：<Text
                    style={AppStyles.contentText}>{Tools.city2AreaName(this.state.data.city)}</Text></Text>
                <Text style={[AppStyles.nameText, {marginTop: AppSetting.NORMAL_MARGIN}]}>注册时间：<Text
                    style={AppStyles.contentText}>{Tools.timestampToTime(this.state.regTime)}</Text></Text>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingTop: AppSetting.NORMAL_MARGIN,
        paddingBottom: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemHead: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: AppSetting.NORMAL_MARGIN,
    }
})

