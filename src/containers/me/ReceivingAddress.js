import React, {Component} from 'react'

import {
    View,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'

import ActionBar from '../../components/ActionBar'
import Button from '../../components/Button'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import ListView from "../../components/DragonList"

/**
 * 收货地址管理
 */
export default class ReceivingAddress extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: []
        }
    }

    componentDidMount() {
        ProgressDialog.show()
        this.loadData(true)
    }

    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.random) {
            this.loadData(true)
        }
    }


    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {

        Api.getAddress(isRefresh, this.props.user.userId, isRefresh ? '' : this.state.data[this.state.data.length - 1].rownum)
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        ProgressDialog.hide()
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            })
            return
        }
        var oldData = this.state.data ? this.state.data : []
        var orderData = oldData.concat(data.dataList.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.rownum == item.rownum
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    }


    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        ProgressDialog.hide()
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }


    itemView = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={styles.addressItem}>
                <View style={{flex: 1}}>
                    <Text style={{color: AppSetting.TITLE, fontSize: 15}}>{item.item.consignee}</Text>
                    {
                        item.item.isDefault === 0 ? null :

                            <Text style={{
                                fontSize: 12,
                                width: 40,
                                color: 'white',
                                backgroundColor: AppSetting.MAIN_COLOR,
                                padding: 3,
                                textAlign: 'center',
                                marginTop: 5
                            }}>默认</Text>

                    }
                </View>
                <View style={{flex: 3, marginLeft: AppSetting.NORMAL_MARGIN, marginRight: AppSetting.NORMAL_MARGIN}}>
                    <Text style={AppStyles.contentText}>{item.item.address}</Text>
                    <Text style={[AppStyles.contentTextSmall, {marginTop: 8}]}>{item.item.mobile}</Text>
                </View>


                <TouchableOpacity
                    activeOpacity={0.7}
                    underlayColor='transparent'
                    style={{alignItems: 'center'}}
                    onPress={() => Actions.AddAddress({isAdd: false, data: item.item})}>
                    <Image
                        source={require('../../../assets/images/ic_edit.png')}
                    />

                </TouchableOpacity>

            </TouchableOpacity>
        );
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'收货地址管理'}/>
                </View>

                <ListView
                    style={{flex: 1}}
                    data={this.state.data}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    onEndReached={this.loadMore}
                    renderItem={this.itemView}
                    hasMoreData={this.state.hasMoreData}
                    keyExtractor={(item, index) => item.key = index}
                />

                <Button text={'新建地址'}
                        style={styles.button}
                        onPress={() => Actions.AddAddress()}/>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    button: {
        margin: AppSetting.NORMAL_MARGIN,
        borderRadius: 5,
        borderWidth: 0.5,
    },
    addressItem: {
        flexDirection: 'row',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        backgroundColor: 'white',
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        paddingTop: AppSetting.NORMAL_MARGIN / 2,
        paddingBottom: AppSetting.NORMAL_MARGIN / 2,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
