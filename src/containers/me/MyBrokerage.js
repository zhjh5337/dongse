import React, {Component} from 'react'
import {
    View,
    Text,
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import Button from '../../components/Button'
import IconItemView from '../../components/IconItemView'

/**
 * 我的佣金
 */
export default class MyBrokerage extends Component {


    static propTypes = {
        data: PropTypes.array,
    };

    static defaultProps = {
        data: [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
        ],
    };


    constructor(props) {
        super(props)
        this.state = {}
    }

    /**
     * 判断是否实名认证
     * @returns {boolean}
     */
    isAuth = () => {
        if (!this.props.real || this.props.user.isAuth !== 2) {
            Toast.show('请先进行实名认证！')
            return false;
        }
        return true;
    }

    onWithdrawalPress = () => {
        if(!this.isAuth()){
            return;
        }
        Actions.WithdrawalAmount()
        // Actions.BrokerageWithdraw()
    }


    render() {
        return (
            <View style={{
                flex: 1,
            }}>
                <View style={{
                    height: 160,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: AppSetting.MAIN_COLOR
                }}>
                    <Text style={{fontSize: 26, color: 'white'}}>￥ 5000.00</Text>
                    <Text style={{fontSize: 15, color: 'white', marginTop: AppSetting.NORMAL_MARGIN}}>可提现佣金余额</Text>
                </View>
                <IconItemView
                    align={'top'}
                    name={'累计佣金'}
                    content={'￥300000'}
                    isShowArrow={false}/>
                <IconItemView
                    align={'center'}
                    name={'待审核'}
                    content={'￥300000'}
                    isShowArrow={false}/>
                <IconItemView
                    align={'bottom'}
                    name={'已提现'}
                    content={'￥300000'}
                    isShowArrow={false}/>
                <View style={{flex: 1}}/>
                <Button style={{
                    marginHorizontal: AppSetting.CENTER_MARGIN,
                    marginBottom: AppSetting.CENTER_MARGIN, borderRadius: 5
                }}
                        text={'申请提现'}
                        onPress={this.onWithdrawalPress}/>
            </View>
        )
    }
}


