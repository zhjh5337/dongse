import React, {Component} from 'react'
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import Button from '../../components/Button'
import ActionBar from '../../components/ActionBar'
import PropTypes from 'prop-types'
import Toast from "react-native-root-toast"
import Alipay from 'react-native-yunpeng-alipay'
/**
 * 充值
 */
export default class Recharge extends Component {

    static propTypes = {
        account: PropTypes.string,
        isVip: PropTypes.bool,
    };

    static defaultProps = {
        account: undefined,
        isVip: false,
    };

    constructor(props) {
        super(props)
        this.state = {
            isAliPay: true,
            account: props.account
        }
    }


    paramsIsNull = () => {
        if (!this.state.account || !(/^(\d+)(\.\d+)?$/.test(this.state.account))) {
            Toast.show('请正确输入充值金额')
            return true;
        }
        return false;
    }

    onRechargePress = (isAliPay) => {
        this.setState({
            isAliPay:isAliPay
        })




    }


    onPayPress = () => {
        if(!this.state.isAliPay){
            Toast.show('暂时只支持支付宝')
            return
        }
        if(this.paramsIsNull()){
            return;
        }
        ProgressDialog.show();

        Api.aliPay(this.props.user.userId, this.state.account, this.props.isVip)
            .then(data => {
                console.log(data.appOrderInfo);

                //
                // let re = {
                //     app_id: "2018050102615667",
                //     biz_content: "{\"body\":\"会员充值\",\"out_trade_no\":\"11\",\"product_code\":\"QUICK_MSECURITY_PAY\",\"subject\":\"会员充值\",\"total_amount\":\"0.01\"}",
                //     charset: "utf-8",
                //     format: "json",
                //     method: "alipay.trade.app.pay",
                //     notify_url: " http://api.linzhen.cc/pay/ailNotify",
                //     orderInfo: "app_id=2018050102615667&biz_content=%7B%22body%22%3A%22%E4%BC%9A%E5%91%98%E5%85%85%E5%80%BC%22%2C%22out_trade_no%22%3A%2211%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%2C%22subject%22%3A%22%E4%BC%9A%E5%91%98%E5%85%85%E5%80%BC%22%2C%22total_amount%22%3A%220.01%22%7D&charset=utf-8&format=json&method=alipay.trade.app.pay&notify_url=%20http%3A%2F%2Fapi.linzhen.cc%2Fpay%2FailNotify&sign_type=RSA2&timestamp=2018-05-09%2013%3A56%3A27&version=1.0&sign=Rnk7cN0MnMiT7hslXlMb33GtSigvzPmfLTWPkSYy0g1lflSKzU12RuL%2FRq%2F3WlZy8SBTh31yt5bZmjN9uIIa4JwKaY1zEkAyXxY2bRwjiG1ZEXOxl5UC6ffx3LzubrShaRGTCQoSaQ6bjtVKGx84q8di%2BVa%2Fc6PQrtffDeNTMTHC1kl1aoAR6HuM%2FvUdc5O9v28bAUz4lawu1L5oOBjB5Gdv8Quq%2F60eNsn0hacqTDIp8dGSAR4RtxBdM51EqvU%2BhdlSLZwkJ3117mgO6va2dSxhSqE7bVfhWbQS73Mq2%2FPVDnLuXv0WDsRE7A5%2FRgcZouHWAiGBTdvjGp8i6DtvLw%3D%3D",
                //     sign_type: "RSA2",
                //     timestamp: "2018-05-09 13:56:27",
                //     version: "1.0"
                // }
                // this.aliPay(re)
                this.aliPay(data.appOrderInfo.orderInfo)
                ProgressDialog.hide();
            })
            .catch(e => {
                ProgressDialog.hide();
            })
    }


    aliPay = (data) => {
        Alipay.pay(data)
            .then((data) => {
                console.log('---------aliPay success -----------' + JSON.stringify(data))
            })
            .catch(e => {
                console.log('---------aliPay error -----------' + JSON.stringify(e))
            } )

    }

    IconItemView(iconPath, name, onPress, isShowSelect) {
        return (
            <TouchableOpacity activeOpacity={0.7} underlayColor='transparent' onPress={onPress}>
                <View style={styles.background}>

                    <Image source={iconPath}/>

                    <Text style={{
                        color: AppSetting.BLACK,
                        fontSize: 15,
                        flex: 1,
                        justifyContent: 'flex-start',
                        marginLeft: AppSetting.NORMAL_MARGIN
                    }}>{name}</Text>

                    {isShowSelect ? <Image source={require('../../../assets/images/ic_select.png')}/>
                        : <Image source={require('../../../assets/images/ic_un_select.png')}/>}
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'充值'}/>
                </View>

                <Text style={[AppStyles.nameText, {margin: AppSetting.NORMAL_MARGIN}]}>请选择支付方式</Text>

                {this.IconItemView(require('../../../assets/images/ic_by_zhifbao.png'),
                    '支付宝支付', () => this.onRechargePress(true), this.state.isAliPay)}

                <View style={{
                    backgroundColor: AppSetting.SplitLine,
                    height: AppSetting.LineWidth,
                    paddingLeft: AppSetting.NORMAL_MARGIN
                }}/>

                {this.IconItemView(require('../../../assets/images/ic_by_wechat.png'),
                    '微信支付', () => this.onRechargePress(false), !this.state.isAliPay)}
                <View style={{backgroundColor: AppSetting.SplitLine, height: AppSetting.LineWidth}}/>


                <View style={styles.inputBg}>
                    <Text style={AppStyles.itemName}>请输入充值金额</Text>

                    <View style={{flexDirection: 'row', marginTop: AppSetting.NORMAL_MARGIN}}>
                        <Text style={AppStyles.itemName}>￥ </Text>
                        <TextInput
                            style={[AppStyles.itemContent, {flex: 1, padding:0}]}
                            placeholderTextColor={AppSetting.HINT}
                            placeholder={'请输入充值金额'}
                            underlineColorAndroid='transparent'
                            value={this.state.account}
                            onChangeText={(text) => this.setState({account: text})}
                        />


                    </View>

                    <View style={{
                        marginTop: 5,
                        height: AppSetting.LineWidth,
                        backgroundColor: AppSetting.SplitLine
                    }}/>

                </View>

                <Button style={{marginHorizontal: 20, marginTop: 40, borderRadius: 5}}
                        text={'立即充值'}
                        onPress={this.onPayPress}/>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    background: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: AppSetting.NORMAL_MARGIN,
        backgroundColor: 'white',
    },
    separatorTop: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        right: 0,
        top: 0,
        left: 0,
        backgroundColor: AppSetting.SplitLine,
    },
    inputBg: {
        marginTop: AppSetting.NORMAL_MARGIN,
        padding: AppSetting.NORMAL_MARGIN,
        backgroundColor: 'white',
        borderTopWidth: AppSetting.LineWidth,
        borderBottomWidth: AppSetting.LineWidth,
        borderTopColor: AppSetting.SplitLine,
        borderBottomColor: AppSetting.SplitLine,
    }
})

