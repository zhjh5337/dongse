import React, {Component} from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'

import {AppSetting, AppStyles} from "../../components/AppStyles"

/**
 * 订单ITEM
 */
export default class ConsumerOrderItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            typeName: this.props.data.typeName,
            totalAmount: this.props.data.totalAmount,
            selfNum: this.props.data.selfNum,
            addTime: this.props.data.addTime,
            address: this.props.data.address,
            baodanSn: this.props.data.baodanSn,
            baodanType: this.props.data.baodanType,
            baodanStatus: this.props.data.baodanStatus,
        }
    }

    componentDidMount() {
    }


    componentWillUnmount() {

    }

    itemView = (source, name, content, style) => {
        return (
            <View style={[{flexDirection: 'row', justifyContent: 'center'}, style]}>
                <Image
                    source={source}
                    resizeMode={'contain'}
                />
                <Text style={[AppStyles.nameText, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{name}</Text>
                <Text style={[AppStyles.contentText, {marginLeft: AppSetting.NORMAL_MARGIN, flex: 1}]}
                      numberOfLines={1}>{content}</Text>
            </View>
        )
    }


    render() {

        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => Actions.RecordedDetail({
                    data: this.props.data
                })}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: AppSetting.NORMAL_MARGIN,
                    paddingRight: AppSetting.NORMAL_MARGIN,
                }}>
                    <Image
                        style={{
                            height: 40,
                            width: 40
                        }}
                        resizeMode={'contain'}
                        source={Tools.getConsumeTypeImage(this.state.baodanType)}
                    />
                    <Text
                        style={[AppStyles.nameText, {marginLeft: AppSetting.NORMAL_MARGIN}]}>{this.state.typeName}</Text>
                </View>
                <View style={styles.itemLine}/>
                {this.itemView(require('../../../assets/images/ic_account.png'), '金额', this.state.totalAmount)}
                {/*{this.itemView(require('../../../assets/images/ic_idcard.png'), '身份证号', '10011231231231231231', {marginTop: AppSetting.NORMAL_MARGIN})}*/}
                {this.itemView(require('../../../assets/images/ic_time.png'), '报单时间', Tools.timestampToTime(this.state.addTime), {marginTop: AppSetting.NORMAL_MARGIN})}
                {this.itemView(require('../../../assets/images/ic_address.png'), '签约地点', this.state.address, {marginTop: AppSetting.NORMAL_MARGIN})}
                <View style={styles.itemLine}/>
                {this.itemView(require('../../../assets/images/ic_num.png'), '订单编号', this.state.baodanSn)}
                <Image
                    style={{
                        height: 70,
                        position: 'absolute',
                        right: AppSetting.NORMAL_MARGIN,
                    }}
                    source={Tools.getOrderTypeImage(this.state.baodanStatus)}
                />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingTop: AppSetting.NORMAL_MARGIN,
        paddingBottom: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    }

})

