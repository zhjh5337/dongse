import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ListView from "../../components/DragonList"
import ActionBar from "../../components/ActionBar"

/**
 * 充值记录
 */
export default class RechargeRecord extends Component {



    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: [],
        }
    }

    componentDidMount() {
        this.loadData(true)
    }


    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {
        Api.rechargeDetail(isRefresh, this.props.userId, isRefresh ? '' : this.state.data[this.state.data.length - 1].rownum)
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            });
            return
        }
        let oldData = this.state.data ? this.state.data : [];
        let orderData = oldData.concat(data.dataList.filter((item) => {
            let findIndex = oldData.findIndex((oldItem) => {
                return oldItem.orderId === item.orderId
            });
            return findIndex === -1
        }));
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    };



    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }



    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'充值记录'}/>
                </View>
                <ListView
                    style={{flex: 1}}
                    data={this.state.data}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    onEndReached={this.loadMore}
                    renderItem={(item) => <DetailItemView data={item.item}/>}
                    hasMoreData={this.state.hasMoreData}
                    keyExtractor={(item, index) => item.key = index}
                />
            </View>
        )
    }
}


/**
 * 明细ITEM
 */
class DetailItemView extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
    }


    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{height: 60, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flex: 1, justifyContent: 'space-between'}}>
                        <Text style={AppStyles.nameText}>{this.props.data.payName}</Text>
                        <Text style={[AppStyles.lightTextBig, {}]}>状态：{this.props.data.payStatus === 0 ? '待支付' : this.props.data.payStatus === 1 ?  '充值成功' : '交易关闭'}</Text>
                        <Text style={AppStyles.contentTextSmall}>{Tools.unix2DateStr(this.props.data.ctime)}</Text>
                    </View>
                    <View style={{ justifyContent: 'center'}}>
                        <Text style={{fontSize: 17, color: AppSetting.GREEN, textAlign: 'center'}}>￥ {AppSetting.parseBalance( this.props.data.account)}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        padding: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemHead: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: AppSetting.NORMAL_MARGIN,
    }
})
