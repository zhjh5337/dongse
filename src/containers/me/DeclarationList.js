import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import ListView from "../../components/DragonList"
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ConsumerOrderItem from "./ConsumerOrderItem"

/**
 * 报单列表
 */
export default class DeclarationList extends Component {

    static propTypes = {
        data: PropTypes.array,
    };

    static defaultProps = {
        data: [],
    };


    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: []
        }
    }

    componentDidMount() {
        this.loadData(true)
    }


    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {

        Api.getConsumerOdersList({
            "userId": this.props.userId,
            "lastRowNum":isRefresh?0:this.state.data[this.state.data.length-1]
        })
            .then(response => {
                console.log(response);
                let arry = isRefresh ? response.dataList : this.state.data.concat(response.dataList);
                this.setState({
                    data: arry,
                    refreshing: false,
                })
            })
            .catch(e => {
                Toast.show(e.message);

            })
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: data.page < data.total_pages
            })
            // this.dataDispatch(Math.min(data.page, data.total_pages), this.orderFilter(data.objects))
            this.dataDispatch(Math.min(data.page, data.total_pages), (data.objects))
            return
        }
        var oldData = this.props.data ? this.props.data : []
        var orderData = oldData.concat(data.objects.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.number == item.number
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: data.page < data.total_pages,
            loadingMore: false,
        })
        this.dataDispatch(Math.min(data.page, data.total_pages), this.orderFilter(orderData))
    }


    // orderFilter = (data) => {
    //     let newData = data.filter(item => item.status == 1 || item.status == 2)
    //     newData.sort((a, b) => {
    //         if (a.status > b.status) {
    //             return 1
    //         } else if (a.status < b.status) {
    //             return -1
    //         } else {
    //             if (a.created_at < b.created_at) {
    //                 return 1;
    //             } else if (a.created_at > b.created_at) {
    //                 return -1;
    //             }
    //             return 0;
    //         }
    //     })
    //     return newData
    // }

    /**
     * 数据发送回调
     * @param pageNum
     * @param data
     */
    dataDispatch = (pageNum, data) => {

    }

    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }

    /**
     * 订单匹配成功
     */
        // orderMatchingSuccess = (orderNum) => {
        //     this.dataDispatch(this.props.pageNum, this.props.data.filter(item => item.number != orderNum))
        // }

    tapItem = (name, onPress, style) => {
        return (
            <TouchableOpacity
                style={[{
                    flex: 1,
                    height: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: AppSetting.SplitLine,
                    flexDirection: 'row'
                }, style]}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={onPress}
            >
                <Text style={AppStyles.nameText}>{name}</Text>
                <Image style={{marginLeft: 6}} source={require('../../../assets/images/ic_arrow_down.png')}/>

            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                {/*<View style={{*/}
                    {/*backgroundColor: 'white',*/}
                    {/*flexDirection: 'row',*/}
                    {/*borderColor: AppSetting.SplitLine,*/}
                    {/*borderBottomWidth: AppSetting.LineWidth,*/}
                {/*}}>*/}
                    {/*{this.tapItem('类型', () => {*/}
                    {/*})}*/}
                    {/*{this.tapItem('月份', () => {*/}
                    {/*}, {borderLeftWidth: AppSetting.LineWidth, borderRightWidth: AppSetting.LineWidth,})}*/}
                    {/*{this.tapItem('分类', () => {*/}
                    {/*})}*/}
                {/*</View>*/}
                <ListView
                    style={{flex: 1}}
                    data={this.state.data}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    onEndReached={this.loadMore}
                    renderItem={(item) => <ConsumerOrderItem data={item.item}/>}
                    hasMoreData={this.state.hasMoreData}
                    keyExtractor={(item, index) => item.key = index}
                />
            </View>
        )
    }
}
