/**
 * Created by lilong on 2018/4/26.
 */
import React, {Component} from 'react'

import ListView from "../../components/DragonList"
import CommodityOrderItem from "./CommodityOrderItem"
import Toast from "react-native-root-toast"

/**
 * 商城订单列表baseview
 */
export default class BaseCommodityOrders extends Component {


    constructor(props) {
        super(props)
        this.state = {
            data: [],
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            type: this.props.type,
            userId:this.props.userId
        }
    }


    componentDidMount() {

        this.loadData(true)
    }


    componentWillReceiveProps(nextProps) {

        this.setState({
            userId:nextProps.userId,
        })
    }


    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {

        Api.getTpOrderList(this.state.type, this.props.userId,isRefresh?0:this.state.data[this.state.data.length-1].rownum)
            .then(response => {
                let arry = isRefresh?response.dataList:this.state.data.concat(response.dataList);
                this.setState({
                    data:arry,
                    refreshing: false,
                })
            })
            .catch(e => {
                Toast.show(e.message);

            })
    };


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: data.page < data.total_pages,
            })
            this.dataDispatch(Math.min(data.page, data.total_pages), data.objects)
            return
        }
        var oldData = this.props.data ? this.props.data : []
        var orderData = oldData.concat(data.objects.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.number == item.number
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: data.page < data.total_pages,
            loadingMore: false,
        })
        this.dataDispatch(Math.min(data.page, data.total_pages), orderData)
    }

    /**
     * 数据发送回调
     * @param pageNum
     * @param data
     */
    dataDispatch = (pageNum, data) => {

    }

    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }

    render() {
        return (
            <ListView
                style={{flex: 1}}
                data={this.state.data}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                onEndReached={this.loadMore}
                renderItem={(item) => <CommodityOrderItem data={item.item} type={this.state.type}/>}
                hasMoreData={this.state.hasMoreData}
                keyExtractor={(item, index) => item.key = index}
            />
        )
    }
}
