import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ListView from "../../components/DragonList"
import ActionBar from "../../components/ActionBar"

/**
 * 提现记录
 */
export default class CashRecord extends Component {

    static propTypes = {
        data: PropTypes.array,
    };

    static defaultProps = {
        type: 0,
        data: [],
    };


    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: props.data,
            type: props.type,
        }
    }

    componentDidMount() {
        this.loadData(true)
    }


    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {
        Api.cashRecord(isRefresh, this.props.userId, isRefresh ? '' : this.state.data[this.state.data.length - 1].rownum)
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            });
            return
        }
        let oldData = this.state.data ? this.state.data : [];
        let orderData = oldData.concat(data.dataList.filter((item) => {
            let findIndex = oldData.findIndex((oldItem) => {
                return oldItem.id === item.id
            });
            return findIndex === -1
        }));
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    };


    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'提现记录'}/>
                </View>
                <ListView
                    style={{flex: 1}}
                    data={this.props.data}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    onEndReached={this.loadMore}
                    renderItem={(item) => <DetailItemView data={item.item}/>}
                    hasMoreData={this.state.hasMoreData}
                    keyExtractor={(item, index) => item.key = index}
                />
            </View>
        )
    }
}


/**
 * 明细ITEM
 */
class DetailItemView extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
    }


    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={styles.teamItemBg}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={() => {
                }}>
                <View style={{height: 60, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flex: 1, justifyContent: 'space-between'}}>
                        <Text style={[AppStyles.itemContent, {}]}>状态：{Tools.getCashRecordName(this.props.data.status)}</Text>
                        <Text style={AppStyles.contentTextSmall}>{Tools.unix2DateStr(this.props.data.payTime)}</Text>
                    </View>
                    <Text style={{fontSize: 17, color: AppSetting.GREEN, textAlign: 'center'}}>{this.props.data.money}</Text>
                </View>

            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    teamItemBg: {
        backgroundColor: 'white',
        borderColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
        padding: AppSetting.NORMAL_MARGIN,
    },
    itemLine: {
        backgroundColor: AppSetting.SplitLine,
        height: AppSetting.LineWidth,
        marginTop: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
    itemHead: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: AppSetting.NORMAL_MARGIN,
    }
})
