import React, {Component} from 'react'
import {
    View
} from 'react-native'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {SegmentedView} from 'teaset'
import AccountFunds from "./AccountFunds";
import CustomerList from "./CustomerList";
import DeclarationList from "./DeclarationList";


/**
 * 我的客户
 */
export default class MyCustomer extends Component {

    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            position: 0
        }
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'我的客户'}/>
                </View>
                <SegmentedView
                    style={{flex: 1}}
                    barStyle={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.MAIN_COLOR}
                    activeIndex={this.state.position}
                    onChange={position =>
                        this.setState({
                            position: position
                        })
                    }
                >
                    <SegmentedView.Sheet title={'客户列表'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <CustomerList userId={this.props.userId}/>

                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'报单列表'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <DeclarationList userId={this.props.userId}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet title={'账户资金'}
                                         titleStyle={AppStyles.tabItem}
                                         activeTitleStyle={AppStyles.tabActiveItem}>
                        <AccountFunds userId={this.props.userId}/>
                    </SegmentedView.Sheet>
                </SegmentedView>

            </View>
        );
    }
}
