import React, {Component} from 'react'
import {
    View,
    Switch,
    Text,
    StyleSheet,
} from 'react-native'
import {AppSetting, AppStyles} from '../../components/AppStyles'
import Button from '../../components/Button'
import ActionBar from '../../components/ActionBar'
import Toast from "react-native-root-toast"
import InputView from "../../components/InputView"
import AreaPicker from "../../components/AreaPicker"
import PropTypes from 'prop-types'

/**
 * 添加收货地址
 */
export default class AddAddress extends Component {

    static propTypes = {
        isAdd: PropTypes.bool,
    };

    static defaultProps = {
        isAdd: true,
    };



    constructor(props) {
        super(props)
        this.state = {
            isAdd: props.isAdd,
            data: props.data,
            showArea: false,
            name: props.data ? props.data.consignee : '',
            mobile: props.data ? props.data.mobile : '',
            areaName: undefined,
            areaCode: props.data ? [props.data.province, props.data.city, props.data.district] : [],
            address: props.data ? props.data.address : '',
            isDefault: props.data ? props.data.isDefault === 1 : false,
        }
    }

    componentDidMount() {
       if(this.state.areaCode.length !== 0){
           this.setState({
               areaName:Tools.code2AreaName(this.state.areaCode)
           })
       }
    }


    paramsIsNull = () => {
        if (!this.state.name) {
            Toast.show('请输入收货人姓名');
            return true;
        }
        if (!this.state.mobile || !Tools.isPhone(this.state.mobile)) {
            Toast.show('请正确输入手机号');
            return true;
        }
        if (this.state.areaCode.length == 0) {
            Toast.show('请选择区域');
            return true;
        }
        if (!this.state.address) {
            Toast.show('请输入详细地址');
            return true;
        }

        return false;
    }







    /**
     * 删除地址
     */
    deleteAddress = () => {
        ProgressDialog.show()
        Api.deleteAddress(this.state.data.addressId)
            .then(data => {
                Toast.show('地址删除成功')
                this.popEvent()
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })
    }

    /**
     * 添加修改地址
     */
    addAddress = () => {
        if (this.paramsIsNull()) {
            return
        }
        ProgressDialog.show()
        Api.addAddress(this.props.user.userId, this.state.name, this.state.mobile, this.state.areaCode[0], this.state.areaCode[1], this.state.areaCode[2], this.state.address, this.state.isDefault)
            .then(data => {
                Toast.show('地址添加成功')
                this.popEvent()
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })
    }

    /**
     * 更新当前地址
     */
    updateAddress = () => {
        if (this.paramsIsNull()) {
            return
        }
        Api.updateAddress(this.props.user.userId, this.state.data.addressId, this.state.name, this.state.mobile, this.state.areaCode[0], this.state.areaCode[1], this.state.areaCode[2], this.state.address, this.state.isDefault)
            .then(data => {
                Toast.show('地址修改成功')
                this.popEvent()
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })
    }


    popEvent = () => {
        ProgressDialog.hide()
        Actions.pop({refresh:({'random': Math.random()})})
    }


    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    {
                        this.state.isAdd ?
                            <ActionBar title={'新增地址'}/>
                            :
                            <ActionBar title={'编辑地址'}
                                       functionIco={require('../../../assets/images/ic_delete.png')}
                                       functionEvent={this.deleteAddress}/>
                    }
                </View>


                <InputView
                    align={'top'}
                    value={this.state.name}
                    name={'收货人'}
                    hintText={'请输入姓名'}
                    onChangeText={(text) => this.setState({name: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.mobile}
                    name={'手机号'}
                    hintText={'请输入手机号'}
                    onChangeText={(text) => this.setState({mobile: text})}/>

                <InputView
                    align={'center'}
                    value={this.state.areaName}
                    name={'所在地区'}
                    hintText={'请选择区域'}
                    editable={false}
                    funcIco={require('../../../assets/images/ic_arrow_down.png')}
                    funcOnPress={(text) => this.setState({showArea: !this.state.showArea})}/>

                <InputView
                    align={'center'}
                    value={this.state.address}
                    name={'详细地址'}
                    hintText={'请输入地址'}
                    onChangeText={(text) => this.setState({address: text})}/>

                <View style={{
                    backgroundColor: 'white',
                    height: AppSetting.ITEM_HEIGHT,
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: AppSetting.NORMAL_MARGIN,
                    paddingRight: 10,
                    justifyContent: 'space-between'
                }}>
                    <Text style={styles.itemName}>设为默认</Text>
                    <Switch
                        value={this.state.isDefault}
                        onTintColor={AppSetting.MAIN_COLOR}
                        tintColor={AppSetting.HINT}
                        onValueChange={(status) => this.setState({
                            isDefault: status
                        })}
                    />
                    <View
                        style={AppStyles.separatorBottom}/>
                </View>


                <Button style={{marginHorizontal: 20, marginTop: 40, borderRadius: 5}}
                        text={'保存地址'}
                        onPress={this.state.isAdd ? this.addAddress : this.updateAddress}/>


                <AreaPicker
                    {...this.props}
                    show={this.state.showArea}
                    closePress={show => this.setState({showArea: show})}
                    confirmPress={(areaName, areaCode) => {
                        this.setState({
                            areaName: areaName,
                            areaCode: areaCode,
                        })
                    }}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    itemName: {
        fontSize: 16,
        color: AppSetting.TITLE,
        width: 85,
        backgroundColor: 'transparent'
    },
})
