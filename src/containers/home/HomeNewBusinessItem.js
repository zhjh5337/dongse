import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import moment from "moment/moment"

/**
 * 首页热销商品ITEM
 */
export default class HomeNewBusinessItem extends Component {

    static propTypes = {
        data: PropTypes.object,
        onPress: PropTypes.func,
    };

    static defaultProps = {
    };

    constructor(props) {
        super(props)
        this.state = {
            data: props.data,
            onPress: props.onPress,
            style: props.style,
        }
    }


    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={{
                    height: 80,
                    flexDirection: 'row',
                    backgroundColor: 'white',
                    marginRight: AppSetting.NORMAL_MARGIN,
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    borderBottomWidth: AppSetting.LineWidth,
                    borderBottomColor: AppSetting.SplitLine,
                }}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <Image
                    style={{
                        height: 60,
                        width: 60,
                        marginTop: 10,
                    }}
                    resizeMethod='resize'
                    resizeMode='contain'
                    source={Tools.getConsumeTypeImage(this.props.data.baodanType)}
                />
                <View style={{
                    backgroundColor: AppSetting.SplitLine,
                    width: AppSetting.LineWidth,
                    margin: 10
                }}/>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-around',
                        marginRight: 10,
                    }}
                >

                    <Text
                        style={[AppStyles.nameText]}
                        numberOfLines={1}
                    >{this.props.data.typeName}</Text>

                    <Text
                        style={[AppStyles.contentText]}
                        numberOfLines={1}
                    >{this.props.data.address}</Text>
                    <Text
                        style={AppStyles.contentText}
                        numberOfLines={1}>
                        {Tools.unix2DateStr(this.props.data.addTime)}
                    </Text>
                </View>

                <Image
                    style={{
                        height: 60,
                        width: 60,
                    }}
                    resizeMethod='resize'
                    resizeMode='contain'
                    source={Tools.getOrderTypeImage(this.props.data.baodanStatus)}
                />

            </TouchableOpacity>
        );
    }
}
