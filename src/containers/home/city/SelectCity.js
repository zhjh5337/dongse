import React, {Component} from 'react';
import {
    View,
    DeviceEventEmitter
} from 'react-native';

import SearchBox from './SearchBox';
import SearchResult from './SearchResult';
import CityList from './IndexListView';
import {AppSetting, AppStyles} from "../../../components/AppStyles"
import ActionBar from "../../../components/ActionBar"


// 下面是数据部分
import DATA_JSON from '../../../../assets/value/city-list.json'


const NOW_CITY_LIST = [
    {
        "area_id": 610100,
        "area_name": "西安市",
        "parent_id": 610000,
        "shortName": "西安",
        "areaGrade": 2,
        "areaType": 0,
        "spellName": "xianshi"
    }
];
const ALL_CITY_LIST = DATA_JSON.allCityList;
const HOT_CITY_LIST = DATA_JSON.hotCityList;

export default class SelectCity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectOffice: props.isSelectOffice || false,
            showSearchResult: false,
            keyword: '',
            searchResultList: [],
            allCityList: ALL_CITY_LIST,
            hotCityList: HOT_CITY_LIST,
            nowCityList: [props.area]
        };
    }


    onChanegeTextKeyword(newVal) {
        if (newVal === '') {
            this.setState({showSearchResult: false});
        } else {
            // 在这里过滤数据结果
            let dataList = this.filterCityData(newVal);

            this.setState({keyword: newVal, showSearchResult: true, searchResultList: dataList});
        }
    }

    filterCityData(text) {
        let rst = [];
        for (let idx = 0; idx < ALL_CITY_LIST.length; idx++) {
            let item = ALL_CITY_LIST[idx];
            if (item.shortName.indexOf(text) === 0 || item.spellName.indexOf(text) === 0) {
                rst.push(item);
            }
        }
        return rst;
    }

    onSelectCity(cityJson) {
        console.log(cityJson);
        if (this.state.showSearchResult) {
            this.setState({showSearchResult: false, keyword: ''});
        }
        if (this.state.isSelectOffice) {
            DeviceEventEmitter.emit('selectOffice', cityJson);
        } else {
            Store.dispatch({type: Types.SET_AREA, area: cityJson})
            DeviceEventEmitter.emit('selectArea', cityJson);
        }
        Actions.pop()
    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'选择城市'}/>
                </View>
                <SearchBox
                    keyword={this.state.keyword}
                    onChanegeTextKeyword={(vv) => {
                        this.onChanegeTextKeyword(vv)
                    }}/>{this.state.showSearchResult
                ? (<SearchResult
                    keyword={this.state.keyword}
                    onSelectCity={this.onSelectCity.bind(this)}
                    searchResultList={this.state.searchResultList}/>)
                : (
                    <View style={{flex: 1}}>
                        <CityList
                            onSelectCity={this.onSelectCity.bind(this)}
                            allCityList={this.state.allCityList}
                            hotCityList={this.state.hotCityList}
                            nowCityList={this.state.nowCityList}/>
                    </View>
                )}

            </View>
        )
    }
}
