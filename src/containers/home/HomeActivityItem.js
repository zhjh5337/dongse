import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting, AppStyles} from "../../components/AppStyles"

/**
 * 首页活动ITEM
 */
export default class HomeActivityItem extends Component {

    static propTypes = {
        data: PropTypes.object,
        onPress: PropTypes.func,
    };

    static defaultProps = {
    };

    constructor(props) {
        super(props)
        this.state = {
            data: props.data,
            onPress: props.onPress,
            style: props.style,
        }
    }


    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={{
                    height: 100,
                    flexDirection: 'row',
                    backgroundColor: 'white',
                    marginTop: AppSetting.NORMAL_MARGIN,
                    padding: AppSetting.NORMAL_MARGIN,
                }}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <Image
                    style={{
                        height: 76,
                        width: 76,
                        backgroundColor: AppSetting.SplitLine
                    }}
                    resizeMethod='resize'
                    resizeMode='contain'
                    source={{uri:this.props.data.thumb}}
                />
                <View
                    style={{
                        flex:1,
                        marginLeft: AppSetting.NORMAL_MARGIN,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                    }}
                >
                    <Text
                        style={AppStyles.nameText}
                        numberOfLines={1}
                    >{this.props.data.title}</Text>

                    <Text
                        style={AppStyles.contentText}
                        numberOfLines={2}
                    >{this.props.data.description}</Text>

                    <Text
                        style={AppStyles.bottomText}
                        numberOfLines={1}
                    >{AppSetting.unix2Time(this.props.data.publishTime)}</Text>

                </View>

                <View style={{
                    backgroundColor: AppSetting.SplitLine,
                    height: AppSetting.LineWidth,
                    position: 'absolute',
                    right: AppSetting.NORMAL_MARGIN,
                    bottom: 0,
                    left: AppSetting.NORMAL_MARGIN
                }}/>
            </TouchableOpacity>
        );
    }
}
