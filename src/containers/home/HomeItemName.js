import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting} from "../../components/AppStyles";

export  default  class HomeItemName extends Component{

    static propTypes = {
        name: PropTypes.string,
        onPress: PropTypes.func,
    };

    static defaultProps = {

    };

    constructor(props) {
        super(props)
        this.state = {
            name: props.name,
            onPress: props.onPress,
            style: props.style,
        }
    }

    render(){
        return (
            <View
                style={[{
                    height: 50,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems:'center',
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    marginRight: AppSetting.NORMAL_MARGIN,
                }, this.props.style]}>
                <Text
                    style={{
                        fontSize: 16,
                        color: AppSetting.BLACK
                    }}
                >{this.props.name}</Text>

                <TouchableOpacity
                    style={{flexDirection: 'row',alignItems:'center'}}
                    activeOpacity={0.7}
                    underlayColor='transparent'
                    onPress={this.props.onPress}
                >
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#bdc8ca'
                        }}
                    >更多</Text>
                    <Image source={require('../../../assets/images/arrow_right.png')}
                           style={{tintColor: '#bdc8ca'}}/>
                </TouchableOpacity>

            </View>
        );
    }
}
