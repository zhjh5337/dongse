import React, {Component} from 'react'
import {AppStyles, AppSetting} from '../../components/AppStyles'
import ActionBar from '../../components/ActionBar'
import {QRScannerView} from '../../components/QRScanner'
import ToastUtil from "../../components/ToastUtil"
import {View} from 'react-native'

/**
 * 二维码扫描
 */
export default class QRScanning extends Component {

    constructor(props) {
        super(props);
        this.state = {
            qrCoder: undefined
        }
    }

    actionBar = () => {
        return (
            <View style={{backgroundColor: 'white'}}>
                <ActionBar
                    title={'二维码扫描'}
                />
            </View>
        );
    }


    onScanResultReceived = (data) => {
        if (!this.state.qrCoder) {
            this.setState({
                qrCoder: data
            }, () => {
                try {
                    this.parseCoder(JSON.parse(data.data))
                } catch (e) {
                    this.setState({
                        qrCoder: undefined
                    })
                    Toast.show('非法二维码，请检查后重新扫描');
                }
            })
        }
    }

    /**
     * 二维码解析
     * @param data
     */
    parseCoder = (data) => {
        if (!data.tag || !data.userId ||  !data.mobile || data.tag !== 'DongSe') {
            Toast.show('非法二维码，请检查后重新扫描');
            Actions.pop();
            return;
        }
        Actions.pop({refresh:({'referee': data.mobile})});
    }

    render() {
        return (
            <QRScannerView
                renderTopBarView={this.actionBar}
                onScanResultReceived={this.onScanResultReceived}
                borderColor={AppSetting.GREEN}
                borderWidth={1}
                cornerColor={AppSetting.GREEN}
                cornerBorderWidth={4}
                cornerBorderLength={20}
                rectHeight={220}
                rectWidth={220}
                scanBarColor={AppSetting.GREEN}
                hintText={'请将二位码置于取景框中扫描'}
                hintTextStyle={{
                    color: '#fff',
                    fontSize: 14,
                    backgroundColor: 'transparent',
                    marginTop: 60
                }}
            />
        );
    }
}
