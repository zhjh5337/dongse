import React, {Component} from 'react'
import {
    View,
    ScrollView,
    DeviceEventEmitter,
    NativeModules,
    Platform,
    Image,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import InputView from "../../components/InputView"
import Button from '../../components/Button'
import AreaPicker from '../../components/AreaPicker'
import Toast from "react-native-root-toast"
import {ActionSheet} from "teaset"
import RNFS from 'react-native-fs'

/**
 * 我要报单
 */
export default class Recorded extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showArea: false,
            buyType: undefined,
            typeName: '',
            payType: undefined,
            amount: undefined,
            idCard: this.props.real.selfNum,
            address: undefined,
            remarks: '',
            areaCode: [],
            compactImages: [],
            invoiceImages: [],
            types: [],
            signImage: undefined
        }

    }

    componentDidMount() {

        ProgressDialog.show()
        Api.orderType()
            .then(data => {
                ProgressDialog.hide()
                this.setState({
                    types: data.dataList
                })
            })
            .catch(e => {
                ProgressDialog.hide()
            })
    }


    /**
     * 购买类型
     */
    buyTypeShowList = () => {
        let items = []
        this.state.types.forEach((item, i) => {
            items.push({
                title: item.typeName,
                onPress: () => this.buyTypePress(item.typeName, item.typeId)
            })
        })
        ActionSheet.show(items, {title: '取消'});
    }

    buyTypePress = (typeName, typeID) => {
        this.setState({
            typeName: typeName,
            buyType: typeID
        })
    }


    /**
     * 购买类型
     */
    payTypeShowList = () => {
        let items = []
        AppSetting.PAY_TYPE.forEach((item, i) => {
            items.push({
                title: item.name,
                onPress: () => this.payTypePress(item)
            })
        })
        ActionSheet.show(items, {title: '取消'});
    }

    payTypePress = (item) => {
        this.setState({
            payType: item
        })
    }

    paramsIsNull = () => {
        if (!this.state.buyType) {
            Toast.show('请选择购买类型');
            return true;
        }
        if (!this.state.amount) {
            Toast.show('请输入金额');
            return true;
        }
        if (!this.state.payType) {
            Toast.show('请选择支付方式');
            return true;
        }
        if (!this.state.idCard || !Tools.isIdCard(this.state.idCard)) {
            Toast.show('请正确输入身份证号');
            return true;
        }
        if (!this.state.address) {
            Toast.show('请输入签约地点');
            return true;
        }
        return false;
    }


    onCommitPress = () => {
        if (this.paramsIsNull()) {
            return
        }
        if (!this.state.signImage) {
            Toast.show('请签名后提交');
            return
        }
        ProgressDialog.show();
        Api.addBusiness(this.props.user.userId, this.state.buyType, this.state.amount, this.state.address, this.state.signImage, this.state.remarks)
            .then(data => {
                ProgressDialog.hide()
                Actions.replace('ConsumerOrders', {position: 1})
            })
            .catch(e => {
                ProgressDialog.hide()
                Toast.show(e.message)
                Actions.pop()
            })
    }


    /**
     * 下一步
     */
    next = () => {
        Tools.signView((value) => {
            if (value !== '') {
                RNFS.readFile(value, 'base64')
                    .then((content) => {
                        this.setState({
                            signImage: `data:image/png;base64,` + content
                        })
                    })
                    .catch((err) => {
                    })
            }
        })

    }

    /**
     * 合同预览
     */
    compactPreView = () => {
        if (this.paramsIsNull()) {
            return
        }
        Tools.compactPreview(this.props.user.userId, this.state.amount, this.state.payType.name, this.state.address);
    }

    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        title={'我要报单'}
                        functionName={'合同预览'}
                        functionEvent={this.compactPreView}/>
                </View>
                <ScrollView style={{flex: 1}}>
                    <InputView
                        style={{marginTop: AppSetting.NORMAL_MARGIN}}
                        align={'top'}
                        name={'购买类型'}
                        value={this.state.typeName}
                        editable={false}
                        hintText={'请选择购买类型'}
                        funcIco={require('../../../assets/images/ic_arrow_down.png')}
                        funcOnPress={this.buyTypeShowList}
                    />

                    <InputView
                        align={'center'}
                        value={this.state.amount}
                        name={'金额'}
                        hintText={'请输入金额'}
                        onChangeText={(text) => this.setState({amount: text})}/>

                    <InputView
                        align={'center'}
                        name={'支付方式'}
                        value={this.state.payType ? this.state.payType.name : undefined}
                        editable={false}
                        hintText={'请选择支付方式'}
                        funcIco={require('../../../assets/images/ic_arrow_down.png')}
                        funcOnPress={this.payTypeShowList}
                    />

                    <InputView
                        align={'center'}
                        value={this.props.real.realName}
                        name={'被服务方'}/>

                    <InputView
                        align={'center'}
                        value={this.state.idCard}
                        name={'身份证号'}
                        hintText={'请输入身份证号'}
                        onChangeText={(text) => this.setState({idCard: text})}/>

                    <InputView
                        align={'bottom'}
                        value={this.state.address}
                        name={'签约地点'}
                        hintText={'请输入签约地点'}
                        onChangeText={(text) => this.setState({address: text})}/>

                    <InputView
                        style={{marginTop: AppSetting.NORMAL_MARGIN}}
                        value={this.state.remarks}
                        name={'备注'}
                        hintText={'填写备注信息'}
                        onChangeText={(text) => this.setState({remarks: text})}/>

                    <InputView
                        style={{marginTop: AppSetting.NORMAL_MARGIN}}
                        align={'top'}
                        name={'签名'}
                        hintText={'请点击签名'}
                        editable={false}
                        funcIco={require('../../../assets/images/arrow_right.png')}
                        funcOnPress={this.next}
                    />

                    {
                        this.state.signImage ? <View style={{
                            backgroundColor: 'white',
                            borderBottomColor: AppSetting.SplitLine,
                            borderBottomWidth: AppSetting.LineWidth,
                            paddingBottom: AppSetting.NORMAL_MARGIN,
                            height: 160,
                        }}>
                            <Image
                                style={{flex: 1}}
                                source={{uri: this.state.signImage}}
                                resizeMode={'contain'}
                            />
                        </View> : null
                    }

                </ScrollView>

                <Button style={{
                    marginTop: AppSetting.CENTER_MARGIN,
                    marginHorizontal: AppSetting.CENTER_MARGIN,
                    marginBottom: AppSetting.CENTER_MARGIN,
                    borderRadius: 5
                }}
                        text={'报单'}
                        onPress={this.onCommitPress}/>
                {/*<AreaPicker*/}
                {/*{...this.props}*/}
                {/*show={this.state.showArea}*/}
                {/*closePress={show => this.setState({showArea: show})}*/}
                {/*confirmPress={(areaName, areaCode) => {*/}
                {/*this.setState({*/}
                {/*address: areaName,*/}
                {/*areaCode: areaCode,*/}
                {/*})*/}
                {/*}}*/}
                {/*/>*/}
            </View>
        );
    }
}
