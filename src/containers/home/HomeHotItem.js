import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity, Dimensions
} from 'react-native'
import PropTypes from 'prop-types'
import {AppSetting, AppStyles} from "../../components/AppStyles"


const itemWidth = (Dimensions.get('window').width - AppSetting.NORMAL_MARGIN * 3) / 2;
const itemHeight = itemWidth;
/**
 * 首页热销商品ITEM
 */
export default class HomeHotItem extends Component {

    static propTypes = {
        id: PropTypes.number,
        name: PropTypes.string,
        ico: PropTypes.string,
        price: PropTypes.number,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        name: '',
        price: '--'
    };

    constructor(props) {
        super(props)
        this.state = {
            id: props.id,
            ico: props.ico,
            name: props.name,
            price: props.price,
            onPress: props.onPress,
            style: props.style,
        }
    }


    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        return (
            <TouchableOpacity
                style={{
                    height: itemHeight,
                    width: itemWidth,
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    marginTop: AppSetting.NORMAL_MARGIN,
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    borderColor: AppSetting.SplitLine,
                    borderWidth: AppSetting.LineWidth
                }}
                activeOpacity={0.7}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <Image
                    style={{
                        flex: 5,
                        backgroundColor: AppSetting.SplitLine
                    }}
                    source={{uri:this.props.ico}}
                />
                <View
                    style={{
                        flex: 2,
                        justifyContent: 'space-around',
                        paddingLeft:6,
                        paddingRight:6,
                    }}
                >

                    <Text
                        style={[AppStyles.contentText, {color: AppSetting.RED}]}
                        numberOfLines={1}
                    >￥{this.props.price}</Text>

                    <Text
                        style={AppStyles.bottomText}
                        numberOfLines={1}>
                        {this.props.name}
                    </Text>
                </View>

            </TouchableOpacity>
        );
    }
}
