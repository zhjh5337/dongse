import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting} from "../../components/AppStyles"
import HomeItemName from './HomeItemName'
import Item from './HomeNewBusinessItem'
import LatestOrders from "./LatestOrders";

/**
 * 首页最新出单
 */
export default class HomeNewBusiness extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: []
        }
    }


    componentDidMount() {
        Api.getNewBusiness()
            .then(data => {
                let items = [];
                data.dataList.forEach((item, i) => {
                    items.push(<Item
                        key={i}
                        data={item}
                        onPress={() => Actions.RecordedDetail({
                            data: item,
                            isPublic: true
                        })}/>)
                })
                this.setState({
                    datas: items
                })
            })
            .catch(e => {
            })
    }

    componentWillUnmount() {

    }


    render() {
        return (
            <View
                style={{
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    marginTop: AppSetting.NORMAL_MARGIN,
                }}
            >
                <HomeItemName
                    name={'最新出单'}
                    onPress={() => Actions.LatestOrders()}/>
                <View style={{
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    marginRight: AppSetting.NORMAL_MARGIN,
                    height: AppSetting.LineWidth,
                    backgroundColor: AppSetting.SplitLine,
                }}/>
                {
                    this.state.datas
                }
            </View>
        );
    }
}
