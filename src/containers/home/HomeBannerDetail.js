/**
 * Created by lilong on 2018/5/3.
 */

import React, {Component} from 'react'
import {
    View,
    Text,
    Dimensions,
    WebView
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from "../../components/ActionBar"
import PropTypes from 'prop-types'

/**
 * 首页广告详情
 */
export default class HomeBannerDetail extends Component {

    static propTypes = {

    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            data:this.props.data,
        }
        console.log(JSON.stringify(props.data))
    }

    componentDidMount() {
    }

    onNavigationStateChange = (navState) => {
        this.setState({
            backButtonEnabled: navState.canGoBack,
            forwardButtonEnabled: navState.canGoForward,
            url: navState.url,
            status: navState.title,
            loading: navState.loading,
            scalesPageToFit: this.state.scalesPageToFit
        });
    };
    onShouldStartLoadWithRequest = (event) => {
        // 允许为webview发起的请求运行一个自定义的处理函数。返回true或false表示是否要继续执行响应的请求   ios
        return true;
    };


    render() {


        let bbq = "script.content=\"width=width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0\";";
        let bbq2 = `var script = document.createElement('meta');
                    script.name = 'viewport';
                    ${bbq}
                    document.getElementsByTagName('head')[0].appendChild(script);
                   
                    `;

        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'广告详情'}/>
                </View>
                <Text style={[AppStyles.nameText,{textAlign:'center'}]} numberOfLines={1}>{this.state.data.name}</Text>
                <Text style={[AppStyles.bottomText, {marginTop: AppSetting.NORMAL_MARGIN}]}>{this.state.data.time}</Text>
                <View style={{
                    height: AppSetting.LineWidth,
                    backgroundColor: AppSetting.SplitLine,
                    marginTop: AppSetting.NORMAL_MARGIN
                }}/>
                <WebView
                    style={{flex: 1,marginTop: AppSetting.NORMAL_MARGIN}}
                    automaticallyAdjustContentInsets={false}
                    source={{html: this.state.data.content, baseUrl: ' '}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                />

            </View>

        );
    }

}
