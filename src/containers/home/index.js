import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    DeviceEventEmitter
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles";
import ActionBar from '../../components/ActionBar'
import HomeBanner from './HomeBanner'
import HomeTopFunc from './HomeTopFunc'
import HomeNotice from './HomeNotice'
import HomeActivity from './HomeActivity'
import HomeHot from './HomeHot'
import HomeNewBusiness from './HomeNewBusiness'
import Location from '../../components/Location'

/**
 * 首页
 */
export default class index extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    componentDidMount() {
        DeviceEventEmitter.addListener('selectArea',(data)=>{
            console.log('======selectArea======= ' + JSON.stringify(data))
        });

        Location.getLongitudeAndLatitude();

    }

    componentWillUnmount() {

    }



    componentWillReceiveProps(nextProps){

        // this.setState({
        //     user:nextProps.user
        // })
        // if(nextProps.statusBar) {
        //     StatusBar.setBarStyle('light-content', true)
        // }
    }


    leftView = () => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={{
                    paddingHorizontal: 10,
                    alignItems: 'center',
                }}
                onPress={() => Actions.SelectCity()}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        height: 30,
                        borderRadius: 15,
                        paddingLeft: 13,
                        paddingRight: 13,
                        alignItems: 'center',
                        backgroundColor: "#5853a2"
                    }}
                >
                    <Text style={{color: '#FFFFFF', fontSize:13}}>{this.props.area.area_name}</Text>
                    <Image source={require('../../../assets/images/ic_arrow_down.png')}
                           style={{
                               tintColor: 'white',
                               marginLeft: 5
                           }}/>
                </View>


            </TouchableOpacity>
        );
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar
                        leftView={this.leftView()}
                        title={'東銫房車天下'}
                    />


                </View>
                <ScrollView style={{flex: 1}}>
                    <HomeBanner/>
                    <HomeTopFunc user={this.props.user} real={this.props.real}/>
                    <HomeNotice/>
                    <HomeActivity/>
                    <HomeHot/>
                    <HomeNewBusiness/>
                    {/*<View style={{*/}
                    {/*position: 'absolute',*/}
                    {/*right: 0,*/}
                    {/*top: 190,*/}
                    {/*left: 0*/}
                    {/*}}>*/}
                    {/*<HomeTopFunc/>*/}
                    {/*<HomeNotice/>*/}
                    {/*<HomeActivity/>*/}
                    {/*</View>*/}

                </ScrollView>


            </View>
        );
    }
}
