import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting} from "../../components/AppStyles"
import HomeItemName from './HomeItemName'
import HomeHotItem from './HomeHotItem'

/**
 * 首页热销
 */
export default class HomeHot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goods: []
        }
    }


    componentDidMount() {

        Api.getHotGoods()
            .then(data => {
                let items = [];
                data.dataList.forEach((item, i) => {
                    items.push(<HomeHotItem
                        key={i}
                        id={item.goodsId}
                        name={item.goodsName}
                        price={item.shopPrice}
                        ico={item.originalImg}/>)
                })
                this.setState({
                    goods: items
                })
            })
            .catch(e => {
                console.log('热销商品 : ' + JSON.stringify(e))
            })
    }

    componentWillUnmount() {

    }


    getItems = () => {
        let items = [];
        this.state.goods.forEach((item, i) => {
            items.push(<HomeHotItem
                key={i}
                id={item.goodsId}
                name={item.goodsName}
                price={item.shopPrice}
                ico={item.originalImg}/>)
        })
    }


    render() {
        return (
            <View
                style={{
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    marginTop: AppSetting.NORMAL_MARGIN,
                }}
            >
                <HomeItemName
                    name={'热销商品'}
                    onPress={() => {
                    }}/>
                <View style={{
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    marginRight: AppSetting.NORMAL_MARGIN,
                    height: AppSetting.LineWidth,
                    backgroundColor: AppSetting.SplitLine,
                }}/>
                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        paddingBottom: AppSetting.NORMAL_MARGIN,
                    }}
                >
                    {
                        this.state.goods
                    }
                </View>


            </View>
        );
    }
}
