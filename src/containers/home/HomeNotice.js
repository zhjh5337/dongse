import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import Swiper from 'react-native-swiper'
import {Carousel} from 'teaset'
import PropTypes from 'prop-types'

/**
 * 首页通知公告
 */
export default class HomeNotice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            data: [],
            width: Dimensions.get('window').width
        }
    }


    componentDidMount() {
        Api.getNotice()
            .then(data => {
                console.log('===========getNotice data ===========' + JSON.stringify(data))
                // let items = [];
                // data.dataList.forEach((item, i) => {
                //     items.push(<NoticeItem
                //         key={i}
                //         data={item}
                //         width={this.state.width}/>)
                // })
                this.setState({
                    // items: items,
                    data: data.dataList
                })
            })
            .catch(e => {
            })


    }

    componentWillUnmount() {

    }

    renderControl = () => {
        return (
            <Carousel.Control
                style={{alignItems: 'flex-end'}}
                dot={<Image style={{
                    height: 8,
                    width: 8,
                    borderRadius: 4,
                    borderColor: AppSetting.PLACEHOLD,
                    borderWidth: 4,
                    marginRight: 6,
                }}/>}
                activeDot={<Image style={{
                    height: 8,
                    width: 8,
                    borderRadius: 4,
                    marginRight: 6,
                    borderColor: AppSetting.MAIN_COLOR,
                    borderWidth: 4,
                }}/>}

            />
        );
    }


    render() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    height: 80,
                    backgroundColor: 'white',
                    // borderRadius: 5,
                    borderColor: AppSetting.SplitLine,
                    borderWidth: AppSetting.LineWidth,
                    marginTop: AppSetting.NORMAL_MARGIN,
                    // marginLeft: AppSetting.NORMAL_MARGIN,
                    // marginRight: AppSetting.NORMAL_MARGIN,
                    alignItems: 'center'
                }}
            >
                <Image
                    style={{
                        width: 60,
                        height: 60,
                        margin: AppSetting.NORMAL_MARGIN
                    }}
                    resizeMethod='resize'
                    resizeMode='contain'
                    source={require('../../../assets/images/ic_notice.png')}
                />

                {/*<Carousel*/}
                {/*style={{*/}
                {/*height: 80,*/}
                {/*flex: 1*/}
                {/*}}*/}
                {/*control={this.renderControl()}*/}
                {/*onLayout={e => this.setState({width: e.nativeEvent.layout.width})}*/}
                {/*>*/}
                {/*{this.state.items}*/}
                {/*</Carousel>*/}


                <Swiper
                    showsButtons={false}
                    horizontal={true}
                    loop={true}
                    autoplay
                    height={80}
                    style={{flex: 1}}
                    activeDot={<View style={styles.swipePoint}/>}
                    paginationStyle={{bottom: 3, left: null, right: 10}}
                >
                    {this.state.data ? this.state.data.map((item, index) => (
                        <NoticeItem
                            key={index}
                            data={item}
                            width={this.state.width}/>
                    )) : null}
                </Swiper>

            </View>
        );
    }
}

class NoticeItem extends Component {

    static propTypes = {
        data: PropTypes.object,
        width: PropTypes.number,
    };

    static defaultProps = {
        width: Dimensions.get('window').width,
    };

    constructor(props) {
        super(props)
        this.state = {
            data: props.data,
            width: props.width,
            // title: props.data.title,
            // content: props.data.content
        }

        console.log('======NoticeItem======= ' + JSON.stringify(this.props.data))
        console.log('======NoticeItem width ======= ' + JSON.stringify(this.props.width))
        console.log('======NoticeItem width ======= ' + Dimensions.get('window').width)
    }


    render() {
        return (
            <TouchableOpacity
                style={{
                    height: 80,
                    flex: 1,
                    // width:  this.props.width,
                    // flexDirection: 'column',
                    // paddingRight: AppSetting.NORMAL_MARGIN,
                    paddingTop: 10
                }}
                onPress={() => Actions.NoticeDetail({id: this.props.data.articleId})}>
                <Text
                    style={{
                        color: AppSetting.BLACK,
                        fontSize: 16
                    }}
                    numberOfLines={1}
                >{this.props.data.title}</Text>
                <Text
                    style={{
                        color: AppSetting.TITLE_GRAY,
                        fontSize: 14,
                        marginTop: 8
                    }}
                    numberOfLines={2}
                >{this.state.data.content}</Text>
            </TouchableOpacity>
        );
    }
}

let styles = StyleSheet.create({
    swipePoint: {
        backgroundColor: AppSetting.DEFAULT_TEXT,
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },

    bannerImage: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    }

})
