import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from "../../components/ActionBar"
import PropTypes from 'prop-types'
import Pdf from 'react-native-pdf'

/**
 * 合同预览
 */
export default class CompactPreView extends Component {

    static propTypes = {
        src: PropTypes.string,
    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            src: props.src,
        }
    }

    componentDidMount() {
    }


    render() {

        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'合同预览'}/>
                </View>

                <Pdf source={{uri:this.props.src,cache:true}}
                     style={{flex: 1}}/>
            </View>

        );
    }

}
