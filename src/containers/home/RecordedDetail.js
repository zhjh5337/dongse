import React, {Component} from 'react'
import {
    View,
    Image,
    Text,
    ScrollView,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from '../../components/ActionBar'
import IconItemView from "../../components/IconItemView"
import MultipleImageItemView from "../../components/MultipleImageItemView"
import moment from "moment/moment"
import Button from "../../components/Button"
import PropTypes from 'prop-types'


/**
 * 报单详情
 */
export default class RecordedDetail extends Component {

    static propTypes = {
        isPublic: PropTypes.bool,
    };

    static defaultProps = {
        isPublic: false,
    };

    constructor(props) {
        super(props);
        this.state = {
            data: props.data || undefined,
            buyType: '',
            amount: '',
            idCard: '',
            address: '',
            remarks: '',
            invoiceImages: [],
        }

    }

    uploadInvoice = () => {
        if (this.state.invoiceImages.length === 0) {
            Toast.show('请上传选择消费发票后上传')
            return
        }
        let invoice = ''
        this.state.invoiceImages.forEach((item, i) =>
                invoice += `data:${item.mime};base64,` + item.data + '@'
        )
        ProgressDialog.show()
        Api.uploadInvoice(this.state.data.baodanId, invoice)
            .then(data => {
                Toast.show('上传成功')
                ProgressDialog.hide()
                Actions.pop({refresh:({'random': Math.random()})})
            })
            .catch(e => ProgressDialog.hide())

    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    {
                        this.props.data.baodanStatus > 1 ?
                            <ActionBar
                                title={'报单详情'}
                                functionName={'合同预览'}
                                functionEvent={() => Actions.CompactPreView({src: this.props.data.hetongUrl})}
                            />
                            :
                            <ActionBar
                                title={'报单详情'}
                            />
                    }
                </View>
                <ScrollView>
                    <View style={{
                        backgroundColor: 'white',
                        height: 80,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingLeft: AppSetting.NORMAL_MARGIN,
                        paddingRight: AppSetting.NORMAL_MARGIN,
                    }}>
                        <Image
                            style={{
                                height: 60,
                                width: 60
                            }}
                            resizeMode={'contain'}
                            source={Tools.getConsumeTypeImage(this.state.data.baodanType)}
                        />
                        <Text style={{
                            color: AppSetting.TITLE,
                            fontSize: 20,
                            marginLeft: AppSetting.NORMAL_MARGIN
                        }}>{this.state.data.typeName}</Text>
                        <View style={{flex: 1}}/>
                        <Image
                            style={{
                                height: 80,
                                width: 80
                            }}
                            resizeMode={'contain'}
                            source={Tools.getOrderTypeImage(this.state.data.baodanStatus)}
                        />
                    </View>
                    <IconItemView
                        align={'top'}
                        name={'订单编号'}
                        content={this.state.data.baodanSn}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'center'}
                        name={'被服务方'}
                        content={this.state.data.userName}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'center'}
                        name={'身份证号'}
                        content={this.state.data.selfNum}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />


                    <IconItemView
                        align={'center'}
                        name={'消费类型'}
                        content={this.state.data.typeName}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'center'}
                        name={'金额'}
                        content={AppSetting.parseBalance(this.state.data.totalAmount)}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'center'}
                        name={'签约时间'}
                        content={moment.unix(this.state.data.addTime).format('YYYY-MM-DD HH:mm:ss')}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'center'}
                        name={'签约地点'}
                        content={this.state.data.address}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'bottom'}
                        name={'审核日期'}
                        content={moment.unix(this.state.data.csStatusTime).format('YYYY-MM-DD HH:mm:ss')}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    <IconItemView
                        align={'bottom'}
                        name={'出单日期'}
                        content={moment.unix(this.state.data.confirmTime).format('YYYY-MM-DD HH:mm:ss')}
                        contentShowLeft={false}
                        isShowArrow={false}
                    />

                    {
                        this.state.data.baodanStatus < 3 ? null :

                            this.state.data.fapiaoPathList === null || this.state.data.fapiaoPathList.length === 0 ?
                                <MultipleImageItemView
                                    style={{marginTop: AppSetting.NORMAL_MARGIN}}
                                    name={'发票上传'}
                                    value={this.state.invoiceImages}
                                    onImageChanged={images => {
                                        this.setState({
                                            invoiceImages: images
                                        })
                                    }}
                                />
                                :
                                <MultipleImageItemView
                                    style={{marginTop: AppSetting.NORMAL_MARGIN}}
                                    name={'消费发票'}
                                    isPreview={true}
                                    value={this.state.data.fapiaoPathList}
                                />
                    }


                    <IconItemView
                        style={{
                            marginTop: AppSetting.NORMAL_MARGIN,
                            marginBottom: AppSetting.NORMAL_MARGIN
                        }}
                        name={'备注'}
                        content={this.state.data.userNote}
                        contentShowLeft={false}
                        isShowArrow={false}/>


                </ScrollView>
                {
                    this.state.data.baodanStatus < 3 ? null :
                        this.state.data.fapiaoPathList === null || this.state.data.fapiaoPathList.length === 0 ?
                            <Button
                                style={{margin: AppSetting.NORMAL_MARGIN, borderRadius: 5}}
                                text={'上传'}
                                onPress={this.uploadInvoice}
                            />
                            :
                            null
                }
            </View>
        );
    }
}
