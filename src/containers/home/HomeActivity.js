import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import {AppSetting} from "../../components/AppStyles"
import HomeItemName from './HomeItemName'
import HomeActivityItem from './HomeActivityItem'

/**
 * 首页活动
 */
export default class HomeActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }


    componentDidMount() {
        Api.getActivity()
            .then(data => {
                let re = [];
                data.dataList.forEach((item, i) => {
                    re.push(<HomeActivityItem
                        key={i}
                        data={item}
                        onPress={() => Actions.NoticeDetail({id: item.articleId})}
                    />)
                })
                this.setState({
                    items: re
                })
            })
            .catch(e => {
            })
    }

    componentWillUnmount() {

    }


    render() {
        return (
            <View
                style={{
                    flexDirection: 'column',
                    backgroundColor: 'white',
                    marginTop: AppSetting.NORMAL_MARGIN,
                }}
            >
                <HomeItemName
                    name={'活动推荐'}
                    onPress={() => Actions.ActivityList()}/>

                <View style={{
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    marginRight: AppSetting.NORMAL_MARGIN,
                    height: AppSetting.LineWidth,
                    backgroundColor: AppSetting.SplitLine
                }}/>
                {
                    this.state.items
                }

            </View>
        );
    }
}
