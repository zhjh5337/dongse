import React, {Component} from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import FuncIcoView from '../../components/FuncIcoView'
import Recorded from "./Recorded";
import RecordedDetail from "./RecordedDetail";
import NewsList from "./NewsList";

/**
 * 首页顶部功能块
 */
export default class HomeTopFunc extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    componentDidMount() {

    }

    componentWillUnmount() {

    }

    /**
     * 判断是否实名认证
     * @returns {boolean}
     */
    isAuth = () => {
        if (!this.props.real || this.props.user.isAuth !== 2) {
            Toast.show('请先进行实名认证！')
            return false;
        }
        return true;
    }

    render() {
        return (
            <View
                style={{
                    flexDirection: 'column',
                    height: 181,
                    backgroundColor: 'white',
                    borderRadius: 5,
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    marginRight: AppSetting.NORMAL_MARGIN,
                }}
            >
                <View
                    style={{
                        flex: 5,
                        flexDirection: 'row'
                    }}
                >
                    <FuncIcoView
                        icoSize={55}
                        fontSize={15}
                        text={'我要录单'}
                        color={AppSetting.BLACK}
                        ico={require('../../../assets/images/ic_ludan.png')}
                        onPress={() => {
                            if (!this.isAuth()) {
                                return;
                            }
                            Actions.Recorded()
                        }}
                    />
                    <View style={{backgroundColor: AppSetting.SplitLine, width: AppSetting.LineWidth}}/>
                    <FuncIcoView
                        icoSize={55}
                        fontSize={15}
                        text={'我要提现'}
                        color={AppSetting.BLACK}
                        ico={require('../../../assets/images/ic_tixian.png')}
                        onPress={() => {
                            if (!this.isAuth()) {
                                return;
                            }
                            Actions.WithdrawalAmount()
                        }}
                    />
                </View>
                <View style={{backgroundColor: AppSetting.SplitLine, height: AppSetting.LineWidth}}/>
                <View
                    style={{
                        flex: 3,
                        flexDirection: 'row'
                    }}
                >
                    <FuncIcoView
                        text={'通知公告'}
                        ico={require('../../../assets/images/ic_info.png')}
                        onPress={() => Actions.NoticeList()}
                    />

                    <FuncIcoView
                        text={'资料认证'}
                        ico={require('../../../assets/images/ic_auth.png')}
                        onPress={() => {
                            this.props.user.isAuth === 2 ? Actions.VerifiedDetail() :
                                this.props.user.isAuth === 3 ? Actions.NewVerified({
                                    isFail: true,
                                    reason: this.props.real.reviewContent
                                }) : Actions.NewVerified()
                        }}
                    />

                    <FuncIcoView
                        text={'门店查询'}
                        ico={require('../../../assets/images/ic_shop.png')}
                        onPress={() => Actions.Nearby()}
                    />

                    <FuncIcoView
                        text={'资讯信息'}
                        ico={require('../../../assets/images/ic_news.png')}
                        onPress={() => Actions.NewsList()}
                    />
                </View>
            </View>
        );
    }
}
