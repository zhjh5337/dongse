import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import Swiper from 'react-native-swiper'
import Toast from "react-native-root-toast"
import PropTypes from 'prop-types'

/**
 * 首页Banner
 */
export default class HomeBanner extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }


    componentDidMount() {
        Api.getAdvert()
            .then(data => {
                console.log(data);
                this.setState({
                    data: data.dataList
                });
            })
            .catch(e => {
            })
    }

    componentWillUnmount() {

    }

    renderImg = () => {
        let imageViews = [];
        this.state.data.forEach((item, i) => {
            imageViews.push(<BannerItem key={i} data={item}/>)
        })
        return imageViews;
    }

    render() {
        return (
            <Swiper
                showsButtons={false}
                horizontal={true}
                loop={true}
                autoplay={true}
                height={200}
                activeDot={<View style={styles.swipePoint}/>}
                paginationStyle={
                    {bottom: 10}
                }
            >
                {this.renderImg()}
            </Swiper>
        );
    }
}

class BannerItem extends Component {

    static propTypes = {
        data: PropTypes.object,
    };

    static defaultProps = {};

    constructor(props) {
        super(props)
        this.state = {
            data: props.data,
        }
    }


    render() {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={{
                    height: 200
                }}
                onPress={() => {
                    // Actions.HomeBannerDetail({data:this.state.data});
                }}>
                <Image style={styles.bannerImage}
                       resizeMethod='resize'
                       resizeMode='stretch'
                       source={{uri: this.props.data.adCode}}/>
                {/*<View style={{*/}
                {/*position: 'absolute',*/}
                {/*bottom: 0,*/}
                {/*right: 0,*/}
                {/*alignItems: 'center',*/}
                {/*left: 0,*/}
                {/*height: 45,*/}
                {/*}}>*/}
                {/*<Text style={{textAlign:'left'}}>{this.props.data.adName}</Text>*/}
                {/*</View>*/}
            </TouchableOpacity>
        );
    }
}

let styles = StyleSheet.create({
    swipePoint: {
        backgroundColor: AppSetting.DEFAULT_TEXT,
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },

    bannerImage: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    }

})

