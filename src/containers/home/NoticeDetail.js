import React, {Component} from 'react'
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
} from 'react-native'
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from "../../components/ActionBar"
import HTMLView from 'react-native-htmlview'
import PropTypes from 'prop-types'

/**
 * 公告 新闻  活动 详情
 */
export default class NoticeDetail extends Component {


    static propTypes = {
        id: PropTypes.number,
    };

    static defaultProps = {
        id: -1,
    };


    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            title: '',
            time: '',
            content: '',
        }
    }

    componentDidMount() {
        Api.getNewsDetail(this.props.id).then(data => {
            if(data.tpArticle){
                this.setState({
                    title:data.tpArticle.title,
                    time:data.tpArticle.addTime,
                    content:data.tpArticle.content,
                })
            }
        })
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'详情'}/>
                </View>

                <ScrollView
                    style={{
                        flex: 1,
                        padding: AppSetting.NORMAL_MARGIN
                    }}>
                    <Text style={[AppStyles.nameText, {textAlign: 'center'}]}
                          numberOfLines={1}>{this.state.title}</Text>
                    <Text
                        style={[AppStyles.bottomText, {marginTop: AppSetting.NORMAL_MARGIN}]}>{Tools.unix2DateStr(this.state.time)}</Text>
                    <View style={{
                        height: AppSetting.LineWidth,
                        backgroundColor: AppSetting.SplitLine,
                        marginTop: AppSetting.NORMAL_MARGIN
                    }}/>
                    <HTMLView
                        style={{flex:1, marginTop: AppSetting.NORMAL_MARGIN, backgroundColor:AppSetting.DEFAULT_BG}}
                        value={Tools.decodeHtml(this.state.content)}
                        stylesheet={styles}
                    />

                </ScrollView>
            </View>

        );
    }

}
const styles = StyleSheet.create({
    a: {
        fontWeight: '300',
        // color: '#FF3366', // make links coloured pink
    },
});
