import React, {Component} from 'react'
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import ListView from "../../components/DragonList"
import {AppSetting, AppStyles} from "../../components/AppStyles"
import ActionBar from "../../components/ActionBar"
import moment from "moment/moment"
import * as Types from "../../actions/types"

/**
 * 通知公告列表
 */
export default class NoticeList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            loadingMore: false,
            hasMoreData: undefined,
            data: []
        }
    }

    componentDidMount() {
        this.loadData(true)
    }


    onRefresh = () => {
        this.setState({refreshing: true}, () => {
            this.loadData(true)
        })
    }

    loadMore = () => {
        if (!this.state.hasMoreData || this.state.loadingMore) {
            return
        }
        this.loadData(false)
    }

    /**
     * 数据加载
     * @param isRefresh
     */
    loadData = (isRefresh) => {
        Api.getNoticeList(isRefresh, isRefresh ? '' : this.state.data[this.state.data.length - 1].rownum)
            .then(data => this.responseHandler(isRefresh, data))
            .catch(e => this.errorHandler(isRefresh, e))
    }


    /**
     * 返回数据处理
     * @param isRefresh
     * @param data
     */
    responseHandler = (isRefresh, data) => {
        if (isRefresh) {
            this.setState({
                refreshing: false,
                hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
                data: data.dataList
            })
            return
        }
        var oldData = this.state.data ? this.state.data : []
        var orderData = oldData.concat(data.dataList.filter((item) => {
            var findIndex = oldData.findIndex((oldItem) => {
                return oldItem.rownum == item.rownum
            })
            return findIndex == -1
        }))
        this.setState({
            hasMoreData: AppSetting.PAGE_SIZE < data.dataList.length,
            loadingMore: false,
            data: orderData
        })
    }


    /**
     * 异常处理
     * @param isRefresh
     * @param error
     */
    errorHandler = (isRefresh, error) => {
        console.log('----NoticeList----' + error.message)
        this.setState(isRefresh ? {refreshing: false} : {loadingMore: false})
    }


    itemView = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={styles.itemBG}
                onPress={() => Actions.NoticeDetail({id: item.item.articleId})}>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={AppStyles.nameText} numberOfLines={1}>{item.item.title}</Text>
                    <Text style={[AppStyles.contentText, {marginTop: AppSetting.NORMAL_MARGIN}]}
                          numberOfLines={2}>{item.item.description}</Text>
                    <Text
                        style={styles.newsTime}>{moment.unix(item.item.publishTime).format('YYYY-MM-DD HH:mm:ss')}</Text>
                </View>
                <Image source={require('../../../assets/images/arrow_right.png')}
                       style={{tintColor: AppSetting.SplitLine}}/>
            </TouchableOpacity>
        );
    }


    render() {
        return (
            <View style={AppStyles.main}>
                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'通知公告'}
                               barStyle={'default'}
                               backEvent={() => Actions.pop()}/>
                </View>

                <ListView
                    style={{flex: 1}}
                    data={this.state.data}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    onEndReached={this.loadMore}
                    renderItem={this.itemView}
                    hasMoreData={this.state.hasMoreData}
                    keyExtractor={(item, index) => item.key = index}
                />
            </View>

        );
    }

}


const styles = StyleSheet.create({
    itemBG: {
        flexDirection: 'row',
        padding: AppSetting.NORMAL_MARGIN,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
    },
    newsTime: {
        fontSize: 13,
        color: '#B2B2B2',
        marginTop: 10,
        alignContent: 'center'
    },
    itemName: {
        fontSize: 17,
        color: AppSetting.BLACK
    }
})
