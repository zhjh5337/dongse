import React, {Component} from 'react'
import {ImageBackground} from 'react-native'

/**
 *
 */
export default class SplashView extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }


    componentDidMount() {
        this.timer = setTimeout(
            () => Actions.reset('Main'),
            2000
        );
    }

    componentWillUnmount() {

    }

    render() {
        return (
            <ImageBackground
                source={require('../../assets/images/loading_bg.png')}
                style={{flex: 1, backgroundColor:'white'}}
                resizeMode={'contain'}/>
        );
    }
}
