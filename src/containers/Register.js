import React, {Component} from 'react'
import {
    ScrollView,
    View,
} from 'react-native'
import {Checkbox, SegmentedBar} from 'teaset';
import {AppSetting, AppStyles} from '../components/AppStyles'
import Button from '../components/Button'
import ActionBar from '../components/ActionBar'
import Toast from "react-native-root-toast"
import InputView from "../components/InputView"
import VeriCodeInput from "../components/VeriCodeInput"
import AreaPicker from '../components/AreaPicker'
import ServiceAgreement from "./home/ServiceAgreement";

export default class Register extends Component {


    constructor(props) {
        super(props)
        this.state = {
            // mobile: '',
            // captcha: '',
            // password: '',
            // password2: '',
            // idCard: '',
            // referee: '',//推荐人
            mobile: __DEV__ ? '18591914191' : undefined,
            captcha:  __DEV__ ? '123456' : undefined,
            password: __DEV__ ?  '123456' : undefined,
            password2:  __DEV__ ? '123456' : undefined,
            idCard: __DEV__ ?  '61042919861015001X' : undefined,
            referee:  __DEV__ ? '18092289558' : undefined,//推荐人
            creditCode: '',
            areaName: '',
            areaCode: [],
            checked: false,
            registerType: 0,
            showArea: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.referee) {
           this.setState({
               referee:nextProps.referee
           })
        }
    }

    paramsIsNull = () => {
        if (!this.state.mobile || !Tools.isPhone(this.state.mobile)) {
            Toast.show('请正确输入手机号');
            return true;
        }
        if (!this.state.captcha) {
            Toast.show('请输入验证码');
            return true;
        }
        if (!this.state.password || !this.state.password2) {
            Toast.show('请输入密码');
            return true;
        }
        if (this.state.registerType === 0 ? (!this.state.idCard || !Tools.isIdCard(this.state.idCard)) : (!this.state.creditCode || !Tools.isEnterpriseCreditCode(this.state.creditCode))) {
            Toast.show(this.state.registerType === 0 ? '请正确输入身份证号' : '请正确输入企业信用代码');
            return true;
        }
        if (!this.state.areaName) {
            Toast.show('请选择所属地');
            return true;
        }
        if (!this.state.checked) {
            Toast.show('您未同意注册协议');
            return true;
        }
        if (this.state.password !== this.state.password2) {
            Toast.show('两次输入密码不一致');
            return true;
        }
        return false;
    }

    onRegisterPress = () => {
        if (this.paramsIsNull()) {
            return;
        }
        ProgressDialog.show();
        Api.register(this.state.registerType + 1, this.state.mobile, this.state.captcha, this.state.password, this.state.registerType == 0 ? this.state.idCard : this.state.creditCode, this.state.areaCode[0], this.state.areaCode[1], this.state.areaCode[2], this.state.referee)
            .then(response => {
                Toast.show('注册成功！');
                ProgressDialog.hide()
                Actions.reset('Login');
            })
            .catch(e => {
                Toast.show(e.message)
                ProgressDialog.hide()
            })

    }

    render() {
        return (
            <View style={AppStyles.main}>

                <View style={{backgroundColor: AppSetting.MAIN_COLOR}}>
                    <ActionBar title={'注册'}
                               functionIco={require('../../assets/images/ic_scan.png')}
                               functionEvent={() => Actions.QRScanning()}/>
                </View>
                <SegmentedBar
                    style={AppStyles.tabBg}
                    indicatorLineColor={AppSetting.DEFAULT_TEXT}
                    activeIndex={this.state.registerType}
                    onChange={position => {
                        this.setState({
                            registerType: position
                        })
                    }}
                >
                    <SegmentedBar.Item title='个人用户'
                                       titleStyle={AppStyles.tabItem}
                                       activeTitleStyle={AppStyles.tabActiveItem}/>
                    <SegmentedBar.Item title='企业用户'
                                       titleStyle={AppStyles.tabItem}
                                       activeTitleStyle={AppStyles.tabActiveItem}/>
                </SegmentedBar>
                <ScrollView style={{flex: 1}}>

                    <InputView
                        style={{marginTop: 15}}
                        align={'top'}
                        value={this.state.mobile}
                        name={'手机号'}
                        hintText={'请输入手机号'}
                        onChangeText={(text) => this.setState({mobile: text})}/>

                    <VeriCodeInput
                        type={1}
                        align={'center'}
                        value={this.state.captcha}
                        mobile={this.state.mobile}
                        name={'验证码'}
                        hintText={'请输入验证码'}
                        onChangeText={(text) => this.setState({captcha: text})}/>

                    <InputView
                        align={'center'}
                        value={this.state.password}
                        name={'密码'}
                        hintText={'请输入至少6位密码'}
                        isPassword={true}
                        onChangeText={(text) => this.setState({password: text})}/>

                    <InputView
                        align={'center'}
                        value={this.state.password2}
                        name={'确认'}
                        hintText={'请再次输入密码'}
                        isPassword={true}
                        onChangeText={(text) => this.setState({password2: text})}/>

                    {
                        this.state.registerType == 0 ?
                            <InputView
                                align={'center'}
                                value={this.state.idCard}
                                name={'身份证'}
                                hintText={'请输入身份证号'}
                                onChangeText={(text) => this.setState({idCard: text})}/>
                            :
                            <InputView
                                align={'center'}
                                value={this.state.creditCode}
                                name={'信用代码'}
                                hintText={'请输入企业信用代码'}
                                onChangeText={(text) => this.setState({creditCode: text})}/>
                    }


                    <InputView
                        align={'bottom'}
                        value={this.state.areaName}
                        name={'所属地'}
                        hintText={'请选择所属地'}
                        editable={false}
                        funcIco={require('../../assets/images/arrow_right.png')}
                        funcOnPress={(text) => this.setState({showArea: !this.state.showArea})}/>


                    <InputView
                        style={{marginTop: 15}}
                        value={this.state.referee}
                        name={'推荐人'}
                        hintText={'请输入推荐人手机号'}
                        maxLength={11}
                        onChangeText={(text) => this.setState({referee: text})}/>

                    <Button style={{marginHorizontal: 20, marginTop: 40, borderRadius: 5}}
                            text={'注册'}
                            onPress={this.onRegisterPress}/>

                    <View
                        style={{marginHorizontal: 20, marginTop: 15, flexDirection: 'row'}}
                    >
                        <Checkbox
                            activeOpacity={0.7}
                            underlayColor='transparent'
                            title='我已阅读并同意'
                            checked={this.state.checked}
                            onChange={isChange => this.setState({
                                checked: isChange
                            })}
                        />
                        <Button
                            style={{backgroundColor:'transparent'}}
                            text={'《東铯房車天下服务协议》'}
                            fontSize={14}
                            color={AppSetting.MAIN_COLOR}
                            onPress={() => Actions.ServiceAgreement()}
                        />
                    </View>

                </ScrollView>
                <AreaPicker
                    {...this.props}
                    show={this.state.showArea}
                    closePress={show => this.setState({showArea: show})}
                    confirmPress={(areaName, areaCode) => {
                        this.setState({
                            areaName: areaName,
                            areaCode: areaCode,
                        })
                    }}
                />
            </View>
        )
    }
}
