import {applyMiddleware, combineReducers, createStore} from 'redux'
import thunk from 'redux-thunk'
import * as Types from '../actions/types'
import CookieManager from "react-native-cookies"

const initialState = {
    currentUser: '',
    token: '',
    area: {
        "area_id": 440300,
        "area_name": "深圳市",
        "parent_id": 440000,
        "shortName": "深圳",
        "areaGrade": 2,
        "areaType": 0,
        "spellName": "shenzhenshi"
    },
};

const app = (state = initialState, action) => {
    switch (action.type) {
        case Types.SET_USER:
            userDataHandler(action.user)
            return {
                ...state,
                currentUser: action.user.userInfo,//用户信息
                partnerInfo:action.user.tpPartner,//合伙人/办事处信息
                // userReal:action.user.tpUserReal,//用户实名认证信息
                userReal:action.user.usersReal,
                token: action.user.token
            }
        case Types.SET_AREA:
            return {
                ...state,
                area: action.area
            }
        case  Types.UPDATE_HEAD:
            return{
                ...state,
                currentUser:action.user
            }
        case  Types.UPDATE_REAL:
            return{
                ...state,
                userReal: action.real
            }
        default:
            return state
    }
}


/**
 * 用户数据处理
 * @param data
 */
userDataHandler = (data) => {
    // CookieManager.set({
    //     name: 'userId',
    //     value: JSON.stringify(data.userInfo.userId),
    //     domain: 'http://dsfctx.linzhen.cc',
    //     origin: 'http://dsfctx.linzhen.cc',
    //     path: '/',
    //     version: '1',
    //     expiration: '2099-05-30T12:30:00.00-05:00'
    // }).then((res) => {
    //     console.log('CookieManager.set =>', res);
    // });

    // CookieManager.getAll()
    //     .then((res) => {
    //         console.log('CookieManager.getAll =>', res);
    //     });
    global.Token = data.token,
        Storage.save({
            key: 'token',
            data: {
                token: data.token,
            },
        });
}


const rootReducer = combineReducers({
    app,
})

const createStoreWithThunk = applyMiddleware(thunk)(createStore)
export const store = createStoreWithThunk(rootReducer)
