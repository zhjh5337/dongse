import {Actions, Scene, Stack, Tabs} from 'react-native-router-flux'
import * as React from "react"
import {connect} from "react-redux"
import {AppSetting, AppStyles} from "../components/AppStyles"

import TabIcon from '../components/TabIcon'
import SplashView from '../containers/SplashView'
import Login from '../containers/Login'
import Register from '../containers/Register'
import Home from '../containers/home'
import Nearby from '../containers/nearby'
import Goods from '../containers/goods'
import Me from '../containers/me'
import ForgetPassword from "../containers/me/ForgetPassword"
import BindingBankCard from "../containers/me/BindingBankCard"
import SetAvatar from "../containers/me/SetAvatar"
import ChangePassword from "../containers/me/ChangePassword"
import About from "../containers/me/About"
import Setting from "../containers/me/Setting"
import NoticeList from "../containers/home/NoticeList"
import NoticeDetail from "../containers/home/NoticeDetail"
import ExtensionCode from "../containers/me/ExtensionCode"
import BankCard from "../containers/me/BankCard"
import Recharge from "../containers/me/Recharge"
import QRScanning from "../containers/home/QRScanning"
import WithdrawalAmount from "../containers/me/WithdrawalAmount"
import NewVerified from "../containers/me/NewVerified"
import VerifiedDetail from "../containers/me/VerifiedDetail"
import Recorded from "../containers/home/Recorded"
import RecordedDetail from "../containers/home/RecordedDetail"
import MyReferrals from "../containers/me/MyReferrals"
import MyCustomer from "../containers/me/MyCustomer"
import BrokerageWithdraw from "../containers/me/BrokerageWithdraw"
import BrokerageDetail from "../containers/me/BrokerageDetail"
import ConsumerOrders from "../containers/me/ConsumerOrders"
import SelectCity from "../containers/home/city/SelectCity"
import AreaPicker from "../components/AreaPicker"
import CommodityOrders from "../containers/me/CommodityOrders"
import NewsList from "../containers/home/NewsList"
import ActivityList from "../containers/home/ActivityList"
import CompactPreView from "../containers/home/CompactPreView"
import HomeBannerDetail from "../containers/home/HomeBannerDetail"
import ReceivingAddress from "../containers/me/ReceivingAddress"
import AddAddress from "../containers/me/AddAddress"
import AccountManager from "../containers/me/AccountManager"
import BalanceDetail from "../containers/me/BalanceDetail"
import RechargeRecord from "../containers/me/RechargeRecord"
import CashRecord from "../containers/me/CashRecord"
import PayPassword from "../containers/me/PayPassword"
import CommissionsDetail from "../containers/me/CommissionsDetail"
import ContractManager from "../containers/me/ContractManager"
import ServiceAgreement from "../containers/home/ServiceAgreement"
import MembershipUpgrade from "../containers/me/MembershipUpgrade"
import LatestOrders from "../containers/home/LatestOrders"

const scenes = () => {
    return Actions.create(
        <Scene key="root" hideNavBar>
            <Scene key="Splash" component={SplashView} title="SplashView"/>
            <Scene key="Login" component={Login} title="Login" initial/>
            <Scene key="Register" component={Register} title="注册"/>
            <Scene key="ForgetPassword" component={ForgetPassword} title="忘记密码"/>

            <Tabs key='Main'
                  tabBarStyle={AppStyles.mainTabBarStyle}
                  inactiveTintColor={AppSetting.BLACK}
                  activeTintColor={AppSetting.DEFAULT_TEXT}
                  tabBarPosition={'bottom'}
                  showLabel={false}
                  swipeEnabled={false}
                  lazy={true}>

                <Scene key='Home'
                       title={'首页'}
                       icon={TabIcon}
                       titleStyle={{color: 'white'}}
                       inactiveIcon={require('../../assets/images/tab_home.png')}
                       activeIcon={require('../../assets/images/tab_home_select.png')}
                       component={connect(({app}) => ({area: app.area, user: app.currentUser, real: app.userReal}))(Home)}
                       position={0}
                       hideNavBar
                       initial/>


                <Scene key='Nearby'
                       title={'附近'}
                       icon={TabIcon}
                       inactiveIcon={require('../../assets/images/tab_nearby.png')}
                       activeIcon={require('../../assets/images/tab_nearby_select.png')}
                       component={connect(({app}) => ({area: app.area}))(Nearby)}
                       position={1}
                       hideNavBar
                />

                <Scene key='Goods'
                       title={'商品'}
                       icon={TabIcon}
                       inactiveIcon={require('../../assets/images/tab_goods.png')}
                       activeIcon={require('../../assets/images/tab_goods_select.png')}
                       component={Goods}
                       position={2}
                       hideNavBar/>

                <Scene key='Me'
                       title={'我的'}
                       icon={TabIcon}
                       inactiveIcon={require('../../assets/images/tab_me.png')}
                       activeIcon={require('../../assets/images/tab_me_select.png')}
                       component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(Me)}
                       position={3}
                       hideNavBar/>

            </Tabs>

            <Scene key="BindingBankCard" component={connect(({app}) => ({real: app.userReal}))(BindingBankCard)}
                   title="绑定银行卡"/>
            <Scene key="BankCard" component={connect(({app}) => ({real: app.userReal}))(BankCard)} title="我的银行卡"/>
            <Scene key="Setting" component={connect(({app}) => ({user: app.currentUser}))(Setting)} title="设置"/>
            <Scene key="SetAvatar" component={connect(({app}) => ({user: app.currentUser}))(SetAvatar)} title="设置头像"/>
            {/*<Scene key="ChangePassword" component={ChangePassword} title="更改密码"/>*/}
            <Scene key="About" component={About} title="关于"/>
            <Scene key="NoticeList" component={NoticeList} title="通知公告"/>
            <Scene key="NewsList" component={NewsList} title="新闻资讯"/>
            <Scene key="ActivityList" component={ActivityList} title="推荐活动"/>
            <Scene key="NoticeDetail" component={NoticeDetail} title="通知详情"/>
            <Scene key="ExtensionCode" component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(ExtensionCode)} title="推广码"/>
            <Scene key="Recharge" component={connect(({app}) => ({user: app.currentUser}))(Recharge)} title="充值"/>
            <Scene key="QRScanning" component={QRScanning} title="二维码扫描"/>
            <Scene key="WithdrawalAmount"
                   component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(WithdrawalAmount)}
                   title="提现"/>
            <Scene key="NewVerified" component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(NewVerified)}
                   title="实名认证"/>
            <Scene key="VerifiedDetail"
                   component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(VerifiedDetail)}
                   title="实名认证详情"/>
            <Scene key="Recorded"
                   component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(Recorded)}
                   title="我要报单"/>
            <Scene key="RecordedDetail" component={RecordedDetail} title="报单详情"/>
            <Scene key="MyReferrals" component={connect(({app}) => ({user: app.currentUser, real: app.userReal}))(MyReferrals)} title="我的推荐"/>
            <Scene key="MyCustomer" component={MyCustomer} title="我的客户"/>
            <Scene key="BrokerageWithdraw" component={BrokerageWithdraw} title="佣金提现"/>
            <Scene key="BrokerageDetail" component={BrokerageDetail} title="佣金明细"/>
            <Scene key="ConsumerOrders" component={connect(({app}) => ({user: app.currentUser}))(ConsumerOrders)}
                   title="消费订单"/>
            <Scene key="SelectCity" component={connect(({app}) => ({area: app.area}))(SelectCity)} title="城市选择"/>
            <Scene key="AreaPicker" component={AreaPicker} title="城市选择"/>
            <Scene key="HomeBannerDetail" component={connect(({app}) => ({user: app.currentUser}))(HomeBannerDetail)}
                   title="广告详情"/>
            <Scene key="CompactPreView" component={CompactPreView} title="合同预览"/>
            <Scene key="CommodityOrders" component={connect(({app}) => ({user: app.currentUser}))(CommodityOrders)} title="商品订单"/>
            <Scene key="ReceivingAddress" component={connect(({app}) => ({user: app.currentUser}))(ReceivingAddress)} title="收货地址管理"/>
            <Scene key="AddAddress" component={connect(({app}) => ({user: app.currentUser}))(AddAddress)} title="添加收货地址"/>
            <Scene key="AccountManager" component={connect(({app}) => ({user: app.currentUser}))(AccountManager)} title="资金管理"/>
            <Scene key="BalanceDetail" component={connect(({app}) => ({user: app.currentUser}))(BalanceDetail)} title="余额明细"/>
            <Scene key="RechargeRecord" component={RechargeRecord} title="充值记录"/>
            <Scene key="CashRecord" component={CashRecord} title="提现记录"/>
            <Scene key="PayPassword" component={connect(({app}) => ({user: app.currentUser}))(PayPassword)} title="支付密码"/>
            <Scene key="CommissionsDetail" component={connect(({app}) => ({user: app.currentUser}))(CommissionsDetail)} title="佣金明细"/>
            <Scene key="ContractManager" component={connect(({app}) => ({user: app.currentUser}))(ContractManager)} title="合同管理"/>
            <Scene key="ServiceAgreement" component={ServiceAgreement} title="服务协议"/>
            <Scene key="MembershipUpgrade" component={MembershipUpgrade} title="会员升级"/>
            <Scene key="LatestOrders" component={LatestOrders} title="最新出单"/>

        </Scene>
    )
}


export default scenes;
