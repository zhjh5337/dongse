import {
    Platform,
    StatusBar
} from 'react-native'
import Toast from 'react-native-root-toast'
import RNFetchBlob from 'react-native-fetch-blob'

const TEST_APP_BACKEND = 'http://117.34.109.161:8080/dsfctx-1.0.0'
const APP_BACKEND = 'http://117.34.109.161:8080/dsfctx-1.0.0'
// http://117.34.109.161:8080/dsfctx-1.0.0/swagger/index.html#/
let activityCount = 0
incrementActivityCount = () => {
    if (Platform.OS == 'android')
        return  // 只在 ios 下生效
    activityCount++
    StatusBar.setNetworkActivityIndicatorVisible(true)
}

decrementActivityCount = () => {
    if (Platform.OS == 'android')
        return  // 只在 ios 下生效
    activityCount--
    if (activityCount == 0) {
        StatusBar.setNetworkActivityIndicatorVisible(false)
    }
}

export class ApplicationError {
    constructor(code, message) {
        this.code = code
        this.message = message
    }
}

const fetchApi = (path, params = {}, options = {}) => {
    params = {...params}
    const query = Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&')
    const url = (__DEV__ ? TEST_APP_BACKEND : APP_BACKEND) + path + (query ? '?' + query : '')
    const fetchOptions = {
        ...options,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': global.Token,
        },
    }
    console.log('===========responseConfig url===========' + JSON.stringify(url))
    console.log('===========responseConfig options===========' + JSON.stringify(fetchOptions))
    incrementActivityCount()
    return fetch(url, fetchOptions)
        .then(response => responseHandle(response.status, response))
        .catch(errorHandle)
}

const uploadImage = (path, imagePath) => {
    incrementActivityCount()
    const filename = imagePath.substring(imagePath.lastIndexOf('/') + 1)
    const extensionType = imagePath.substring(imagePath.lastIndexOf('.') + 1)
    const body = [{
        name: 'attachment',
        filename: filename,
        data: RNFetchBlob.wrap(imagePath),
        type: extensionType ? 'image/' + extensionType : 'image/png'
    }]
    const params = {platform: Platform.OS}
    const query = Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&')
    const url = (__DEV__ ? TEST_APP_BACKEND : APP_BACKEND) + path + '?' + query
    return RNFetchBlob.fetch('POST', url, {
        'Content-Type': 'multipart/form-data',
    }, body)
        .then(response => responseHandle(response.respInfo.status, response))
        .catch(errorHandle)
}

const responseHandle = (statusCode, response) => {

    if(statusCode == 200){
        decrementActivityCount()
    }
    var data = JSON.parse(response._bodyInit);
    console.log('===========responseHandle data ===========' + JSON.stringify(data) )
    if (data.code == 0) {
        return Promise.resolve(response.json())
    }else if(data.code == 401){
        return Promise.reject(new ApplicationError(data.code, '授权过期，请重新登录！'))
        // throw ApplicationError(data.code, '授权过期，请重新登录！')
    }else {
        return Promise.reject(new ApplicationError(data.code, data.msg))
        // throw ApplicationError(data.code, data.msg)
        // return response.json().then((error) => {
        //     throw new ApplicationError(error.code, error.message)
        // })
        // if(data.code == 500)
    }


    switch (statusCode) {
        case 400:
            return response.json().then((error) => {
                throw new ApplicationError(error.code, error.message)
            })
        case 401:
            // store.dispatch({ type: SET_USER, user: null })
            throw new ApplicationError(statusCode, '')
        case 403:
            throw new ApplicationError(statusCode, '')
        default:
            throw new ApplicationError(statusCode, response.text())
    }
}

const errorHandle = (e) => {
    console.log('===========errorHandle===========' + JSON.stringify(e))
    decrementActivityCount()
    if (e instanceof ApplicationError) {
        return Promise.reject(e)
    } else {
        Toast.show('网络不佳，请稍后尝试')//网络不佳，请稍后尝试
        return Promise.reject(new ApplicationError(-1, e.message))
    }
}


const get = (path, params) => {
    if (params === undefined) {
        return fetchApi(path);
    }
    if (Object.prototype.toString.call(params) !== '[object Object]') {
        return fetchApi(path + "/" + params);
    }
    return fetchApi(path, params)
}

const post = (path, params) => {
    console.log(params);
    const options = {
        method: 'POST',
        body: JSON.stringify(params)
    }
    return fetchApi(path, {}, options)
}


const put = (path, params) => {
    const options = {
        method: 'PUT',
        body: JSON.stringify(params)
    }
    return fetchApi(path, {}, options)
}

const del = (path, params) => {
    const options = {
        method: 'DELETE',
        body: params? null : JSON.stringify(params)
    }
    return fetchApi(path, {}, options)
}


export {
    get,
    post,
    put,
    del,
    uploadImage
}
