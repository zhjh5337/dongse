import {uploadImage, get, post, put, del} from '.'
import {AppSetting} from "../components/AppStyles"
import * as Types from "../actions/types"
import PropTypes from 'prop-types'
import LatestOrders from "../containers/home/LatestOrders";


/**
 * 发送验证码
 * @param sendScene  1 用户注册  2找回密码 6身份验证
 * @param mobile  手机号
 * @returns {*}
 */
export const sendVerifyCode = (sendScene, mobile) => {
    return post('/sys/sendSms', {
        sendScene: sendScene,
        phoneNum: mobile,
    })
}


/**
 * 用户注册
 * @param usertype 注册类型 1:个人，2:企业
 * @param mobile 手机号
 * @param captcha 验证码
 * @param password 密码
 * @param sfcard 身份证号/企业信用代码
 * @param province
 * @param city
 * @param district
 * @param inviterMobile 推荐人
 * @returns {Promise<T> | * | Promise<*> | PromiseLike<any> | Promise<any>}
 */
export const register = (usertype, mobile, captcha, password, sfcard, province, city, district, inviterMobile) => {
    return post('/register', {
        "usertype": usertype,
        "mobile": mobile,
        "captcha": captcha,
        "password": password,
        "sfcard": sfcard,
        "province": province,
        "city": city,
        "district": district,
        "inviterMobile": inviterMobile,

    }).then((user) => {
        // Store.dispatch({type: Types.SET_USER, user: user})
        return Promise.resolve(user)
    })
}


/**
 * 用户登录
 * @param username 手机号
 * @param password 用户密码
 * @returns {*}
 */
export const login = (username, password) => {
    return post('/sys/login', {
        "mobile": username,
        "password": password
    }).then((user) => {
        Store.dispatch({
            type: Types.SET_USER,
            user: user
        })
        return Promise.resolve(user)
    })
}

/**
 * 获取广告列表
 * @returns {Promise<T> | * | Promise<*> | PromiseLike<any> | Promise<any>}
 */
export const getAdvert = () => {
    return get('/tpad/listForPhone?pid=1').then((user) => {
        return Promise.resolve(user)
    })
}


/**
 * 企业实名认证
 * @param userType 1 个人 2 企业
 * @param userId
 * @param realName 姓名
 * @param selfNum 身份证号
 * @param frontOfIdCardBase64 身份证正面
 * @param reverseOfIdCardBase64 身份证反面
 * @param shouchiCardBase64 手持身份证
 * @param zhizhaoCardBase64 营业执照
 * @returns {Promise<T> | * | Promise<*> | PromiseLike<any> | Promise<any>}
 */
export const companyCertification = (isUpdate, userId, realName, comName, zhizhaoCard, frontOfIdCardBase64, reverseOfIdCardBase64, shouchiCardBase64, zhizhaoCardBase64, realId) => {
    return post(isUpdate ? '/tpusersreal/update' : '/tpusersreal/save', {
        "userId": userId,
        "userType": 2,
        "realName": realName,
        "comName": comName,
        "zhizhaoCard": zhizhaoCard,
        "frontOfIdCardBase64": frontOfIdCardBase64,
        "reverseOfIdCardBase64": reverseOfIdCardBase64,
        "shouchiCardBase64": shouchiCardBase64,
        "zhizhaoCardBase64": zhizhaoCardBase64 === undefined ? '' : zhizhaoCardBase64,
        "realId": realId,
    }).then((user) => {

        return Promise.resolve(user)
    })
}

/**
 * 个人实名认证
 * @param userId 用户ID
 * @param realName 用户姓名
 * @param selfNum 身份证号
 * @param frontOfIdCardBase64
 * @param reverseOfIdCardBase64
 * @param shouchiCardBase64
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const personCertification = (isUpdate, userId, realName, selfNum, frontOfIdCardBase64, reverseOfIdCardBase64, shouchiCardBase64, realId) => {
    return post(isUpdate ? '/tpusersreal/update' : '/tpusersreal/save', {
        "userId": userId,
        "userType": 1,
        "realName": realName,
        "selfNum": selfNum,
        "frontOfIdCardBase64": frontOfIdCardBase64,
        "reverseOfIdCardBase64": reverseOfIdCardBase64,
        "shouchiCardBase64": shouchiCardBase64,
        "realId": realId
    }).then((user) => {
        return Promise.resolve(user)
    })
}

/**
 * 获取实名认证详情
 * @param userId
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const certificationDetail = (userId) => {
    return get('/tpusersreal/usersRealInfo/' + userId)
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取热门商品
 * @returns {*}
 */
export const getHotGoods = () => {
    return get('/tpgoods/listForPhone?isHot=1&pageSize=10')
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取首页最新报单列表
 * @returns {*}
 */
export const getNewBusiness = () => {
    return get('/tpbaodan/listForPhone?isOpen=true&baodanStatus=3&pageSize=' + AppSetting.PAGE_SIZE)
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取首页最新报单列表
 * @returns {*}
 */
export const latestOrders = (isRefresh, rowNum) => {
    return get('/tpbaodan/listForPhone?isOpen=true&baodanStatus=3&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


// catid=13 公告
// catid=14 活动
// catid=15 资讯

/**
 * 获取最新公告
 * @returns {*}
 */
export const getNotice = () => {
    return get('/tparticle/listForPhone?catId=13&isHot=1&pageSize=5')
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取公告列表
 * @returns {*}
 */
export const getNoticeList = (isRefresh, id) => {
    return get('/tparticle/listForPhone?catId=13&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + id))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取新闻资讯
 * @returns {*}
 */
export const getNewsList = (isRefresh, id) => {
    return get('/tparticle/listForPhone?catId=15&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + id))
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取活动信息
 * @returns {*}
 */
export const getActivityList = (isRefresh, id) => {
    return get('/tparticle/listForPhone?catId=14&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + id))
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取最新活动
 * @returns {*}
 */
export const getActivity = () => {
    return get('/tparticle/listForPhone?catId=14&pageSize=5')
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取最新资讯
 * @returns {*}
 */
export const getIndexNews = () => {
    return get('/tparticle/listForPhone?params=catId=15&isTuijian=1')
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取关于
 * @returns {*}
 */
export const about = () => {
    return get('/tparticle/info/1')
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 银行卡绑定
 * @param realId
 * @param bankName
 * @param bankCard
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const bindingCard = (realId, bankName, bankCard) => {
    return post('/tpusersreal/updateBankInfo', {
        realId: realId,
        bankName: bankName,
        bankCard: bankCard,
    })
        .then((data) => {
            Store.dispatch({
                type: Types.UPDATE_REAL,
                real: data.usersReal,
            })
            return Promise.resolve(data)
        })
}


/**
 * 添加报单
 * @param userId 报单人ID
 * @param baodanType 类型ID
 * @param totalAmount 报单总价
 * @param address 地址
 * @param 签名图片 合同base64 多个合同以, 分隔
 * @param userNote 用户备注
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const addBusiness = (userId, baodanType, totalAmount, address, autograph, userNote) => {
    return post('/tpbaodan/save', {
        userId: userId,
        baodanType: baodanType,
        totalAmount: totalAmount,
        address: address,
        autograph: autograph,
        userNote: userNote,
    })
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取报单类型
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const orderType = () => {
    return get('/tpbaodantype/listForPhone')
        .then((data) => {
            return Promise.resolve(data)
        })

}

/**
 * 上传用户头像
 * @param fileStream
 * @returns {*}
 */
export const uploadAvatar = (userId, head) => {
    return post('/sys/user/updatePhoto',
        {
            userId: userId,
            headPicBase64: head
        })
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 重置密码
 * @param mobile
 * @param captcha
 * @param password
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const resetPassword = (mobile, captcha, password) => {
    return post('/retrievePassword', {
        "mobile": mobile,
        "captcha": captcha,
        "password": password
    }).then((user) => {
        return Promise.resolve(user)
    })
}


/**
 * 获取商品订单列表
 * @param pageNum
 * @returns {*}
 */

export const getTpOrderList = (type, userId, lastRowNum) => {
    let param = {};
    switch (type) {
        case 0:
            param = {
                "userId": userId,
                "lastRowNum": lastRowNum,
                "pageSize": AppSetting.PAGE_SIZE,
            };
            break;
        case 1:
            param = {
                "userId": userId,
                "lastRowNum": lastRowNum,
                "payStatus": 0,
                "pageSize": AppSetting.PAGE_SIZE,
            };
            break;
        case 2:
            param = {
                "userId": userId,
                "lastRowNum": lastRowNum,
                "payStatus": 1,
                "shippingStatus": 0,
                "orderStatus": 1,
                "pageSize": AppSetting.PAGE_SIZE,
            };
            break;
        case 4:
            param = {
                "userId": userId,
                "lastRowNum": lastRowNum,
                "orderStatus": 4,
                "pageSize": AppSetting.PAGE_SIZE,
            };
            break;
    }
    return get('/tporder/listForPhone', param)
}


/**
 * 获取消费订单列表
 * @returns {*}
 */
export const getConsumerOdersList = (isRefresh, userId, type, rowNum) => {
    return get('/tpbaodan/listForPhone?userId=' + userId + (!type ? '' : '&baodanStatus=' + type) + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取我的客户列表
 * @returns {*}
 * @returns {*}
 */
export const getReferee = (isRefresh, userId, rowNum) => {
    return get('/sys/user/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取我的客户列表
 * @returns {*}
 * @returns {*}
 */
export const getMyCustomersList = (isRefresh, userId, rowNum) => {
    return get('/sys/user/listForPhone?userId=' + userId + '&firstLeader=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取办事处列表
 * @param hProvince //省
 * @param hCity  //市
 * @param hDistrict  //区
 * @param title   //名称
 * @returns {*}
 */
export const getOfficesList = (code, codeType, title) => {

    let param = {};
    if (codeType == 'Provincecode') {
        param = {
            hProvince: code,
            title: title === undefined ? '' : title,
        };
    }
    else if (codeType == 'citycode') {
        param = {
            hCity: code,
            title: title === undefined ? '' : title,
        };
    }
    else {
        param = {
            hDistrict: code,
            title: title === undefined ? '' : title,
        };
    }
    return get('/tppartner/listForPhone', param)
        .then((data) => {
            return Promise.resolve(data)
        })
};

/**
 * 账户资金
 * @returns {*}
 */
export const getMyAccount = (userId) => {
    return get('/tpbaodanrebatelog/queryMoneyList/' + userId)
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取收货地址
 * @returns {*}
 */
export const getAddress = (isRefresh, userId, rowNum) => {
    return get('/tpuseraddress/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 添加收货地址
 * @param userId 用户ID
 * @param consignee 收货人
 * @param mobile 手机号
 * @param province 省
 * @param city 市
 * @param district 地区
 * @param address 地址
 * @param isDefault 是否默认
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const addAddress = (userId, consignee, mobile, province, city, district, address, isDefault) => {
    return post('/tpuseraddress/save', {
        userId: userId,
        consignee: consignee,
        mobile: mobile,
        province: province,
        city: city,
        district: district,
        address: address,
        isDefault: isDefault ? 1 : 0,
    })
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 删除收货地址
 * @returns {*}
 */
export const deleteAddress = (addressId) => {
    return post('/tpuseraddress/delete', {
        addressIds: [addressId]
    })
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 更新收货地址
 * @param userId 用户ID
 * @param consignee 收货人
 * @param mobile 手机号
 * @param province 省
 * @param city 市
 * @param district 地区
 * @param address 地址
 * @param isDefault 是否默认
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const updateAddress = (userId, addressId, consignee, mobile, province, city, district, address, isDefault) => {
    return post('/tpuseraddress/update', {
        userId: userId,
        addressId: addressId,
        consignee: consignee,
        mobile: mobile,
        province: province,
        city: city,
        district: district,
        address: address,
        isDefault: isDefault ? 1 : 0,
    })
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 重置密码
 * @param mobile
 * @param captcha
 * @param password
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const payPassword = (mobile, captcha, password) => {
    return post('/paymentPassword', {
        "mobile": mobile,
        "captcha": captcha,
        "paypwd": password
    }).then((user) => {
        return Promise.resolve(user)
    })
}


/**
 * 提现记录
 * @returns {*}
 */
export const cashRecord = (isRefresh, userId, rowNum) => {
    return get('/tpwithdrawals/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 余额明细
 * @returns {*}
 */
export const balanceDetail = (isRefresh, userId, rowNum, type) => {
    let str = ''
    if (type == 1) {
        str = '&zhuanqu=1'
    } else if (type == 2) {
        str = '&xiaofei=1'
    }
    return get('/tpaccountlog/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum) + str)
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 充值明细
 * @param isRefresh
 * @param userId
 * @param rowNum
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const rechargeDetail = (isRefresh, userId, rowNum) => {
    return get('/tprecharge/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 我的客户-佣金-佣金明细
 * @returns {*}
 */
export const commissionsDetail = (isRefresh, userId, rowNum) => {
    return get('/tpbaodanrebatelog/listForPhone?userId=' + userId + '&pageSize=' + AppSetting.PAGE_SIZE + (isRefresh ? '' : '&lastRowNum=' + rowNum))
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 获取客服电话
 * @returns {*}
 */
export const getCustomerTel = () => {
    return get('/tpconfig/queryObjectByName/phone')
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 提现申请
 * @param userId 用户ID
 * @param bankName 银行名称
 * @param bankCard 银行卡号
 * @param realname 姓名
 * @param money 提现金额
 * @param paypwd 支付密码
 * @returns {*|Promise<T>|Promise<*>|PromiseLike<any>|Promise<any>}
 */
export const cashApply = (userId, bankName, bankCard, realname, money, paypwd) => {
    return post('/tpwithdrawals/save', {
        bankCard: bankCard,
        bankName: bankName,
        money: money,
        realname: realname,
        userId: userId,
        paypwd: paypwd,
    })
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取服务协议
 * @returns {*}
 */
export const getServiceAgreement = () => {
    return get('/tparticle/info/2')
        .then((data) => {
            return Promise.resolve(data)
        })
}


/**
 * 获取资讯详情
 * @returns {*}
 */
export const getNewsDetail = (id) => {
    return get('/tparticle/info/' + id)
        .then((data) => {
            return Promise.resolve(data)
        })
}

/**
 * 更新用户信息
 * @param user
 * @returns {Promise<T> | * | Promise<*> | PromiseLike<any> | Promise<any>}
 */
export const updateUser = (userId, sex) => {
    return post('/sys/user/update', {
        userId: userId,
        sex: sex
    })
        .then((data) => {
            Store.dispatch({
                type: Types.UPDATE_HEAD,
                user: data.userInfo
            })
            return Promise.resolve(data)
        })
}


/**
 * 支付宝支付
 * @param userId
 * @param account
 * @param isVip
 * @returns {Promise<T> | * | Promise<*> | PromiseLike<any> | Promise<any>}
 */
export const aliPay = (userId, account, isVip) => {
    return post('/pay/aliPay', {
        userId: userId,
        account: account,
        buyVip: isVip ? 1 : 0,
    })
        .then((data) => {

            return Promise.resolve(data)
        })
}



/**
 * 上传发票
 * @param fileStream
 * @returns {*}
 */
export const uploadInvoice = (baodanId, images) => {
    return post('/tpbaodan/uploadFapiao',
        {
            baodanId: baodanId,
            invoiceImgBase64: images
        })
        .then((data) => {
            return Promise.resolve(data)
        })
}

