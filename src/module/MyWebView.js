import React, {Component} from 'react'
import {
    requireNativeComponent
} from 'react-native'
import PropTypes from 'prop-types'

export default class MyWebView extends Component {

    render() {
        return (
            <MyWebView
                {...this.props}
                url={this.props.url}
            />
        );
    }
}


MyWebView.propTypes = {
    url:PropTypes.string,
}

module.exports = requireNativeComponent('MyWebView',MyWebView);
