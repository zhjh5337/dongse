import React, {Component} from 'react'


import PropTypes from 'prop-types'
import InputView  from '../components/InputView'
import Toast from "react-native-root-toast"

/**
 *
 */
export default class VeriCodeInput extends Component {

    static propTypes = {
        areaCode:PropTypes.string,
        mobile:PropTypes.string,
        value: PropTypes.string,
        align: PropTypes.string,
        type:PropTypes.number,
        onChangeText: PropTypes.func,
    };

    static defaultProps = {
        type: 1,
        mobile: '',
        veriCode: '',
    };

    constructor(props) {
        super(props)
        this.state = {
            type: props.type,
            align: props.align,
            mobile: props.mobile,
            value: props.value,
            areaCode: props.areaCode,
            onChangeText: props.onChangeText,
            timerCount: 60,
            timerTitle: '获取验证码',
            isDisable: false,//
        }
    }


    //倒计时
    daoJClick() {
        if (this.props.mobile === '') {
            Toast.show('请输入手机号码');
            return;
        }
        if (!(/^1[34578]\d{9}$/.test(this.props.mobile))) {
            Toast.show('请输入正确的手机号码');
            return
        }
        if(this.state.isDisable){
            return
        }
        Api.sendVerifyCode(this.props.type, this.props.mobile)
            .then((data) => {
            }).catch((e) => {
        });
        this.setState({
            isDisable: false,
        });
        const codeTime = this.state.timerCount;
        const now = Date.now();
        const overTimeStamp = now + codeTime * 1000 + 100;
        /*过期时间戳（毫秒） +100 毫秒容错*/
        this.interval = setInterval(() => {
            /* 切换到后台不受影响*/
            const nowStamp = Date.now();
            if (nowStamp >= overTimeStamp) {
                /* 倒计时结束*/
                this.interval && clearInterval(this.interval);
                this.setState({
                    timerCount: codeTime,
                    timerTitle: '获取验证码',
                    isDisable: false,
                });
            } else {
                const leftTime = parseInt((overTimeStamp - nowStamp) / 1000, 10);
                this.setState({
                    timerCount: leftTime,
                    timerTitle: '正在获取' + `(${leftTime})s`,
                    isDisable: true,
                });
            }
            /* 切换到后台 timer 停止计时 */
        }, 1000)
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.timer);
        this.setState = (state,callback)=>{
            return;
        };
    }

    render() {
        return (
            <InputView
                {...this.props}
                returnKeyLabel={this.props.returnKeyLabel}
                returnKeyType={this.props.returnKeyType}
                align={this.props.align}
                value={this.props.pin}
                name={'验证码'}
                hintText={'请输入验证码'}
                funcDisabled={this.state.isDisable}
                onChangeText={this.props.onChangeText}
                funcName={this.state.timerTitle}
                funcOnPress={() => this.daoJClick()}/>
        );
    }
}
