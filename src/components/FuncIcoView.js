import React, {Component} from 'react'
import {
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'


export default class FuncIcoView extends Component {

    static propTypes = {
        text: PropTypes.string,
        color: PropTypes.string,
        fontSize: PropTypes.number,
        icoSize: PropTypes.number,
        onPress: PropTypes.func,
        textMargin: PropTypes.number,
        ico: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
    };

    static defaultProps = {
        color: '#999999',
        fontSize: 13,
        icoSize: 30,
        textMargin: 8,
    };

    constructor(props) {
        super(props)
        this.state = {
            text: props.text,
            color: props.color,
            fontSize: props.fontSize,
            onPress: props.onPress,
            textMargin: props.textMargin,
            ico: props.ico,
            icoSize: props.icoSize,
            style: props.style,
        }
    }

    render() {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                underlayColor='transparent'
                style={[{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center'
                }, this.props.style]}
                onPress={this.props.onPress}>
                <Image
                    source={this.props.ico}
                    resizeMode={'stretch'}
                    style={{height: this.props.icoSize, width: this.props.icoSize}}/>
                <Text style={{
                    color: this.props.color,
                    fontSize: this.props.fontSize,
                    marginTop: this.props.textMargin
                }}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}
