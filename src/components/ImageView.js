import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    TouchableOpacity,
    View,
    Image,
} from 'react-native'
import {AppSetting} from './AppStyles'
import selectImage from 'react-native-image-crop-picker'
import {ActionSheet} from 'teaset'


export default class ImageView extends Component {

    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
        onSelect: PropTypes.func,
        onFuncPress: PropTypes.func,
        funcDisabled: PropTypes.bool,
        funcIco: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        source: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
    };

    static defaultProps = {
        width: 242,
        height: 160,
        funcDisabled: false,
        funcIco: require('../../assets/images/arrow_right.png'),
    };

    constructor(props) {
        super(props)
        this.state = {
            width: props.width,
            height: props.height,
            source: props.source,
            onSelect: props.onSelect,
            style: props.style,
            funcIco: props.funcIco,

        }
    }

    selectImages = (isCamera: boolean) => {
        return isCamera ?
            selectImage.openCamera({
                cropping: true,
                width: 500,
                height: 500,
                includeBase64: true,
                compressImageQuality: 0.7,
            }) :
            selectImage.openPicker({
                cropping: true,
                width: 500,
                height: 500,
                multiple: false,
                waitAnimationEnd: false,
                includeBase64: true,
                maxFiles: 1,
                compressImageQuality: 0.7,
            })
    }


    imageSelect = () => {
        let items = [
            {title: '从相册选择', onPress: () => this.imageProcessing(false)},
            {title: '拍照', onPress: () => this.imageProcessing(true)},
        ];
        ActionSheet.show(items, {title: '取消'});
    }

    imageProcessing = (isCamera: boolean) => {
        this.selectImages(isCamera)
            .then(image => {
                this.props.onSelect(image)
                // ProgressDialog.show()
                // Api.uploadAvatar(image.path)
                //     .then(response => {
                //         Api.updateAvatar(response.key).then(response => {
                //             ProgressDialog.hide()
                //         }).catch(e => {
                //             ProgressDialog.hide()
                //             Toast.show(I18n.t('supplement.modify_failed_title'))
                //             console.log(I18n.t('supplement.upload_failure') + JSON.stringify(e))
                //         })
                //     })
                //     .catch(e => {
                //         Toast.show(I18n.t('supplement.modify_failed_title'))
                //         ProgressDialog.hide()
                //     })

            }).catch(e => {
        });
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={0.7}
                              underlayColor='transparent'
                              style={[this.props.style, {
                                  width: this.props.width,
                                  height: this.props.height
                              }]}
                              onPress={() => this.imageSelect()}>
                <Image
                    style={{flex: 1}}
                    source={this.props.source}
                />
                {
                    this.props.funcDisabled ?
                        <TouchableOpacity activeOpacity={0.7}
                                          underlayColor='transparent'
                                          style={{
                                              position: 'absolute',
                                              height: 15,
                                              width: 15,
                                              top: 5,
                                              right: 5,
                                          }}
                                          onPress={this.props.onFuncPress}>
                            <Image
                                source={this.state.funcIco}
                            />
                        </TouchableOpacity> : null
                }


            </TouchableOpacity>
        );
    }
}
