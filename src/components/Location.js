import React, {Component} from "react"
import {

    DeviceEventEmitter
} from 'react-native';
import Geolocation from 'Geolocation'
import AREA_DATA from '../../assets/value/city-list.json';

export default class Location {

    static getLongitudeAndLatitude = () => {

        //获取位置再得到城市先后顺序，通过Promise完成
        return new Promise((resolve, reject) => {

            Geolocation.getCurrentPosition(
                location => {

                    //可以获取到的数据
                    var result = "速度：" + location.coords.speed +
                        "\n经度：" + location.coords.longitude +
                        "\n纬度：" + location.coords.latitude +
                        "\n准确度：" + location.coords.accuracy +
                        "\n行进方向：" + location.coords.heading +
                        "\n海拔：" + location.coords.altitude +
                        "\n海拔准确度：" + location.coords.altitudeAccuracy +
                        "\n时间戳：" + location.timestamp;

                    fetch('http://restapi.amap.com/v3/geocode/regeo?location=' + location.coords.longitude + ',' + location.coords.latitude + '&key=cea3e4009397fdffb5ed6e9948969142')
                        .then(response => {
                            var data = JSON.parse(response._bodyInit);
                            console.log(AREA_DATA);
                            var temCity = {};
                            for (let i = 0; i < AREA_DATA.allCityList.length - 1; i++) {
                                if (AREA_DATA.allCityList[i].area_name === data.regeocode.addressComponent.city) {
                                    temCity = AREA_DATA.allCityList[i];
                                    break;
                                }

                            }
                            Store.dispatch({type: Types.SET_AREA, area: temCity});
                            DeviceEventEmitter.emit('selectArea', temCity);
                        })
                        .catch(e => {
                            // console.log('===e===' + e.toString())
                        })
                    // ToastAndroid.show("UTIl" + location.coords.longitude, ToastAndroid.SHORT);
                    console.log('-------------- result  ' + result)

                    // resolve([location.coords.longitude, location.coords.latitude]);
                },
                error => {
                    // Alert.alert("获取位置失败：" + error, "")
                    reject(error);
                }
            );
        })
    }
//http://restapi.amap.com/v3/place/text?keywords=北京大学&city=beijing&output=xml&offset=20&page=1&key=<用户的key>&extensions=all
    static searchPoint(title, callback) {
        fetch('http://restapi.amap.com/v3/place/text?keywords=' + title + '&key=' + 'cea3e4009397fdffb5ed6e9948969142')
            .then(response => {

                callback(JSON.parse(response._bodyInit));
            })
            .catch(e => {
                // console.log('===e===' + e.toString())
            })
    }

}
