import React, {Component} from 'react'
import {
    Animated,
    Text,
    SectionList,
    View
} from 'react-native'

const AnimatedSectionList = Animated.createAnimatedComponent(SectionList)

const VIEWABILITY_CONFIG = {
    minimumViewTime: 3000,
    viewAreaCoveragePercentThreshold: 100,
    waitForInteraction: true
}

class FooterComponent extends React.PureComponent {
    render = () => {
        return (
            <View style={{
                height: 45,
                backgroundColor: '#F8F8F8',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{color: '#888888', fontSize: 12}}>{this.props.text}</Text>
            </View>
        )
    }
}

export default class ExpandableListView extends React.PureComponent {
    constructor(props) {
        super(props)
    }

    scrollPos = new Animated.Value(0)
    scrollSinkX = Animated.event(
        [{nativeEvent: {contentOffset: {x: this.scrollPos}}}],
        {useNativeDriver: true}
    )

    scrollSinkY = Animated.event(
        [{nativeEvent: {contentOffset: {y: this.scrollPos}}}],
        {useNativeDriver: true}
    )

    renderFooterComponent = () => {
        if (this.props.refreshing || this.props.hasMoreData === undefined) {
            return null
        }
        const text = this.props.hasMoreData ? '正在加载更多数据...' : '没有更多数据了'
        return (
            <FooterComponent text={text}/>
        )
    }

    render() {
        return (
            <AnimatedSectionList
                viewabilityConfig={VIEWABILITY_CONFIG}
                ListFooterComponent={this.renderFooterComponent}
                debug={false}
                enableVirtualization={false}
                legacyImplementation={false}
                numColumns={1}
                onScroll={this.scrollSinkY}
                onEndReachedThreshold={0.5}
                stickySectionHeadersEnabled={false}
                {...this.props} />

        )
    }
}
