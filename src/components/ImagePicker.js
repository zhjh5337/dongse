/**
 * Created by lilong on 2017/5/16.
 */
import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
} from 'react-native';
import {AppSetting} from './AppStyles'
import selectImage from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet'
import PropTypes from 'prop-types'

const ScreenWidth = Dimensions.get('window').width;

var ImageWidth = (ScreenWidth - 50) / 3;

export default class ImagePicker extends React.Component {

    static propTypes = {

    };
    static defaultProps = {

    };

    constructor(props) {
        super(props);
        this.state = {
            selected: '',
            images:[],
            imageArry:[],
        };
        this.handlePress = this.handlePress.bind(this);
        this.showActionSheet = this.showActionSheet.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: '',
            images:[],
        })
    }

    showActionSheet() {
        this.ActionSheet.show()
    }

    handlePress(i) {
        if (i === 1) {
            selectImage.openCamera({
                cropping: true,
                width: 500,
                height: 500,
                includeBase64: true,
                compressImageQuality: 0.7,
            }).then(image => {
                this.state.imageArry.push({
                    image: image,
                    url: `data:${image.mime};base64,` + image.data,
                });
                //取扩展名和base64
                var tem = image.mime.split('/');
                this.state.images.push(tem[tem.length - 1] + ';' + image.data);

                this.setState({
                    imageArry: this.state.imageArry,

                });
                this.props.selectImage(this.state.imageArry);
            }).catch(e => alert(e));
        }
        else if (i === 2) {
            selectImage.openPicker({
                cropping: true,
                width: 500,
                height: 500,
                multiple: false,
                waitAnimationEnd: false,
                includeBase64: true,
                maxFiles: 9,
                compressImageQuality: 0.7,
            }).then(images => {


                this.state.imageArry.push({
                    uri: `data:${images.mime};base64,` + images.data,
                    mime: images.mime
                });
                //取扩展名和base64
                var tem = images.mime.split('/');
                this.state.images.push(tem[tem.length - 1] + ';' + images.data);

                this.setState({
                    imageArry: this.state.imageArry,
                });
                this.props.selectImage(this.state.imageArry);
            }).catch();
        }
    };


    getImageArry() {
        return this.state.images;
    };

    getopenArry() {
        return this.state.imageArry;
    };

    render() {

        return (
            <View style={[styles.containerStyle, this.props.containerStyle]}>

                <TouchableOpacity style={{
                    alignItems:'center',margin: 5,
                }} underlayColor='transparent'
                                    onPress={() => {
                                        this.showActionSheet()
                                    }}>
                    <Image resizeMode='contain' style={styles.image_style} source={require('../../assets/images/addImage.png')}/>
                </TouchableOpacity>
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'选择图片'}
                    options={['取消', '拍照', '相册',]}
                    cancelButtonIndex={0}
                    onPress={this.handlePress}
                />
            </View>
        );
    }
}

let styles = StyleSheet.create({
    containerStyle: {
        padding: 10,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },

    image_style: {
        width: ImageWidth,
        height: ImageWidth,
    },
});
