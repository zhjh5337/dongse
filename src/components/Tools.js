import React from "react";
import PropTypes from 'prop-types'

const idCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

const bankData = '../../assets/value/bank.json'
import AREA_DATA from '../../assets/value/area.json'
import {AppSetting} from "./AppStyles"
import moment from "moment/moment"
import {InteractionManager, NativeModules, Platform,Dimensions} from "react-native"
import RNFS from "react-native-fs";

const isIphoneX = ()=>{
    let dimen = Dimensions.get('window');
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        (dimen.height === 812 || dimen.width === 812)
    );
};

const HTML_DECODE = {
    "&lt;": "<",
    "&gt;": ">",
    "&amp;": "&",
    "&nbsp;": " ",
    "&quot;": "\"",
    "?": ""
};

/**
 * 是否手机号
 * @param mobile 手机号码
 * @returns {boolean}
 */
const isPhone = (mobile) => {
    return /^1[34578]\d{9}$/.test(mobile)
}


/**
 * 判断账号是否为6-20位以字母开头数字字母大小写混搭的字符串
 * @param account
 * @returns {boolean}
 */
const isAccount = (account) => {
    return /^[a-zA-Z][a-zA-Z0-9_-]{5,19}$/.test(account)
}


/**
 * 判断是否为身份证号
 * @param idcard
 * @returns {boolean}
 */
const isIdCard = (idcard) => {
    return idCard.test(idcard)
}


/**
 * 判断是否为企业信用代码
 * @param code
 * @returns {boolean}
 */
const isEnterpriseCreditCode = (code) => {
    return /^[A-Z0-9]{18}$/.test(code)
}


/**
 * 获取银行卡名称
 * @param idCard
 * @returns {*}
 */
const getBankName = (idCard) => {
    var index = -1;
    if (idCard.length() < 16 || idCard.length() > 19) {
        return "";
    }

    //6位Bin号

    var cardbin_6 = idCard.substring(0, 6);
    //
    bankData.bankBin.forEach((item, i) => {
        if (cardbin_6.equals(item)) {
            index = i;
        }
    })
    if (index != -1) {
        return bankData.bankName[index];
    }

    //8位Bin号
    var cardbin_8 = idCard.substring(0, 8);
    bankData.bankBin.forEach((item, i) => {
        if (bankBin.equals(item)) {
            index = i;
        }
    })
    if (index != -1) {
        return bankData.bankName[index];
    }
    return
}

/**
 * 时间戳转换
 * */
function timestampToTime(timestamp) {
    var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = date.getDate() + ' ';
    h = date.getHours() + ':';
    m = date.getMinutes() + ':';
    s = date.getSeconds();
    return Y + M + D + h + m + s;
}

/**
 * 区域代码转区域地址
 * @param areaCodes
 */
const code2AreaName = (areaCodes: PropTypes.array) => {
    if (areaCodes.length !== 3) {
        return ''
    }
    let code = areaCodes;

    if (areaCodes[0] === areaCodes[1]) {
        code.splice(0, 1)
    }
    let address = ''
    AREA_DATA.forEach((province, i) => {
        if (code[0] === province.areaID) {
            address = province.areaName
            if (province.childArea.length !== 0) {
                province.childArea.forEach((city, cityI) => {
                    if (code[1] === city.areaID) {
                        address += city.areaName
                        if (city.childArea.length !== 0 && code.length === 3) {
                            city.childArea.forEach((district, districtI) => {
                                if (code[2] === district.areaID) {
                                    address += district.areaName
                                }
                            })
                        }
                    }
                })
            }
        }

    })
    return address

}


/**
 * 城市区域代码转区域地址
 * @param areaCodes
 */
const city2AreaName = (cityCode: PropTypes.number) => {

    if (!cityCode || cityCode === '') {
        return ''
    }

    let address = ''
    AREA_DATA.forEach((province, i) => {
        if (cityCode === province.areaID) {
            return address = province.areaName
        } else {
            if (province.childArea.length !== 0) {
                province.childArea.forEach((city, cityI) => {
                    if (cityCode === city.areaID) {
                        return address = province.areaName + city.areaName
                    } else {
                        if (city.childArea.length !== 0) {
                            city.childArea.forEach((district, districtI) => {
                                if (cityCode === district.areaID) {
                                    return address = province.areaName + city.areaName + district.areaName
                                }
                            })
                        }
                    }
                })
            }
        }

    })
    return address
}


/**
 * 根据会员级别ID获取对应名称
 * @param levelId
 * @returns {string}
 */
const getLevelName = (levelId) => {
    let name = ''
    AppSetting.LEVEL.forEach((item, i) => {
        if (item.key === levelId) {
            name = item.name
        }
    })
    return name;
}

/**
 *
 * @param date
 * @returns {string}
 */
const unix2DateStr = (date) => {
    return moment.unix(date).format('YYYY-MM-DD HH:mm:ss')
}


/**
 * 根据状态ID获取对应名称
 * @param statusID
 * @returns {string}
 */
const getCashRecordName = (statusID) => {
    let name = ''
    AppSetting.RECORD_TYPES.forEach((item, i) => {
        if (item.key === statusID) {
            name = item.name
        }
    })
    return name;
}


/**
 * 根据报单类型ID获取对应图片
 * @param
 * @returns {*}
 */
const getOrderTypeImage = (code) => {

    if (code === -3) {//已作废
        return '';
    } else if (code === -2) { //初审拒绝
        return require('../../assets/images/ic_audit_status_m2.png');
    } else if (code === -1) { //复审拒绝
        return require('../../assets/images/ic_audit_status_m1.png');
    } else if (code === 0) {//待初审
        return require('../../assets/images/ic_audit_status_0.png');
    } else if (code === 1) {//待复审
        return require('../../assets/images/ic_audit_status_1.png');
    } else if (code === 2) {//排号中
        return require('../../assets/images/ic_audit_status_2.png');
    } else if (code === 3) {//已出单
        return require('../../assets/images/ic_audit_status_3.png');
    } else {//4已完成
        return require('../../assets/images/ic_audit_status_4.png');
    }
}

/**
 * 获取消费类型图片
 * @param code
 * @returns {*}
 */

const getOrderTypeImage2 = (code) => {
    switch (code) {
        case 0://待确认
            return require('../../assets/images/ic_audit_status_0.png');
        case 1://已确认
            return require('../../assets/images/ic_audit_status_3.png');
        case 2://已出单
            return require('../../assets/images/ic_audit_status_2.png');
        case 3://已完成
            return require('../../assets/images/ic_audit_status_4.png');
        case 4://已作废
            return require('../../assets/images/ic_audit_status_1.png');
    }
}
const getConsumeTypeImage = (code) => {
    switch (code) {
        case 1://购车
            return require('../../assets/images/ic_car.png');
        case 2://购房
            return require('../../assets/images/ic_house.png');
        case 3://购房
            return require('../../assets/images/ic_shop.png');

        default:
            return require('../../assets/images/ic_other.png');
    }
}


/**
 * 合同预览
 * @param userId 用户ID
 * @param amount 合同额
 * @param payType 支付类型
 * @param address 地址
 */
const compactPreview = (userId, amount, payType, address) => {
    let str = 'http://dsfctx.linzhen.cc/Mobile/Baodan/HT/uid/' + userId + '/total_amount/' + amount + '/pay_code/' + payType + '/address/' + address
    if (Platform.OS === 'ios') {
        InteractionManager.runAfterInteractions(() => {
            NativeModules.PushNative.RNOpenOneVC(str);
        });
    }
    else {
        NativeModules.IntentModule.startActivityFromJS("com.dongse.car.ui.WebActivity", "{name:'合同预览', url:'" + str + "'}")
    }
}


const compactPreview2 = (url) => {
    if (Platform.OS === 'ios') {
        InteractionManager.runAfterInteractions(() => {
            NativeModules.PushNative.RNOpenOneVC(url);
        });
    }
    else {
        NativeModules.IntentModule.startActivityFromJS("com.dongse.car.ui.WebActivity", "{name:'合同预览', url:'" + url + "'}")
    }
}

const signView = (callback) => {
    if (Platform.OS === 'ios') {
        InteractionManager.runAfterInteractions(() => {
            NativeModules.PushNative2.RNOpenOneVC(callback);
        });
    }
    else {
        NativeModules.IntentModule.startActivityFroResult("com.dongse.car.ui.AutographActivity", "{name:'签名', saveName:'保存', clearName:'清除'}", callback)
    }
};


const decodeHtml = (s) => {
    let REGX_HTML_DECODE = /&\w+;|&#(\d+);/g;
    s = (s != undefined) ? s : this.toString();
    return (typeof s == "string") ?
        replacecode(s, HTML_DECODE, REGX_HTML_DECODE) : (Array.isArray(s)) ? decodeArray(s, HTML_DECODE, REGX_HTML_DECODE) : s;
}

function replacecode(s, decode, regxde) {
    return s.replace(regxde, function ($0, $1) {
        var c = decode[$0];
        if (c == undefined) {
            // Maybe is Entity Number
            if (!isNaN($1)) {
                c = String.fromCharCode(($1 == 160) ? 32 : $1);
            } else {
                c = $0;
            }
        }
        return c;
    })
};

function decodeArray(arr, decode, regxde) {
    return arr.map(function (item) {
        return replacecode(item, decode, regxde)
    })
}


const getAuthName = (num) => {
    let names = [
        {key:0, name:'未认证'},
        {key:1, name:'待审核'},
        {key:2, name:'已认证'},
        {key:3, name:'认证失败'},
    ]
    let name =''
    names.forEach((item, i) => {
        if(item.key === num){
            return name = item.name
        }
    });
    return name;
}

export {
    isPhone,
    isAccount,
    isIdCard,
    isEnterpriseCreditCode,
    getBankName,
    timestampToTime,
    code2AreaName,
    getLevelName,
    unix2DateStr,
    getCashRecordName,
    getOrderTypeImage,
    compactPreview,
    compactPreview2,
    signView,
    getConsumeTypeImage,
    city2AreaName,
    decodeHtml,
    getAuthName
}
