import React from "react";
import {Dimensions, Text, Platform, StatusBar} from 'react-native';
import {Overlay, Toast} from 'teaset'
let androidMarginTop =  Platform.Version > 19 ? StatusBar.currentHeight : 0;
export default class ToastUtil extends Toast {

    static topPosition = Platform.OS === 'ios' ? 64 : (56 + androidMarginTop);

    static baseNotify(message, color) {
        let overlayView = (
            <Overlay.View
                style={{
                    backgroundColor: color,
                    height: 45,
                    width: Dimensions.get('window').width,
                    justifyContent: 'center',
                    alignItems: 'center',
                    top: this.topPosition,
                    position: 'absolute'
                }}
                overlayOpacity={0}
            >
                <Text style={{color: 'white', fontSize: 15}}>{message}</Text>
            </Overlay.View>
        );
        let key = Overlay.show(overlayView);
        setTimeout(() => Overlay.hide(key), 3000);
        return key;
    }


    static errorNotify(message) {
        return this.baseNotify(message, '#FE6264');
    }

    static warnNotify(message) {
        return this.baseNotify(message, '#FD8006');
    }

    static successNotify(message) {
        return this.baseNotify(message, '#00CC9A');
    }

}
