import React, {Component} from 'react'
import {
    StyleSheet,
    Platform,
    TouchableWithoutFeedback,
    View,
    Image,
    Dimensions,
    Text
} from 'react-native'
import moment from "moment/moment";
const PixelRatio = require('PixelRatio');
// copy from react-navigation/src/views/Header/Header.js
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const TITLE_OFFSET = Platform.OS === 'ios' ? 70 : 56;
const ScreenHeight = Dimensions.get('window').height;
const ScreenWidth = Dimensions.get('window').width;
const AppSetting = {
    IMAGE_fIRST_URL: 'http://117.34.109.161:8080/dsfctx-1.0.0',

    BLUE: '#00AAFF',
    BLUELIGHT:'#3ABCFD',
    REDLIGHT:'#FE6264',
    BLACK: '#333333',
    RED: '#f61012',
    GREEN: '#67B246',
    YELLOW:'#FBB744',
    ORANGE:'#FD8006',
    PLACEHOLD: '#CCCCCC',
    TITLE_GRAY:'#878787',

    GRAY:'#F2F2F2',


    DEFAULT_TEXT: '#453f97',
    MAIN_COLOR:'#453f97',
    DEFAULT_BG:'#f7f7f7',
    TITLE: '#333333',
    DEFAULT: '#5E5E66',
    OTHER: '#858585',
    HINT: '#DADADA',
    SSMALL_MARGIN:5,
    SMALL_MARGIN: 10,
    NORMAL_MARGIN: 12,
    CENTER_MARGIN: 20,
    SECTION_MARGIN:45,

    ITEM_HEIGHT: 50,
    HEADER_HEIGHT: APPBAR_HEIGHT + STATUSBAR_HEIGHT,
    /**
     * 分割线颜色
     */
    SplitLine: '#E3E3E3',
    /**
     * 分割线宽度
     */
    LineWidth:  PixelRatio.roundToNearestPixel(Platform.OS === 'ios' ? 0.4 : 0.2),
    /**
     * 按钮高亮
     */
    touchHighlight: '#a3a3a3',

    PAGE_SIZE: 20,



    PAY_TYPE : [
        {name:'POS刷卡', type:'POS'},
        {name:'在线转账', type:'ZXPAY'},
        {name:'其它', type:'QT'}
    ],


    LEVEL : [
        {key:1, name:'普通会员'},
        {key:2, name:'业务员'},
        {key:3, name:'办事处'},
        {key:4, name:'合伙人'}
    ],

    RECORD_TYPES : [
        {key:-2, name:'删除作废'},
        {key:-1, name:'审核失败'},
        {key:0, name:'申请中'},
        {key:1, name:'审核成功'},
        {key:2, name:'付款成功'},
        {key:3, name:'付款失败'}
    ]

}

const AppStyles = StyleSheet.create({
    main:{
        flex: 1,
        backgroundColor: AppSetting.DEFAULT_BG
    },
    card: {
        flexDirection: 'column',
        borderRadius: 4,
        borderWidth: AppSetting.LineWidth,
        borderColor: '#CECECE',
        marginHorizontal: AppSetting.NORMAL_MARGIN,
        backgroundColor: 'white'
    },
    separator: {
        height: AppSetting.LineWidth,
        backgroundColor: '#E6E6E6',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    },
    tabBg:{
        height: 45,
        borderBottomColor: AppSetting.SplitLine,
        borderBottomWidth: AppSetting.LineWidth,
    },
    nameText: {
        fontSize: 16,
        color: AppSetting.TITLE,
        backgroundColor: 'transparent'
    },
    contentText:{
        fontSize: 15,
        color: AppSetting.OTHER,
        backgroundColor: 'transparent'
    },
    contentTextSmall:{
        fontSize: 14,
        color: AppSetting.OTHER,
        backgroundColor: 'transparent'
    },
    contentText2:{
        fontSize: 15,
        color: AppSetting.DEFAULT,
        backgroundColor: 'transparent'
    },
    bottomText:{
        fontSize: 13,
        color: AppSetting.OTHER,
        backgroundColor: 'transparent'
    },

    itemName:{
        fontSize: 16,
        color: AppSetting.DEFAULT,
        backgroundColor: 'transparent'
    },
    itemContent:{
        fontSize: 15,
        color: AppSetting.OTHER,
        backgroundColor: 'transparent'
    },
    itemContentInput:{
        fontSize: 15,
        color: AppSetting.OTHER,
        padding:0,
        backgroundColor: 'transparent'
    },
    itemImage:{
        padding:5,
    },

    normalText: {
        fontSize: 13,
        color: AppSetting.TITLE,
        backgroundColor: 'transparent'
    },
    lightText: {
        fontSize: 13,
        color: '#666666',
        backgroundColor: 'transparent'
    },
    lightTextBig: {
        fontSize: 14,
        color: '#666666',
        backgroundColor: 'transparent'
    },
    light2Text: {
        fontSize: 13,
        color: '#888888',
        backgroundColor: 'transparent'
    },
    redText: {
        fontSize: 13,
        color: '#FF6F80',
        backgroundColor: 'transparent'
    },
    redTextmore: {
        fontSize: 14,
        color: '#FF6F80',
        backgroundColor: 'transparent'
    },
    redTextbig: {
        fontSize: 15,
        color: '#FF6F80',
        backgroundColor: 'transparent'
    },
    greenText: {
        fontSize: 15,
        backgroundColor: 'transparent'
    },
    blueText: {
        fontSize: 13,
        color: '#1AA1E6',
        backgroundColor: 'transparent'
    },
    tabItem: {
        fontSize: 16,
        color: '#878787'
    },
    promptText: {
        fontSize: 14,
        color: '#878787'
    },
    tabActiveItem: {
        fontSize: 16,
        color: AppSetting.DEFAULT_TEXT
    },
    tabActiveSmallItem: {
        fontSize: 14,
        color: AppSetting.DEFAULT_TEXT
    },
    mainTabBarStyle: {
        borderTopWidth : AppSetting.LineWidth,
        borderColor    : AppSetting.SplitLine,
        backgroundColor: 'white',
    },
    separatorTop: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        right: 0,
        top: 0,
        left: 0
    },
    separatorCenter: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: AppSetting.NORMAL_MARGIN,
    },
    separatorBottom: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
    },
    loginInput:{
        color: AppSetting.DEFAULT_TEXT,
        fontSize: 17,
        padding:0,
        flex: 5,
        backgroundColor: 'transparent'
    }

})



SeparatorView = (align: string) => {
    return (
        <View
            style={align === 'top' ? AppStyles.separatorTop : align === 'center' ? AppStyles.separatorCenter : AppStyles.separatorBottom}/>
    )
}


commonIconItem = (iconSource, text, cb) => {
    return (
        <TouchableWithoutFeedback onPress={cb}>
            <View
                style={{
                    height: 50,
                    backgroundColor: 'white',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: 15,
                    paddingRight: 10
                }}>
                <Image source={iconSource}/>
                <Text style={[AppStyles.nameText, {marginLeft: 10}]}>{text}</Text>
                <View style={{flex: 1}}></View>
                <Image source={require('../../assets/images/arrow_right.png')}/>
                <View style={AppStyles.separator}/>
            </View>
        </TouchableWithoutFeedback>
    )
}

commonTextItem = (text, cb) => {
    return commonIconItem(null, text, cb)
}

parseBalance =(balance) =>{
    if(!balance || balance === null || balance === ''){
        balance = 0;
    }
    return parseFloat(balance).toFixed(2)
}

unix2Time = (unix) => {
    return moment.unix(unix).format('YYYY-MM-DD HH:mm:ss')
}

// moment

AppSetting.commonIconItem = commonIconItem
AppSetting.commonTextItem = commonTextItem
AppSetting.SeparatorView = SeparatorView
AppSetting.parseBalance = parseBalance
AppSetting.unix2Time = unix2Time
AppSetting.ScreenWidth = ScreenWidth
AppSetting.ScreenHeight = ScreenHeight

export {AppStyles, AppSetting}
