import React, {Component} from 'react';
import {View, Modal, Text, TouchableOpacity} from 'react-native';

import {Wheel, Overlay} from 'teaset';
import AREA_DATA from '../../assets/value/areas.json'
import {AppSetting, AppStyles} from "./AppStyles"
import Button from "./Button"
import PropTypes from 'prop-types'

export default class AreaPicker extends Component {

    static propTypes = {
        closePress: PropTypes.func,
        confirmPress: PropTypes.func,
    };

    static defaultProps = {
        // color: '#FFFFFF',
        // fontSize: 18,
    };

    constructor(props) {
        super(props);
        this.state = {
            provinceNames: AREA_DATA.province,
            provinceValues: AREA_DATA.provinceValue,
            cityNames: [],
            cityValues: [],
            countyNames: [],
            countyValues: [],
            provinceName: AREA_DATA.province[0],
            provinceValue: AREA_DATA.provinceValue[0],
            cityName: '',
            cityValue: 0,
            countyName: '',
            countyValue: 0,
            isVisible: this.props.show,
            closePress: props.closePress,
            confirmPress: props.confirmPress
        }

    }


    componentWillMount() {

    }


    componentWillReceiveProps(nextProps) {
        if(nextProps.show){
            this.setState({
                provinceName: AREA_DATA.province[0],
                provinceValue: AREA_DATA.provinceValue[0],
            }, () => {
                this.searchCitys();
            })
        }
        this.setState({
            isVisible: nextProps.show,
        });
    }

    closeModal = () => {
        this.setState({
            isVisible: false
        });
        this.props.closePress(false);
    }

    confirmModal = () => {
        this.closeModal()
        var areaName = this.state.provinceName + this.state.cityName + this.state.countyName;
        var areaCode = this.state.countyValue != 0 ?
            [this.state.provinceValue, this.state.cityValue, this.state.countyValue] :
            [this.state.provinceValue, this.state.provinceValue, this.state.cityValue]
        this.props.confirmPress(areaName, areaCode);
    }


    searchCitys = () => {
        for (let i = 0; i < AREA_DATA.city.length; i++) {
            var itemName = AREA_DATA.city[i];
            var itemValue = AREA_DATA.cityValue[i];
            if (AREA_DATA.cityValue[i].name == this.state.provinceValue) {
                this.setState({
                    cityNames: itemName.area,
                    cityValues: itemValue.area,
                    cityName: itemName.area[0],
                    cityValue: itemValue.area[0],
                }, () => this.searchCountyCitys())
            }
        }
    }

    searchCountyCitys = () => {
        var result = false
        for (let i = 0; i < AREA_DATA.county.length; i++) {
            var itemName = AREA_DATA.county[i];
            var itemValue = AREA_DATA.countyValue[i];

            if (AREA_DATA.countyValue[i].name == this.state.cityValue) {
                this.setState({
                    countyNames: itemName.area,
                    countyValues: itemValue.area,
                    countyName: itemName.area[0],
                    countyValue: itemValue.area[0],
                })
                result = true;
            }
        }
        if (!result) {
            this.setState({
                countyNames: [],
                countyValues: [],
                countyName: '',
                countyValue: 0,
            })
        }
    }

    render() {
        return (

            <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isVisible}
                onRequestClose={() => this.closeModal()}
            >
                <View style={{flex: 1}}>
                    <TouchableOpacity activeOpacity={0.7}
                                      underlayColor='transparent'
                                      style={{flex: 1}}
                                      onPress={() => this.closeModal()}/>
                    <View style={{height: 290}}>
                        <View style={{height: AppSetting.LineWidth, backgroundColor: AppSetting.SplitLine}}/>
                        <View style={{
                            height: 45,
                            backgroundColor: '#FFFFFF',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingLeft: AppSetting.NORMAL_MARGIN,
                            paddingRight: AppSetting.NORMAL_MARGIN,
                        }}>
                            <Button
                                text={'取消'}
                                fontSize={13}
                                color={AppSetting.TITLE_GRAY}
                                style={{backgroundColor: 'transparent'}}
                                onPress={() => this.closeModal()}
                            />
                            <Text style={AppStyles.nameText}>地址选择</Text>
                            <Button
                                text={'确定'}
                                fontSize={13}
                                color={AppSetting.MAIN_COLOR}
                                style={{backgroundColor: 'transparent'}}
                                onPress={() => this.confirmModal()}
                            />
                        </View>
                        <View style={{height: AppSetting.LineWidth, backgroundColor: AppSetting.SplitLine}}/>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            backgroundColor: '#FFFFFF',
                            padding: AppSetting.NORMAL_MARGIN,
                        }}>
                            <Wheel
                                style={{flex: 1}}
                                itemStyle={{textAlign: 'center'}}
                                items={this.state.provinceNames}
                                onChange={index => {
                                    this.setState({
                                        provinceName: this.state.provinceNames[index],
                                        provinceValue: this.state.provinceValues[index],
                                    }, () => this.searchCitys())
                                }}
                            />
                            <Wheel
                                style={{flex: 1}}
                                itemStyle={{textAlign: 'center'}}
                                items={this.state.cityNames}
                                onChange={index => {
                                    this.setState({
                                        cityName: this.state.cityNames[index],
                                        cityValue: this.state.cityValues[index],
                                    }, () => this.searchCountyCitys())
                                }}
                            />
                            <Wheel
                                style={{flex: 1}}
                                itemStyle={{textAlign: 'center'}}
                                items={this.state.countyNames}
                                onChange={index => {
                                    this.setState({
                                        countyName: this.state.countyNames[index],
                                        countyValue: this.state.countyValues[index],
                                    })
                                }}
                            />
                        </View>
                    </View>
                </View>

            </Modal>
        );
    }

}
