import React, {Component} from 'react';
import {View, Modal, Text} from 'react-native';

import { Wheel, Overlay} from 'teaset';
import AREA_DATA from '../../assets/value/areas.json'
import {AppSetting, AppStyles} from "./AppStyles"
import Button from "./Button"

export default class AreaPicker extends Component {


    constructor(props) {
        super(props);
        this.state = {
            provinceNames: AREA_DATA.province,
            provinceValues: AREA_DATA.provinceValue,
            cityNames: [],
            cityValues: [],
            countyNames: [],
            countyValues: [],
            provinceName: AREA_DATA.province[0],
            provinceValue: AREA_DATA.provinceValue[0],
            cityName: '',
            cityValue: 0,
            countyName: '',
            countyValue: 0,
        }

    }


    componentWillMount() {
        this.searchCitys();
    }


    searchCitys = () => {
        for (let i = 0; i < AREA_DATA.city.length; i++) {
            var itemName = AREA_DATA.city[i];
            var itemValue = AREA_DATA.cityValue[i];
            if (AREA_DATA.cityValue[i].name == this.state.provinceValue) {
                this.setState({
                    cityNames: itemName.area,
                    cityValues: itemValue.area,
                    cityName: itemName.area[0],
                    cityValue: itemValue.area[0],
                }, () => this.searchCountyCitys())
            }
        }
    }

    searchCountyCitys = () => {
        var result = false
        for (let i = 0; i < AREA_DATA.county.length; i++) {
            var itemName = AREA_DATA.county[i];
            var itemValue = AREA_DATA.countyValue[i];

            if (AREA_DATA.countyValue[i].name == this.state.cityValue) {
                this.setState({
                    countyNames: itemName.area,
                    countyValues: itemValue.area,
                    countyName: itemName.area[0],
                    countyValue: itemValue.area[0],
                })
                result = true;
            }
        }
        if (!result) {
            this.setState({
                countyNames: [],
                countyValues: [],
                countyName: '',
                countyValue: 0,
            })
        }
    }

    render() {
        return (
            <Modal
                animationType={"slide"}
                transparent={true}
                onRequestClose={() => {
                    alert("Modal has been closed.")
                }}
            >
                <View style={{height: 290}}>

                    <View style={{height: AppSetting.LineWidth, backgroundColor: AppSetting.SplitLine}}/>
                    <View style={{
                        height: 45,
                        backgroundColor: '#FFFFFF',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingLeft: AppSetting.NORMAL_MARGIN,
                        paddingRight: AppSetting.NORMAL_MARGIN,
                    }}>
                        <Button
                            text={'取消'}
                            fontSize={13}
                            color={AppSetting.TITLE_GRAY}
                            style={{backgroundColor: 'transparent'}}
                        />
                        <Text style={AppStyles.nameText}>地址选择</Text>
                        <Button
                            text={'确定'}
                            fontSize={13}
                            color={AppSetting.MAIN_COLOR}
                            style={{backgroundColor: 'transparent'}}
                        />
                    </View>
                    <View style={{height: AppSetting.LineWidth, backgroundColor: AppSetting.SplitLine}}/>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        backgroundColor: '#FFFFFF',
                        padding: AppSetting.NORMAL_MARGIN,
                    }}>
                        <Wheel
                            style={{flex: 1}}
                            itemStyle={{textAlign: 'center'}}
                            items={this.state.provinceNames}
                            onChange={index => {
                                this.setState({
                                    provinceName: this.state.provinceNames[index],
                                    provinceValue: this.state.provinceValues[index],
                                }, () => this.searchCitys())
                            }}
                        />
                        <Wheel
                            style={{flex: 1}}
                            itemStyle={{textAlign: 'center'}}
                            items={this.state.cityNames}
                            onChange={index => {
                                this.setState({
                                    cityName: this.state.cityNames[index],
                                    cityValue: this.state.cityValues[index],
                                }, () => this.searchCountyCitys())
                            }}
                        />
                        <Wheel
                            style={{flex: 1}}
                            itemStyle={{textAlign: 'center'}}
                            items={this.state.countyNames}
                            onChange={index => {
                                this.setState({
                                    countyName: this.state.countyNames[index],
                                    countyValue: this.state.countyValues[index],
                                })
                            }}
                        />
                    </View>
                </View>
            </Modal>
        );
    }

}
