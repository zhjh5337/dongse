import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {AppSetting} from "./AppStyles";
import {
    StyleSheet,
    Text,
    StatusBar,
    TouchableOpacity,
    View,
    Image,
    Platform
} from 'react-native'

import {Theme} from 'teaset'

export default class ActionBar extends Component {

    static propTypes = {
        isShowBack: PropTypes.bool,
        title: PropTypes.string,
        backgroundColor: PropTypes.string,
        funcTextColor: PropTypes.string,
        functionName: PropTypes.string,
        barStyle: PropTypes.string,
        functionIco: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        backEvent: PropTypes.func,
        functionEvent: PropTypes.func,
    };

    static defaultProps = {
        isShowBack: true,
        isShowFunction: true,
        backgroundColor: AppSetting.MAIN_COLOR,
        functionName: '',
        title: '',
        funcTextColor: '',
        barStyle: 'light-content'
    };

    constructor(props) {
        super(props)
        this.state = {
            isShowBack: props.isShowBack,
            title: props.title,
            backgroundColor: props.backgroundColor,
            functionName: props.functionName,
            functionIco: props.functionIco,
            backEvent: props.backEvent,
            functionEvent: props.functionEvent,
            funcTextColor: props.funcTextColor,
            barStyle: props.barStyle,
            backColor: '',
            titleColor: '',
            isShowLine: true,
            leftView: props.leftView,
        }
    }

    componentWillMount() {
        this.titleColor = this.props.backgroundColor === '#FFFFFF' || this.props.backgroundColor === '#ffffff' ? AppSetting.BLACK : '#FFFFFF'
        this.backColor = this.props.backgroundColor === '#FFFFFF' || this.props.backgroundColor === '#ffffff' ? AppSetting.MAIN_COLOR : '#FFFFFF'
        this.isShowLine = this.props.backgroundColor === '#FFFFFF' || this.props.backgroundColor === '#ffffff'
        StatusBar.setBarStyle(this.props.barStyle, true)
    }

    componentWillReceiveProps(nextProps){
        // StatusBar.setBarStyle(nextProps.barStyle, true)
        // console.log('=====statusBar======' + JSON.stringify(nextProps))
    }

    componentWillUnmount() {
        StatusBar.setBarStyle(this.props.barStyle, true)
    }

    backView = () => {
        return (
            <View style={{flex: 2}}>
                {
                    this.props.leftView ? this.props.leftView :
                    !this.props.isShowBack ? null :

                        <TouchableOpacity style={{
                            flexDirection: 'row',
                            paddingHorizontal: 10,
                            alignItems: 'center',
                            height: '100%',
                        }} onPress={this.props.backEvent ? this.props.backEvent : () => Actions.pop()}>
                            <Image
                                underlayColor='transparent'
                                source={require('../../assets/images/back_black.png')}
                                style={{tintColor: this.backColor}}
                                />
                            <Text style={{
                                fontSize: 16,
                                color: this.backColor,
                                backgroundColor: 'transparent'
                            }}>返回</Text>
                        </TouchableOpacity>
                }
            </View>
        )
    }


    functionView = () => {
        return (
            <View style={{flex: 2}}>
                {
                    this.props.functionIco === '' && this.props.functionName === '' ? null :
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row-reverse',
                                paddingHorizontal: 13,
                                alignItems: 'center',
                                height: '100%'
                            }}
                            activeOpacity={0.7}
                            underlayColor='transparent'
                            onPress={this.props.functionEvent}>
                            {
                                this.props.functionName === '' ? null :
                                    <Text style={{
                                        fontSize: 16,
                                        color: this.props.funcTextColor === '' ? this.titleColor : this.props.funcTextColor,
                                        marginLeft:5,
                                        backgroundColor: 'transparent'
                                    }}>{this.props.functionName}</Text>
                            }
                            {
                                this.props.functionIco === null ? null :
                                    <Image source={this.props.functionIco}
                                           style={{tintColor: this.props.funcTextColor === '' ? this.titleColor : this.props.funcTextColor}}/>
                            }

                        </TouchableOpacity>
                }
            </View>
        )
    }


    render() {
        return (
            <View style={[styles.background,{
                borderBottomWidth: this.isShowLine ? AppSetting.LineWidth : 0,
                backgroundColor: this.props.backgroundColor,

            }, this.props.style]}>
                {this.backView()}
                <View style={{flex: 3, alignItems: 'center'}}>
                    <Text
                        style={[styles.actionBarTitle, {color: this.titleColor}]}>{this.props.title}</Text>
                </View>
                {this.functionView()}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    background: {
        marginTop: Platform.OS === 'ios' ? (Theme.isIPhoneX ? 30 : 20) : Platform.Version > 19 ? StatusBar.currentHeight : 0,
        height: Platform.OS === 'ios' ?   (Theme.isIPhoneX ? 54 : 44) : 56,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: AppSetting.PLACEHOLD,
    },
    actionBarTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        backgroundColor: 'transparent',
        alignSelf: 'center'
    }
})
