import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import {AppSetting, AppStyles} from './AppStyles'
import ImageButton from "../components/ImageButton"
import selectImage from 'react-native-image-crop-picker'
import {ActionSheet, AlbumView, Overlay} from 'teaset'

const itemWidth = (Dimensions.get('window').width - AppSetting.NORMAL_MARGIN * 5) / 4

/**
 * 多图选择
 */
export default class MultipleImageItemView extends Component {

    static propTypes = {
        imageWidth: PropTypes.number,
        imageHeight: PropTypes.number,
        name: PropTypes.string,
        onPress: PropTypes.func,
        isPreview: PropTypes.bool,
        onImageChanged: PropTypes.func,
        imageUrl: PropTypes.array,
        value: PropTypes.array,
    };

    static defaultProps = {
        imageWidth: itemWidth,
        imageHeight: itemWidth,
        isPreview: false,
        imageUrl: [],
        value: []
    };

    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            onPress: props.onPress,
            style: props.style,
            onImageChanged: props.onImageChanged,
            isPreview: props.isPreview,
            images: props.isPreview ? props.imageUrl : props.value,
            value: props.value,
            items: []
        }

    }


    /**
     * 添加图片
     */
    addImage = () => {
        let items = [
            {title: '从相册选择', onPress: () => this.imageProcessing(false)},
            {title: '拍照', onPress: () => this.imageProcessing(true)},
        ];
        ActionSheet.show(items, {title: '取消'});
    }

    /**
     * 删除图片
     * @param index
     */
    delImage = (index) => {
        // images.splice(index, 1)
        let im = this.state.value
        im.splice(index, 1)
        this.setState({
                value: im
            },
            this.props.onImageChanged(this.state.value))
    }

    saveImage = (image) => {
        // images.push(image)
        let im = this.state.value
        im.push(image)
        this.setState({
                value: im,
            },
            () => {
                this.props.onImageChanged(this.state.value)
            })
    }


    selectImages = (isCamera: boolean) => {
        return isCamera ?
            selectImage.openCamera({
                cropping: true,
                width: 500,
                height: 500,
                includeBase64: true,
                compressImageQuality: 0.7,
            }) :
            selectImage.openPicker({
                cropping: true,
                width: 500,
                height: 500,
                multiple: false,
                waitAnimationEnd: false,
                includeBase64: true,
                maxFiles: 1,
                compressImageQuality: 0.7,
            })
    }


    imageProcessing = (isCamera: boolean) => {
        this.selectImages(isCamera)
            .then(image =>
                this.saveImage(image))

            .catch(e => {
            });
    }


    onImagePress = (index) => {
        if (!this.props.isPreview) {
            return
        }
        let pressView = this.refs['it' + index];
        console.log('-------- ' + pressView)
        let ims = []
        this.state.value.forEach((item, i) =>
            ims.push({uri: item})
        )
        pressView.measure((x, y, width, height, pageX, pageY) => {
            let overlayView = (
                <Overlay.PopView
                    style={{}}
                    containerStyle={{flex: 1}}
                    overlayOpacity={1}
                    type='custom'
                    customBounds={{x: pageX, y: pageY, width, height}}
                    ref={v => this.fullImageView = v}
                >
                    <AlbumView
                        style={{flex: 1}}
                        control={true}
                        images={ims}
                        thumbs={ims}
                        defaultIndex={index}
                        onPress={() => this.fullImageView && this.fullImageView.close()}
                    />
                </Overlay.PopView>
            );
            Overlay.show(overlayView);
        });

    }

    render() {
        console.log(this.state.images);
        return (
            <View style={[{
                backgroundColor: 'white',
                borderColor: AppSetting.SplitLine,
                borderTopWidth: AppSetting.LineWidth,
                borderBottomWidth: AppSetting.LineWidth,
                paddingBottom: AppSetting.NORMAL_MARGIN
            },
                this.props.style]}>
                <View style={{
                    height: AppSetting.ITEM_HEIGHT,
                    paddingLeft: AppSetting.NORMAL_MARGIN,
                    justifyContent: 'center'
                }}>
                    <Text style={AppStyles.nameText}>{this.props.name}</Text>
                </View>


                <View style={{
                    flexDirection: 'row',
                    marginLeft: AppSetting.NORMAL_MARGIN,
                    justifyContent: 'flex-start',
                    flexWrap: 'wrap',
                }}>

                    {this.state.value ? this.state.value.map((item, index) => (
                        <TouchableOpacity
                            key={index}
                            ref={'it' + index}>
                            < ImageButton
                                style={styles.imageButton}
                                source={this.props.isPreview ? {uri: item} : {uri: `data:${item.mime};base64,` + item.data}}
                                showFunc={!this.props.isPreview}
                                onFuncPress={() => this.delImage(index)}
                                onPress={() => this.props.isPreview ? this.onImagePress(index) : null}
                            />
                        </TouchableOpacity>
                    )) : null}

                    {
                        this.props.isPreview ? null :
                            <ImageButton
                                style={styles.imageButton}
                                source={require('../../assets/images/ic_add_image.png')}
                                onPress={() => this.addImage()}
                            />
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    imageButton: {
        width: itemWidth,
        height: itemWidth,
        marginRight: AppSetting.NORMAL_MARGIN,
        marginBottom: AppSetting.NORMAL_MARGIN,
    },
})
