import React from 'react'
import {
    View,
    Text,
    ActivityIndicator
} from 'react-native'
import {Overlay} from 'teaset'

export default class ProgressDialog {

    static customKey = null;

    /**
     *
     * @param title
     */
    static show(title) {
        // if(this.customKey != null){
        //     return
        // }
        let dialogView = (
            <Overlay.View
                style={{alignItems: 'center', justifyContent: 'center'}}
                modal={true}
                overlayOpacity={0}
            >
                <View style={{backgroundColor: '#000000', opacity: 0.7,padding: 30, borderRadius: 15, alignItems: 'center'}}>
                    <ActivityIndicator size='large' color={'#FFFFFF'}/>
                    {
                        title !== undefined &&  title !== '' ?  <Text style={{marginTop:10, color:'white', fontSize:15}}>{title}</Text> : null
                    }
                </View>
            </Overlay.View>
        );
        this.customKey = Overlay.show(dialogView);
    }

    static hide() {
        Overlay.hide(this.customKey);
        this.customKey == null
    }


}
