/**
 * Created by lilong on 2018/5/2.
 */

import React, {Component} from 'react'

import {
    Image,
    View,
    TextInput,
} from 'react-native'
import {AppSetting} from './AppStyles'

export default class SearchBar extends Component {

    static propTypes = {};

    static defaultProps = {
        searchPlaceHolder:'',
        onSubmitEditing:()=>{},
        onChangeText:()=>{},
    };

    constructor(props) {
        super(props)
        this.state = {
            searchPlaceHolder: this.props.searchPlaceHolder,
            onSubmitEditing:this.props.onSubmitEditing,
            onChangeText:this.props.onChangeText,
        }
    }

    render() {
        return (
            <View style={{
                flexDirection: 'row',
                padding: 5,
                flex: 1,
                backgroundColor: AppSetting.GRAY,
                borderRadius: 15,
                alignItems: 'center'
            }}>
                <Image style={{height: 15, width: 15, marginRight: 3}} resizeMode="contain"
                       source={require('../../assets/images/icon_search.png')}/>
                <TextInput
                    placeholder={this.state.searchPlaceHolder}
                    underlineColorAndroid="transparent"
                    style={{padding: 0, flex: 1, backgroundColor: AppSetting.GRAY,}}
                    onSubmitEditing={this.state.onSubmitEditing}
                    onChangeText={(value) => this.state.onChangeText(value)}
                />
            </View>
        );
    }
}
