import React, {Component} from 'react'
import moment from "moment/moment"
import {AppSetting} from "./AppStyles"
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet, Dimensions,
} from 'react-native'

/**
 * 钱包ITEM view
 */
export default class WalletItemView extends Component {

    render() {
        return (
            <TouchableOpacity style={styles.itemBg}
                              activeOpacity={0.7}
                              underlayColor='transparent'>
                <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.itemMainText, {flex: 1}]}>{this.props.data.details}</Text>
                    <Text style={styles.itemMainText}>{parseFloat(this.props.data.delta_amount)}</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Text style={[styles.itemMinorText, {flex: 1}]}>{moment.unix(this.props.data.created_at).format('YYYY-MM-DD HH:mm:ss')}</Text>
                    <Text style={styles.itemMinorText}>{AppSetting.parseBalance(this.props.data.current_amount)}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

let styles = StyleSheet.create({
    itemBg: {
        height: 68,
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15
    },
    itemMainText: {
        color: AppSetting.BLACK,
        fontSize: 17
    },
    itemMinorText: {
        color: '#888888',
        fontSize: 14
    },
    btnBg: {
        width: Dimensions.get('window').width,
        backgroundColor: 'white',
        borderTopWidth: AppSetting.LineWidth,
        borderTopColor: AppSetting.SplitLine,
    }
});
