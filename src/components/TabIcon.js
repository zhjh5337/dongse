import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    Text,
    Image,
    View,
} from 'react-native'
import {AppSetting} from './AppStyles'
import {connect} from 'react-redux'

class TabIcon extends Component {

    static propTypes = {
        title: PropTypes.string,
    };

    static defaultProps = {};

    constructor(props) {
        super(props)
        this.state = {
            title: props.title,
        }

    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                <Image source={this.props.focused ? this.props.activeIcon : this.props.inactiveIcon}/>
            </View>
        );
    }
}

export default TabIcon;
