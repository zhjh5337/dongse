import React, {Component} from 'react'

import {
    Text,
    View,
} from 'react-native'
import {AppSetting} from './AppStyles'

export default class FooterView extends Component {

    static propTypes = {

    };

    static defaultProps = {

    };

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={{flexDirection: 'row', padding: 15, justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flex: 1, height: AppSetting.LineWidth, backgroundColor: '#E6E6E6', marginRight: 10}}/>
                <Text style={{fontSize: 12, color: '#B2B2B2'}}>我是有底线的</Text>
                <View style={{flex: 1, height: AppSetting.LineWidth, backgroundColor: '#E6E6E6', marginLeft: 10}}/>
            </View>
        );
    }
}
