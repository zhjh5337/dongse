import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    TouchableOpacity,
    Image,
} from 'react-native'

export default class ImageButton extends Component {

    static propTypes = {
        resizeMode: PropTypes.string,
        onPress: PropTypes.func,
        showFunc: PropTypes.bool,
        onFuncPress: PropTypes.func,
        funcIco: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        // source: PropTypes.oneOfType([
        //     PropTypes.shape({
        //         uri: PropTypes.string,
        //         headers: PropTypes.objectOf(PropTypes.string),
        //     }),
        //     PropTypes.number,
        //     PropTypes.arrayOf(
        //         PropTypes.shape({
        //             uri: PropTypes.string,
        //             width: PropTypes.number,
        //             height: PropTypes.number,
        //             headers: PropTypes.objectOf(PropTypes.string),
        //         }))
        // ]),
    };

    static defaultProps = {
        showFunc: false,
        funcIco: require('../../assets/images/ic_del.png'),
        resizeMode: 'contain',
    };

    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            showFunc: props.showFunc,
            funcIco: props.funcIco,
            onFuncPress: props.onFuncPress,
            resizeMode: props.resizeMode,
            onPress: props.onPress,
            style: props.style,
            source: props.source,
        }
    }

    render() {
        return (
            <TouchableOpacity
                ref={this.props.ref}
                activeOpacity={0.7}
                underlayColor='transparent'
                style={[this.props.style, {alignItems: 'center'}]}
                onPress={this.props.onPress}>
                <Image
                    style={{flex: 1, width: 65, height: 65}}
                    source={this.props.source}
                    resizeMode={this.props.resizeMode}
                />
                {
                    this.props.showFunc ?
                        <TouchableOpacity activeOpacity={0.7}
                                          underlayColor='transparent'
                                          style={{
                                              position: 'absolute',
                                              top: 5,
                                              right: 5,
                                              padding: 5
                                          }}
                                          onPress={this.props.onFuncPress}>
                            <Image
                                source={this.props.funcIco}
                            />

                        </TouchableOpacity> : null
                }

            </TouchableOpacity>
        );
    }
}
