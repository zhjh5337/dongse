import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    Text,
    TouchableOpacity,
    View,
} from 'react-native'
import {AppSetting} from './AppStyles'

export default class Button extends Component {

    static propTypes = {
        text: PropTypes.string,
        color: PropTypes.string,
        fontSize: PropTypes.number,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        color: '#FFFFFF',
        fontSize: 18,
    };

    constructor(props) {
        super(props)
        this.state = {
            text: props.text,
            color: props.color,
            fontSize: props.fontSize,
            onPress: props.onPress,
            style: props.style,
        }
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={0.7}
                              underlayColor='transparent'
                              style={[{
                                  backgroundColor: AppSetting.MAIN_COLOR,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  height: 48
                              }, this.props.style]}
                              onPress={this.props.onPress}>
                <Text style={{color: this.props.color, fontSize: this.props.fontSize, backgroundColor: 'transparent'}}>
                    {this.props.text}
                </Text>
            </TouchableOpacity>
        );
    }
}
