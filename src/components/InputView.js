import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    TextInput,
    View,
    Image,
} from 'react-native'
import {AppSetting} from "./AppStyles";

class InputView extends Component {

    static propTypes = {
        align: PropTypes.string,
        name: PropTypes.string,
        hintText: PropTypes.string,
        isPassword: PropTypes.bool,
        funcDisabled: PropTypes.bool,
        icon: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        funcIco: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        funcName: PropTypes.string,
        backgroundColor: PropTypes.string,
        linearColor: PropTypes.string,
        onPress: PropTypes.func,
        onChangeText: PropTypes.func,
        funcOnPress: PropTypes.func,
        editable: PropTypes.bool,
    };

    static defaultProps = {
        align: '',//top center bottom
        backgroundColor: '#FFFFFF',
        linearColor: AppSetting.SplitLine,
        funcDisabled: false,
        isPassword: false,
        editable: true,
    };

    constructor(props) {
        super(props)
        this.state = {
            align: props.align,
            name: props.name,
            icon: props.icon,
            backgroundColor: props.backgroundColor,
            linearColor: props.linearColor,
            onPress: props.onPress,
            hintText: props.hintText,
            onChangeText: props.onChangeText,
            funcIco: props.funcIco,
            funcName: props.funcOnPress,
            funcOnPress: props.funcOnPress,
            isPassword: props.isPassword,
            funcDisabled: props.funcDisabled,
        }
        // console.disableYellowBox = true;
        // console.warn('YellowBox is disabled.');
    }

    componentWillMount() {
    }

    funcView = () => {
        return (
            this.props.funcName === null && this.props.funcIco === null ? null :
                <TouchableOpacity onPress={this.props.funcOnPress}
                                  activeOpacity={0.7}
                                  underlayColor='transparent'
                                  disabled={this.props.funcDisabled}
                                  style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    {
                        this.props.funcIco === null ? null :
                            <Image resizeMode={'cover'} source={this.props.funcIco}/>
                    }
                    {
                        this.props.funcName === null ? null :
                            <Text style={{
                                fontSize: 15,
                                color: this.props.funcDisabled ? AppSetting.TITLE_GRAY : AppSetting.MAIN_COLOR,
                                marginLeft: 6,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>{this.props.funcName}</Text>
                    }
                </TouchableOpacity>
        );

    }

    separatorView = (align: string) => {
        return (
            <View
                style={[align === 'top' ? InputStyles.separatorTop : align === 'center' ? InputStyles.separatorCenter : InputStyles.separatorBottom, {backgroundColor: this.props.linearColor}]}/>
        )
    }


    render() {
        return (
            <TouchableOpacity activeOpacity={0.7}
                              onPress={this.props.onPress}
                              underlayColor='transparent'
                              style={this.props.style}>
                <View style={[InputStyles.background, {backgroundColor: this.props.backgroundColor}]}>
                    {
                        this.props.align === '' || this.props.align === 'top' ? this.separatorView('top') : null
                    }

                    <View style={{flex: 2, flexDirection: 'row', alignItems: 'center'}}>
                        {
                            !this.props.icon ? null :
                                <Image source={this.props.icon} style={{marginRight: 10}}/>
                        }
                        <Text style={InputStyles.itemName}>{this.props.name}</Text>
                    </View>
                    <TouchableOpacity
                        underlayColor='transparent'
                        activeOpacity={0.7}
                        style={{flex: 5, flexDirection: 'row', alignItems:'center'}}
                        onPress={this.props.editable ? null : this.props.funcOnPress}>
                        {
                            this.props.editable ?
                                <TextInput
                                    onSubmitEditing={this.props.onSubmitEditing || null}
                                    editable={this.props.editable}
                                    placeholderTextColor={'#B2B2B2'}
                                    value={this.props.value}
                                    autoCapitalize={'none'}
                                    underlineColorAndroid='transparent'
                                    keyboardType={this.props.keyboardType}
                                    style={[InputStyles.itemInput, {flex: 1}]}
                                    placeholder={this.props.hintText}
                                    returnKeyLabel={this.props.returnKeyLabel}
                                    returnKeyType={this.props.returnKeyType}
                                    maxLength={this.props.maxLength}
                                    multiline={this.props.multiline}
                                    secureTextEntry={this.props.isPassword}
                                    selectionColor={AppSetting.GREEN}
                                    onChangeText={this.props.onChangeText}/>
                                :

                                <Text
                                    style={[InputStyles.itemContent, {
                                        color: this.props.value ? AppSetting.DEFAULT : '#B2B2B2'
                                    }]}>{this.props.value ? this.props.value : this.props.hintText}</Text>

                        }
                        {
                            this.funcView()
                        }
                    </TouchableOpacity>
                    {this.props.isBottomLine ? null : (this.props.align === '' || this.props.align === 'bottom' ? this.separatorView('bottom') : this.separatorView('center'))

                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const InputStyles = StyleSheet.create({
    background: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: 10,
        backgroundColor: '#FFFFFF'
    },
    separatorTop: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        right: 0,
        top: 0,
        left: 0
    },
    separatorCenter: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: AppSetting.NORMAL_MARGIN,
    },
    separatorBottom: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
    },
    itemName: {
        fontSize: 16,
        color: AppSetting.TITLE,
        backgroundColor: 'transparent',
        padding: 0
    },
    itemInput: {
        fontSize: 15,
        color: AppSetting.DEFAULT,
        padding: 0
    },
    itemContent: {
        fontSize: 15,
        color: AppSetting.DEFAULT,
        marginRight: 7,
        flex: 1,
        backgroundColor: 'transparent'
    },

})


export {InputView, InputStyles}
export default InputView
