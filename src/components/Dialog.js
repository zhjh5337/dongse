import React, {Component} from "react"
import {Dimensions, Text, View} from 'react-native'

import PropTypes from 'prop-types'
import {Overlay} from 'teaset'
import {AppSetting} from "./AppStyles"
import Button from "./Button"

export default class Dialog  {

    static title = '';
    static message = '';
    static cancelable = false;
    static positiveName = '';
    static positivePress;
    static negativeName = '';
    static negativePress;


    static setTitle(title: PropTypes.string) {
        this.title = title
        return this;
    }

    static setMessage(message: PropTypes.string) {
        this.message = message
        return this;
    }

    /**
     * 点击外部是否可取消
     * @param cancelable
     */
    static setCancelable(cancelable: PropTypes.boolean) {
        this.cancelable = cancelable
        return this;
    }

    /**
     * 确定按钮
     * @param name
     * @param onPress
     */
    static setPositiveButton(name: PropTypes.string, onPress: PropTypes.func) {
        this.positiveName = name
        this.positivePress = onPress
        return this;
    }


    /**
     * 取消按钮
     * @param name
     * @param onPress
     */
    static setNegativeButton(name: PropTypes.string, onPress: PropTypes.func) {
        this.negativeName = name
        this.negativePress = onPress
        return this;
    }


    static show() {
        let key;
        let dialogView = (
            <Overlay.View
                style={{alignItems: 'center', justifyContent: 'center'}}
                modal={!this.cancelable}
                overlayOpacity={0.4}
            >
                <View style={{
                    backgroundColor: '#FFFFFF',
                    width: Dimensions.get('window').width - 80,
                    borderRadius: 10,
                    alignItems: 'center',
                    flexDirection: 'column',
                }}>

                    {
                        this.title === '' ? null :
                            <Text style={{
                                color: 'black',
                                fontSize: 18,
                                marginTop: 20,
                            }}>{this.title}</Text>
                    }
                    {
                        this.message === '' ? null :
                        <Text style={{
                            color: '#888888',
                            fontSize: 15,
                            marginTop: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            alignItems: 'center'
                        }}>{this.message}</Text>
                    }
                    <View style={{
                        height: 45,
                        width: '100%',
                        flexDirection: 'row',
                        marginTop: 20,
                        borderTopColor: AppSetting.SplitLine,
                        borderTopWidth: AppSetting.LineWidth
                    }}>
                        {
                            !this.negativeName ? null :
                                <Button
                                    color={AppSetting.BLACK}
                                    text={'取消'}
                                    style={{flex: 1, backgroundColor: 'transparent'}}
                                    onPress={() => {
                                        Overlay.hide(key)
                                        if (this.negativePress !== undefined) {
                                            this.negativePress()
                                        }
                                    }
                                    }/>
                        }
                        {
                            !this.negativeName || !this.positiveName ? null :
                                <View style={{
                                    height: 45,
                                    width: AppSetting.LineWidth,
                                    backgroundColor: AppSetting.SplitLine,
                                }}/>
                        }

                        {
                            !this.positiveName ? null :
                                <Button
                                    color={AppSetting.GREEN}
                                    text={'确定'}
                                    style={{flex: 1, backgroundColor: 'transparent'}}
                                    onPress={() => {
                                        Overlay.hide(key)
                                        if (this.positivePress !== undefined) {
                                            this.positivePress()
                                        }
                                    }
                                    }/>
                        }
                    </View>
                </View>
            </Overlay.View>
        );
        key = Overlay.show(dialogView);
    }

}
