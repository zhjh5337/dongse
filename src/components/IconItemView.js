import React, {Component} from 'react'

import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
} from 'react-native'

import {AppSetting} from '../components/AppStyles'

export default class IconItemView extends Component {

    static propTypes = {
        align: PropTypes.string,
        name: PropTypes.string,
        nameStyle: PropTypes.array,
        icon: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        number: PropTypes.number,
        contentShowLeft: PropTypes.bool,
        content: PropTypes.string,
        // contentTextStyle: PropTypes.symbol,
        contentIco: PropTypes.oneOfType([
            PropTypes.shape({
                uri: PropTypes.string,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
            PropTypes.number,
            PropTypes.arrayOf(
                PropTypes.shape({
                    uri: PropTypes.string,
                    width: PropTypes.number,
                    height: PropTypes.number,
                    headers: PropTypes.objectOf(PropTypes.string),
                }))
        ]),
        isShowArrow: PropTypes.bool,
        backgroundColor: PropTypes.string,
        linearColor: PropTypes.string,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        align: '',//top center bottom
        number: 0,
        content: '',
        backgroundColor: '#FFFFFF',
        linearColor: AppSetting.SplitLine,
        isShowArrow: true,
        contentShowLeft: true,
    };

    constructor(props) {
        super(props)
        this.state = {
            align: props.align,
            name: props.name,
            icon: props.icon,
            number: props.number,
            content: props.content,
            contentTextStyle: props.contentTextStyle,
            isShowArrow: props.isShowArrow,
            backgroundColor: props.backgroundColor,
            linearColor: props.linearColor,
            contentIco: props.contentIco,
            contentShowLeft: props.contentShowLeft,
            nameStyle: props.nameStyle,
            onPress: props.onPress,
        }
    }

    componentWillMount() {

    }

    separatorView = (align: string) => {
        return (
            <View
                style={[align === 'top' ? styles.separatorTop : align === 'center' ? styles.separatorCenter : styles.separatorBottom, {backgroundColor: this.props.linearColor}]}/>
        )
    }


    render() {
        return (
            <TouchableOpacity activeOpacity={0.7} underlayColor='transparent' onPress={this.props.onPress}
                              style={this.props.style}>
                <View style={[styles.background, {backgroundColor: this.props.backgroundColor}]}>
                    {
                        this.props.align === '' || this.props.align === 'top' ? this.separatorView('top') : null
                    }
                    {
                        this.props.icon === undefined ||  this.props.icon === null || this.props.icon === '' ? null :
                            <Image source={this.props.icon} resizeMode={'contain'} style={{marginRight: 5,  width: 18}}/>
                    }

                    <Text
                        style={[styles.itemName, this.props.nameStyle]}>{this.props.name}</Text>
                    {
                        this.props.number === 0 ? null :
                            <View style={styles.numberBg}>
                                <Text style={styles.itemNumber}>{this.props.number}</Text>
                            </View>
                    }
                    {this.props.contentShowLeft ?  <View style={{flex: 1}}></View> : null}
                    {
                        this.props.contentIco === null ? null :
                            <Image source={this.props.contentIco} resizeMode={'contain'} style={{width: 30, height:30}}/>
                    }
                    {
                        this.props.content === '' ? null :
                            <Text style={[styles.itemContent, this.props.contentTextStyle]}>{this.props.content}</Text>
                    }
                    {this.props.contentShowLeft ?  null :  <View style={{flex: 1}}></View>}
                    {
                        this.props.isShowArrow ? <Image source={require('../../assets/images/arrow_right.png')}
                                                        style={{tintColor: AppSetting.SplitLine}}/> : null
                    }
                    {
                        this.props.align === '' || this.props.align === 'bottom' ? this.separatorView('bottom') : this.separatorView('center')
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    background: {
        height: AppSetting.ITEM_HEIGHT,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: AppSetting.NORMAL_MARGIN,
        paddingRight: 10
    },
    separatorTop: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        right: 0,
        top: 0,
        left: 0
    },
    separatorCenter: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: AppSetting.NORMAL_MARGIN,
    },
    separatorBottom: {
        height: AppSetting.LineWidth,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
    },
    itemName: {
        fontSize: 16,
        color: AppSetting.TITLE,
        width:85,
        backgroundColor: 'transparent'
    },
    itemContent: {
        fontSize: 15,
        color: AppSetting.DEFAULT,
        marginRight: 7,
        backgroundColor: 'transparent'
    },
    numberBg: {
        backgroundColor: '#FF3B30',
        height: 16,
        width: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    },
    itemNumber: {
        fontSize: 10,
        color: '#FFFFFF',
        backgroundColor: 'transparent'
    }
})
